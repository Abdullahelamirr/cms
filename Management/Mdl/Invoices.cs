﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Invoices
    {
        public Invoices()
        {
            ActiveStudents = new HashSet<ActiveStudents>();
            InvoicesDetails = new HashSet<InvoicesDetails>();
            Payments = new HashSet<Payments>();
        }

        public long InvoiceId { get; set; }
        public long? StudentId { get; set; }
        public int? RegistryType { get; set; }
        public int? CustomerType { get; set; }
        public long? CompanyId { get; set; }
        public double Paid { get; set; }
        public double TotalPrice { get; set; }
        public double TotalPriceAfterDescount { get; set; }
        public int Discount { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }

        public Companies Company { get; set; }
        public Students Student { get; set; }
        public ICollection<ActiveStudents> ActiveStudents { get; set; }
        public ICollection<InvoicesDetails> InvoicesDetails { get; set; }
        public ICollection<Payments> Payments { get; set; }
    }
}
