﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class EventFiles
    {
        public long EventFileId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }
        public byte[] Pdf { get; set; }
        public byte[] WordFile { get; set; }
        public long? EventId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }

        public Events Event { get; set; }
    }
}
