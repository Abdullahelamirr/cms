﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class MessageTransaction
    {
        public long MessageTransactionId { get; set; }
        public long? SentBy { get; set; }
        public long? RecivedBy { get; set; }
        public long? MessageId { get; set; }
        public int? IsRead { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
