﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Events
    {
        public Events()
        {
            ActiveStudents = new HashSet<ActiveStudents>();
            Agenda = new HashSet<Agenda>();
            EventFiles = new HashSet<EventFiles>();
            InvoicesDetails = new HashSet<InvoicesDetails>();
        }

        public long EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? MaximumNumberChair { get; set; }
        public int? TeacherId { get; set; }
        public int? HostId { get; set; }
        public long? SuperPackageId { get; set; }
        public long? PackageId { get; set; }
        public long? CourseId { get; set; }
        public short? EventType { get; set; }
        public decimal? PricePersonal { get; set; }
        public decimal? PriceCompany { get; set; }
        public int? Discount { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short? Status { get; set; }

        public Courses Course { get; set; }
        public Hosts Host { get; set; }
        public Packages Package { get; set; }
        public SuperPackages SuperPackage { get; set; }
        public Trainers Teacher { get; set; }
        public ICollection<ActiveStudents> ActiveStudents { get; set; }
        public ICollection<Agenda> Agenda { get; set; }
        public ICollection<EventFiles> EventFiles { get; set; }
        public ICollection<InvoicesDetails> InvoicesDetails { get; set; }
    }
}
