﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Messages
    {
        public long MesssageId { get; set; }
        public string Subject { get; set; }
        public string Payload { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
