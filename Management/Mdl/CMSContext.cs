﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Management.Mdl
{
    public partial class CMSContext : DbContext
    {
        public virtual DbSet<ActiveStudents> ActiveStudents { get; set; }
        public virtual DbSet<Agenda> Agenda { get; set; }
        public virtual DbSet<Appointments> Appointments { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<CourseFiles> CourseFiles { get; set; }
        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<EventFiles> EventFiles { get; set; }
        public virtual DbSet<Events> Events { get; set; }
        public virtual DbSet<Hosts> Hosts { get; set; }
        public virtual DbSet<Invoices> Invoices { get; set; }
        public virtual DbSet<InvoicesDetails> InvoicesDetails { get; set; }
        public virtual DbSet<MessageAttachments> MessageAttachments { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<MessageTransaction> MessageTransaction { get; set; }
        public virtual DbSet<Notifications> Notifications { get; set; }
        public virtual DbSet<Packages> Packages { get; set; }
        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<SuperPackages> SuperPackages { get; set; }
        public virtual DbSet<Trainers> Trainers { get; set; }
        public virtual DbSet<UserNotifications> UserNotifications { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=.;Database=CMS;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActiveStudents>(entity =>
            {
                entity.HasKey(e => e.ActiveStudeId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ActualStudent)
                    .WithMany(p => p.ActiveStudents)
                    .HasForeignKey(d => d.ActualStudentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActiveStudents_Students");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.ActiveStudents)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActiveStudents_Events");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.ActiveStudents)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActiveStudents_Invoices");
            });

            modelBuilder.Entity<Agenda>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateTimeFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTimeTo).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.FromToDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(100);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Agenda)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Agenda_Classes");
            });

            modelBuilder.Entity<Appointments>(entity =>
            {
                entity.HasKey(e => e.AppointmentId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateTimeFrom).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Title).HasMaxLength(100);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Appointments_Companies");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Appointments)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_Appointments_Students");
            });

            modelBuilder.Entity<Companies>(entity =>
            {
                entity.HasKey(e => e.CompanyId);

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.BoughtBy).HasMaxLength(150);

                entity.Property(e => e.City).HasMaxLength(150);

                entity.Property(e => e.Communicator).HasMaxLength(150);

                entity.Property(e => e.CompanyName).HasMaxLength(250);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<Countries>(entity =>
            {
                entity.Property(e => e.CountryCityName).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<CourseFiles>(entity =>
            {
                entity.HasKey(e => e.CourseFileId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.CourseFiles)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_CourseFiles_Courses");
            });

            modelBuilder.Entity<Courses>(entity =>
            {
                entity.HasKey(e => e.CourseId);

                entity.Property(e => e.Color).HasMaxLength(10);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.PriceCompany).HasColumnType("money");

                entity.Property(e => e.PricePersonal).HasColumnType("money");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Package)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.PackageId)
                    .HasConstraintName("FK_Courses_Packages");

                entity.HasOne(d => d.SuperPackage)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.SuperPackageId)
                    .HasConstraintName("FK_Courses_SuperPackages");
            });

            modelBuilder.Entity<EventFiles>(entity =>
            {
                entity.HasKey(e => e.EventFileId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventFiles)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_EventFiles_Events");
            });

            modelBuilder.Entity<Events>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(400);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.PriceCompany).HasColumnType("money");

                entity.Property(e => e.PricePersonal).HasColumnType("money");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_Classes_Courses");

                entity.HasOne(d => d.Host)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.HostId)
                    .HasConstraintName("FK_Events_Hosts");

                entity.HasOne(d => d.Package)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.PackageId)
                    .HasConstraintName("FK_Classes_Packages");

                entity.HasOne(d => d.SuperPackage)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.SuperPackageId)
                    .HasConstraintName("FK_Classes_SuperPackages");

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.TeacherId)
                    .HasConstraintName("FK_Classes_Trainers");
            });

            modelBuilder.Entity<Hosts>(entity =>
            {
                entity.HasKey(e => e.HostId);

                entity.Property(e => e.ContactName).HasMaxLength(50);

                entity.Property(e => e.ContactNumber).HasMaxLength(16);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(30);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Countries)
                    .WithMany(p => p.Hosts)
                    .HasForeignKey(d => d.CountriesId)
                    .HasConstraintName("FK_Hosts_Countries");
            });

            modelBuilder.Entity<Invoices>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Invoice_Companies");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.StudentId)
                    .HasConstraintName("FK_Invoice_Students");
            });

            modelBuilder.Entity<InvoicesDetails>(entity =>
            {
                entity.HasKey(e => e.InvoiceDetailsId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.InvoicesDetails)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_InvoicesDetails_Events");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.InvoicesDetails)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("FK_InvoicesDetails_Invoice");
            });

            modelBuilder.Entity<MessageAttachments>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.HasKey(e => e.MesssageId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Subject).HasMaxLength(150);
            });

            modelBuilder.Entity<MessageTransaction>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<Notifications>(entity =>
            {
                entity.HasKey(e => e.NotificationId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MsgTitle).HasMaxLength(70);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Notifications)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Notifications_Users");
            });

            modelBuilder.Entity<Packages>(entity =>
            {
                entity.HasKey(e => e.PackageId);

                entity.Property(e => e.Color)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.PriceCompany).HasColumnType("money");

                entity.Property(e => e.PricePersonal).HasColumnType("money");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.SuperPackage)
                    .WithMany(p => p.Packages)
                    .HasForeignKey(d => d.SuperPackageId)
                    .HasConstraintName("FK_Packages_SuperPackages");
            });

            modelBuilder.Entity<Payments>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CurrentPaidDate).HasColumnType("datetime");

                entity.Property(e => e.NextPaidDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentRefrence)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("FK_Payments_Invoice");
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.HasKey(e => e.StudentId);

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CurrentJob).HasMaxLength(150);

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.Location).HasMaxLength(150);

                entity.Property(e => e.NameAr).HasMaxLength(150);

                entity.Property(e => e.NameEn).HasMaxLength(150);

                entity.Property(e => e.Nationality).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber1).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber2).HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Students_Companies");
            });

            modelBuilder.Entity<SuperPackages>(entity =>
            {
                entity.HasKey(e => e.SuperPackageId);

                entity.Property(e => e.Color).HasMaxLength(10);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.PriceCompany).HasColumnType("money");

                entity.Property(e => e.PricePersonal).HasColumnType("money");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<Trainers>(entity =>
            {
                entity.HasKey(e => e.TrainerId);

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CurrentJob).HasMaxLength(150);

                entity.Property(e => e.Cv).HasColumnName("CV");

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.Location)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameAr).HasMaxLength(150);

                entity.Property(e => e.NameEn).HasMaxLength(150);

                entity.Property(e => e.Nationality).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber1).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber2).HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<UserNotifications>(entity =>
            {
                entity.HasKey(e => e.UserNotifId);

                entity.Property(e => e.ReadOn).HasColumnType("datetime");

                entity.HasOne(d => d.Notification)
                    .WithMany(p => p.UserNotifications)
                    .HasForeignKey(d => d.NotificationId)
                    .HasConstraintName("FK_UserNotifications_Notifications");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastLoginOn).HasColumnType("datetime");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LoginTryAttemptDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(24);
            });
        }
    }
}
