﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Countries
    {
        public Countries()
        {
            Hosts = new HashSet<Hosts>();
        }

        public int CountriesId { get; set; }
        public string CountryCityName { get; set; }
        public int? IndexOfCity { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }

        public ICollection<Hosts> Hosts { get; set; }
    }
}
