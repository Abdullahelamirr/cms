﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Hosts
    {
        public Hosts()
        {
            Events = new HashSet<Events>();
        }

        public int HostId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int? CountriesId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }

        public Countries Countries { get; set; }
        public ICollection<Events> Events { get; set; }
    }
}
