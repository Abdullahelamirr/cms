﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class Notifications
    {
        public Notifications()
        {
            UserNotifications = new HashSet<UserNotifications>();
        }

        public long NotificationId { get; set; }
        public string MsgTitle { get; set; }
        public string Msg { get; set; }
        public short? UserType { get; set; }
        public short? Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }

        public Users CreatedByNavigation { get; set; }
        public ICollection<UserNotifications> UserNotifications { get; set; }
    }
}
