﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class MessageAttachments
    {
        public long Id { get; set; }
        public long MessageId { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
    }
}
