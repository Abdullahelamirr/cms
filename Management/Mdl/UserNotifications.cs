﻿using System;
using System.Collections.Generic;

namespace Management.Mdl
{
    public partial class UserNotifications
    {
        public long UserNotifId { get; set; }
        public long? NotificationId { get; set; }
        public long? ReadBy { get; set; }
        public DateTime? ReadOn { get; set; }

        public Notifications Notification { get; set; }
    }
}
