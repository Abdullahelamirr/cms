﻿import addSuperPacakges from './AddSuperPackages/AddSuperPackages.vue';
import SuperPacakges from './EditSuperPackages/EditSuperPackages.vue';
import { eventBus } from '../../../app';
import moment from 'moment';
export default {
    name: 'SuperPackages',    
    created() {
        this.GetSuperPackages(this.pageNo);      
    },
    components: {
        'add-SuperPacakges': addSuperPacakges,
        'edit-SuperPacakges': SuperPacakges
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },

    data() {
        return {
            pageNo: 1,
            pageSize: 6,
            pages: 0,
            SuperPackages: [],
            state: 0,
            superPackageId: 0,
            dialogTableVisible: false,
        };
    },
    methods: {
        ShowMore(Info) {
            this.EventDetails = Info;
        },
        EditSuperPackagePage(SuperPackage) {
            this.SuperPackagesEdit = SuperPackage; 
            this.state = 2;
        },
        start() {
            this.hover=true
        },
        AddSuperPackagePage() {
            this.state = 1;
        },
        FileChanged(e, superPackageId) {
    
            var files = e.target.files;
            if (files == null) {
                this.superPackageId = superPackageId;
            }
            else {
            if (files.length <= 0) {
                return;
            }
      
            if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                this.$message({
                    type: 'error',
                    message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                });
                this.photo = null;
                return;
            }
          

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.photo = reader.result;
           
                $this.UploadImagesuperPackage();
            };
            reader.onerror = function (error) {
                $this.photo = null;
            };
                reader.readAsDataURL(files[0]);
            }
        },

        UploadImagesuperPackage() {
          
            this.$blockUI.Start();
            var obj = {
                Photo: this.photo,
                superPackageId: this.superPackageId
            };

            this.$http.UploadImagesuperPackage(obj)
                .then(response => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
            
                    setTimeout(() =>
                        window.location.href = '/Packages/SuperPackages'
                        , 500);

                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
        GetSuperPackages(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetSuperPackagesV1(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.SuperPackages = response.data.superPackage;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        OpenPackagePage(obj) {
            this.$parent.SuperPackageParent = obj;
            this.$router.push("/Packages/SubPackages/" + obj.superPackageId);
        },
        OpenCoursePage(obj) {
            this.$parent.SuperPackageParent = obj;
            this.$router.push("/Packages/Courses/" + obj.superPackageId);
        },


        DeleteSuperPackage(SuperPackageId) {
            this.$confirm('سيؤدي ذلك إلى حذف باقة الحزم نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteSuperPackage(SuperPackageId)
                    .then(response => {
                        if (this.SuperPackages.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح باقة الحزم بنجاح"
                        });
                        this.GetSuperPackages();
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
       
    }    
}
