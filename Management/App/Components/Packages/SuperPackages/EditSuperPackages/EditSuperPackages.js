﻿export default {
    name: 'AddSuperPackages',    
    created() {
        var superPackage=this.$parent.SuperPackagesEdit;
        this.form.Name = superPackage.name;
        this.form.Description = superPackage.description;

        this.form.PricePersonal = superPackage.pricePersonal;
        this.form.PriceCompany = superPackage.priceCompany;
        this.form.Discount = superPackage.discount;
        this.form.Color = superPackage.color;
        if (superPackage.contentType == null) {
            superPackage.contentType = '';
        }
        this.form.ContentType = superPackage.contentType;

    },
    data() {
        return {
            Content:[{ value: '', label: 'لايوجد' }, { value: 1, label: 'حـزم' }, { value: 2, label: 'دورات' }],
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                Name: '',
                Description: '',
                PricePersonal: '',
                PriceCompany: '',
                Discount: '',
                Color: '',
                ContentType: '',
              
                SuperPackageId:'',
            },
          
         
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        Edit() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الحزمة'
                });
                return;
            }

            if (!this.form.PricePersonal) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للأفراد'
                });
                return;
            }

            if (!this.form.PriceCompany) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للشركات'
                });
                return;
            }

            if (!this.form.Discount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التخفيض'
                });
                return;
            }
            if (!this.form.Color) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اللون'
                });
                return;
            }

            this.form.SuperPackageId = this.$parent.SuperPackagesEdit.superPackageId;
            this.$http.EditSuperPackage(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetSuperPackages();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        },

    }    
}
