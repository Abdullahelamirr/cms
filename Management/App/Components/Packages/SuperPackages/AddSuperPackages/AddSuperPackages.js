﻿export default {
    name: 'AddSuperPackages',    
    created() {
       
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                Name: '',
                Description: '',
                PricePersonal: '',
                PriceCompany: '',
                Discount: '',
                Color: '',
                ContentType:'',
            },
          
         
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        Save() {
            if (!this.form.ContentType) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال نوع المحتوي'
                });
                return;
            }
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الحزمة'
                });
                return;
            }

            if (!this.form.PricePersonal) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للأفراد'
                });
                return;
            }

            if (!this.form.PriceCompany) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للشركات'
                });
                return;
            }

            if (!this.form.Discount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التخفيض'
                });
                return;
            }
            if (!this.form.Color) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اللون'
                });
                return;
            }


           
          
            this.$blockUI.Start();
            this.$http.AddSuperPackage(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetSuperPackages();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });

                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });


            //if (!this.SirName) {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter SirName please!'
            //    });
            //    return;
            //}

            //if (!this.PhoneNumber) {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter PhoneNumber please!'
            //    });
            //    return;
            //}

            //if (this.Country == '') {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter Country name please!'
            //    });
            //    return;

            //}

            //this.$http.AddCustomer({
            //    CustomerID: 0,
            //    Name: this.Name,
            //    FatherName: this.FatherName,
            //    SirName: this.SirName,
            //    PhoneNumber: this.PhoneNumber,
            //    Age: this.Age,
                
            //    Address: this.Address,
            //    NID: this.NID,
            //    Country: this.Country
            //})
            //    .then(response => {
            //        this.$parent.state = 0;
            //        this.$parent.GetCustomers();
                 
            //        this.$message({
            //            type: 'info',
            //            message: response.data
            //        });
            //    })
            //    .catch((err) => {
            //        this.$message({
            //            type: 'error',
            //            message: err
            //        });
            //    });
        },

   
       // Back() {
       //     this.$parent.state = 0;
       // },
       //// GetCustomers
       // GetCustomers(pageNo) {
            
       //     this.pageNo = pageNo;
       //     if (this.pageNo === undefined) {
       //         this.pageNo = 1;
       //     }
       //     this.$http.GetCustomers(this.pageNo, this.pageSize)
       //         .then(response => {
                   
       //             this.Customers = response.data.customers;
       //             this.pages = response.data.count;
       //         })
       //         .catch((err) => {
       //             console.log(err);
       //             this.survey = [];
       //             this.pages = 0;
       //         });
       // },
    }    
}
