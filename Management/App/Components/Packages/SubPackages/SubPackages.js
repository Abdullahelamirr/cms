﻿import AddSubPackages from './AddSubPackages/AddSubPackages.vue';
import EditSubPackages from './EditSubPackages/EditSubPackages.vue';
import { eventBus } from '../../../app';
import moment from 'moment';
export default {
    name: 'Packages',    
    created() {
       
        console.log(this.$route.params.SelectedId);
        //if (this.$parent.SuperPackageParent==null) {
        //    this.$router.push("/Packages/SuperPackages");
        //}
        this.GetPackagesBySuperPackageId(this.pageNo);  
    },
    components: {
        'add-Packages': AddSubPackages,
        'edit-Packages': EditSubPackages
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            PackageEdit:[],
            SuperPackageParent: this.$parent.SuperPackageParent,
            pageNo: 1,
            pageSize: 6,
            pages: 0,  
            Packages: [],
            state: 0,
            packageId:0
          
        };
    },
    methods: {
        OpenCoursePage(obj) {
           
            this.$parent.PackageParent = obj;
            this.$router.push("/Packages/SubPackages/Courses/" + obj.packageId);
        },
        FileChanged(e, packageId) {

            var files = e.target.files;
            if (files == null) {
                this.packageId = packageId;
            }
            else {
                if (files.length <= 0) {
                    return;
                }

                if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                    this.$message({
                        type: 'error',
                        message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                    });
                    this.photo = null;
                    return;
                }


                var $this = this;

                var reader = new FileReader();
                reader.onload = function () {
                    $this.photo = reader.result;

                    $this.UploadImagePackage();
                };
                reader.onerror = function (error) {
                    $this.photo = null;
                };
                reader.readAsDataURL(files[0]);
            }
        },

        UploadImagePackage() {

            this.$blockUI.Start();
            var obj = {
                Photo: this.photo,
                packageId: this.packageId
            };

            this.$http.UploadImagePackage(obj)
                .then(response => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                   
                    setTimeout(() =>
                        window.location.href = '/Packages/SubPackages/' + this.$parent.SuperPackageParent.superPackageId
                        , 500);
                    this.GetPackagesBySuperPackageId();

                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
        EditPackage(Package) {
            this.PackageEdit = Package;
            this.state = 2;
        },
        AddPackage() {
            this.state = 1;
        },

        Back() {
            this.$router.push("/Packages");
        },

        DeletePackage(PackageId) {
            this.$confirm('سيؤدي ذلك إلى حذف الحزمة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeletePackage(PackageId)
                    .then(response => {
                        if (this.Packages.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الحزمة بنجاح"
                        });
                        this.GetPackagesBySuperPackageId(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
       
        GetPackagesBySuperPackageId(pageNo) {
           
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            if (!!this.$route.params.SuperPackageId) {

                this.$http.GetPackagesBySuperPackageId(this.pageNo, this.pageSize, this.$route.params.SuperPackageId)
                    .then(response => {
                        this.$blockUI.Stop();
                        this.Packages = response.data.packages;
                        this.pages = response.data.count;
                    })
                    .catch((err) => {
                        this.$blockUI.Stop();
                        console.error(err);
                        this.pages = 0;
                    });
            }
            else
            {

                this.$http.GetPackagesBySuperPackageId(this.pageNo, this.pageSize, this.$parent.SuperPackageParent.superPackageId)
                    .then(response => {
                        this.$blockUI.Stop();
                        this.Packages = response.data.packages;
                        this.pages = response.data.count;
                    })
                    .catch((err) => {
                        this.$blockUI.Stop();
                        console.error(err);
                        this.pages = 0;
                    });

            }

        },
       
    }    
}
