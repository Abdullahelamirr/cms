﻿import addCourses from './AddCourses/AddCourses.vue';
import editCourses from './EditCourses/EditCourses.vue';
import moment from 'moment';
export default {
    name: 'Course',    
    created() {
        debugger;
        console.log(this.$route.params.PackageId)
        console.log(this.$parent.PackageParent);
        //if (this.$parent.SuperPackageParent==null) {
        //    this.$router.push("/Packages/SuperPackages");
        //}
        this.GetCoursesByPackageId(this.pageNo);  
    },
    components: {
        'add-Courses': addCourses,
        'edit-Courses': editCourses
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            CourseEdit:[],
            PackageParent: this.$parent.PackageParent,
            pageNo: 1,
            pageSize: 6,
            pages: 0,  
            Courses: [],
            state: 0,
            courseId:0
          
        };
    },
    methods: {
        EditCourse(Course) {
            this.CourseEdit = Course;
            this.state = 2;
        },
        AddCourse() {
            this.state = 1;
        },
        FileChanged(e, courseId) {

            var files = e.target.files;
            if (files == null) {
                this.courseId = courseId;
            }
            else {
                if (files.length <= 0) {
                    return;
                }

                if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                    this.$message({
                        type: 'error',
                        message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                    });
                    this.photo = null;
                    return;
                }


                var $this = this;

                var reader = new FileReader();
                reader.onload = function () {
                    $this.photo = reader.result;

                    $this.UploadImageCourse();
                };
                reader.onerror = function (error) {
                    $this.photo = null;
                };
                reader.readAsDataURL(files[0]);
            }
        },

        UploadImageCourse() {

            this.$blockUI.Start();
            var obj = {
                Photo: this.photo,
                courseId: this.courseId
            };

            this.$http.UploadImageCourse(obj)
                .then(response => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });

                    setTimeout(() =>
                        window.location.href = '/Packages/SuperPackages'
                        , 500);

                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
        Back() {
            this.$router.push("/Packages");
        },
        OpenCourseFile(CourseId) {
            this.$router.push('/Courses/CourseFiles/' + CourseId);

        },

        DeleteCourse(courseId) {
            this.$confirm('سيؤدي ذلك إلى حذف الدورة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteCourse(courseId)
                    .then(response => {
                        if (this.Courses.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الدورة بنجاح"
                        });
                        this.GetCoursesByPackageId(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
       
        GetCoursesByPackageId(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCoursesByPackageId(this.pageNo, this.pageSize, this.$parent.PackageParent.packageId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Courses = response.data.courses;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });

        },
       
    }    
}
