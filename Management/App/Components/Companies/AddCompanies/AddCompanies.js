﻿export default {
    name: 'AddCompanies',
    created () {

    },
    data () {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                CompanyName: '',
                Description: '',
                PhoneNumber: '',
                City: '',
                Email: '',
                Address: '',
                BoughtBy: '',
                Communicator: ''
            }
        };
    },
    methods: {
        Back () {
            this.$parent.state = 0;
        },

        Save () {
            if (!this.form.CompanyName) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الشركة'
                });
                return;
            }

            if (!this.form.PhoneNumber) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }

            if (!this.form.City) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم المدينة'
                });
                return;
            }

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الالكتروني'
                });
                return;
            }
            if (!this.form.Address) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عنـوان الشركة'
                });
                return;
            }
            if (!this.form.BoughtBy) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم المشتري'
                });
                return;
            }

            if (!this.form.Communicator) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال جهة الاتصال'
                });
                return;
            }
            this.$blockUI.Start();
            this.$http.AddCompany(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetCompanies();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        }
    }
};
