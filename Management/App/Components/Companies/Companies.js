﻿import addCompanies from './AddCompanies/AddCompanies.vue';
import editCompanies from './EditCompanies/EditCompanies.vue';
import moment from 'moment';
export default {
    name: 'Companies',
    created () {
        this.GetCompanies(this.pageNo);
    },
    components: {
        'add-Companies': addCompanies,
        'edit-Companies': editCompanies
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            pageNo: 1,
            pageSize: 5,
            pages: 0,
            Students: [],
            Companies: [],
            state: 0,
            visible: false,
            EditCompany: '',
            dialogTableVisible: false,
            EventDetails:[]
        };
    },
    methods: {

        ShowMore(Company) {
            this.EventDetails = Company;
        },
        GetCompanies (pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCompaniesV1(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Companies = response.data.companies;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },

        AddCompaniesPage () {
            this.state = 1;
        },

        EditCompanyInfo (Company) {
            this.EditCompany = Company;
            this.state = 2;
        },

        DeleteCompany (CompanyId) {
            this.$confirm('سيؤدي ذلك إلى حذف الشركة و افـراد الشركة نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteCompany(CompanyId)
                    .then(response => {
                        if (this.Students.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: 'تم مسح الشركة بنجاح'
                        });
                        this.GetCompanies();
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        }
    }
};
