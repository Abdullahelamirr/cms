﻿export default {
    name: 'AddMainNotifications',
    created() {

    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
          
            form: {
                Msg: '',
                MsgTitle: '',
                UserType:[]
            }
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        Save() {

            if (this.form.UserType.length == 0) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء  اختيار المستخدمين'
                });
                return;
            }
            if (!this.form.Msg) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال عنوان مناسب للتنبيه'
                });
                return;
            }
            if (!this.form.MsgTitle) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال محتوي  للتنبيه'
                });
                return;
            }
          

            this.$http.AddNotification(this.form)
                .then(response => {
                  
                    this.$parent.state = 0;
                    //this.$parent.GetNotifications();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                   
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });

        }
    }
}