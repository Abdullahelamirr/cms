﻿import addMainNotifications from './AddMainNotifications/AddMainNotifications.vue';

import moment from 'moment';
export default {
    name: 'MainNotifications',
    created () {
        this.GetUnReadNotificationsByUsers(this.pageNo);
    },
    components: {
        'add-Notifications': addMainNotifications
        
        //'edit-Companies': editCompanies
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            pageNo: 1,
            pageSize: 3,
            pages: 0,
            Read: -1,
            Notifications: [],
            dialogFormVisible : false,
                //{ Msg: 'Info', MsgTitle: 'حول الاجتماع' },
                //{ Msg: 'Info1', MsgTitle: '1حول الاجتماع' },
                //{ Msg: 'Info2', MsgTitle: '2حول الاجتماع' }],
            state: 0
            
        };
    },
    methods: {

        GetUnReadNotificationsByUsers(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            debugger;
            this.$http.GetUnReadNotificationsByUsers(this.pageNo, this.pageSize, this.$parent.IsReadNotification)
                .then(response => {
                    this.$blockUI.Stop();

                    this.Notifications = response.data.notifications;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },
        MarkAsRead(notificationObj) {

            this.$http.GetUnReadNotificationsByUsers(notificationObj)
                .then(response => {

                   // this.$parent.state = 0;
                    this.GetUnReadNotificationsByUsers(this.pageNo);
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {

                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
           
        //    console.log(notificationObj.msg + 'itsObj')
        //    console.log(dialogFormVisible + 'di')
        },

        AddUserNotifications(readStatus) {
            if (readStatus == this.Read) {
                this.Read = 0;
            } else {
                this.Read = readStatus;
            }

           
        }
    
        },
        AddNotificationsPage() {

            this.state = 1;
        },
        
    }
   
