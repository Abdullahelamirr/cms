﻿import UnreadNotification from './MainNotifications/MainNotifications.vue'
//import editCompanies from './EditCompanies/EditCompanies.vue';
import moment from 'moment';
import { debuglog } from 'util';
export default {
    name: 'Notifications',
    created () {
       
    },
    components: {
        'unRead-Notifications': UnreadNotification,
        //'edit-Companies': editCompanies
    },
    prpos:['IsReadNotification'],
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            pageNo: 1,
            pageSize: 5,
            pages: 0,
            IsReadNotification: false,
            Tab1: 'tab fancyTab active',
            Tab2: 'tab fancyTab ',
            TabContent1: 'tab-pane  fade',
            TabContent2: 'tab-pane  fade active in'
            
        };
    },
    methods: {

        OpentTab(tab) {
            
            if (tab == 1) {
                this.IsReadNotification = !this.IsReadNotification;
                this.Tab1 = 'tab fancyTab active';
                this.Tab2 = 'tab fancyTab';

                this.TabContent1 = 'tab-pane  fade active in';
                this.TabContent2 = 'tab-pane  fade';
            } else {
                this.IsReadNotification = !this.IsReadNotification;
                this.Tab2 = 'tab fancyTab active';
                this.Tab1 = 'tab fancyTab';

                this.TabContent2 = 'tab-pane  fade active in';
                this.TabContent1 = 'tab-pane  fade';
            }

        }
       
       
    }
   
};
