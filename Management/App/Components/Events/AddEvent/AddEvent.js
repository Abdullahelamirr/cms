﻿export default {
    name: 'AddEvents',    
    created() {
        this.GetHosts(this.pageNo);
        this.GetTrainers(this.pageNo);
        this.form.Discount = 0;
        this.form.Name = this.$parent.PackgeSelected.packageName;
        this.form.PriceCompany = this.$parent.PackgeSelected.priceCompany;
        this.form.PricePersonal = this.$parent.PackgeSelected.pricePersonal;
        this.form.Discount = this.$parent.PackgeSelected.discount;
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Hosts: [],
            Trainers: [],
            FromToDate: '',
            form: {
                Name: '',
                Description: '',
                StartDate: null,
                EndDate: null,
                ClassType: '',
                TeacherId: '',
                HostId: '',
                SuperPackageId: '',
                PackageId: '',
                CourseId: '',
                PriceCompany: '',
                PricePersonal: '',
                Discount: 0,
                MaximumNumberChair: '',
                EventType:''
            },
          
         
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        GetTrainers(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetTrainers(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Trainers = response.data.trainer;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
        GetHosts(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetLocations(this.pageNo, this.pageSize, null)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Hosts = response.data.locations;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },

        Save() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال اسم الحدث'
                });
                return;
            }

            if (!this.form.HostId) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء اختيار مكان الاستضافة'
                });
                return;
            }
            if (!this.form.TeacherId) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء اختيار اسم المدرب'
                });
                return;
            }

            if (!this.form.PricePersonal) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للأفراد'
                });
                return;
            }

            if (!this.form.PriceCompany) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للشركات'
                });
                return;
            }
            if (!this.form.MaximumNumberChair) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عدد الكراسي'
                });
                return;
            }
            if (!this.form.Discount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التخفيض'
                });
                return;
            }
            if (!this.FromToDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال المدة من - الي'
                });
                return;
            }
            
            if (this.$parent.EventType == 1) {
                this.form.SuperPackageId = this.$parent.PackageId;
            } else if (this.$parent.EventType == 2) {
                this.form.PackageId = this.$parent.PackageId;
            } else {
                this.form.CourseId = this.$parent.PackageId;
            }
            this.form.EventType = this.$parent.EventType; 

            this.form.StartDate = this.FromToDate[0];
            this.form.EndDate = this.FromToDate[1];
            this.$blockUI.Start();
            this.$http.AddEvents(this.form)
                .then(response => {
                    this.$blockUI.Stop();
                    this.$parent.state = 0;
                    this.$parent.GetEvents();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });



        }

       
    }    
}
