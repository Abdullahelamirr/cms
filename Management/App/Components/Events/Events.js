﻿import addEvents from './AddEvent/AddEvent.vue';
import editEvents from './EditEvents/EditEvents.vue';
import EventTrainingKit from '../EventTrainingKit/EventTrainingKit.vue';
import moment from 'moment';
export default {
    name: 'Events',
    created () {
        this.GetEvents(this.pageNo);
       
       // this.GetSuperPackagesContainsPackagesAndCourses();
    },
    components: {
        'add-Events': addEvents,
        'edit-Events': editEvents,
        'eventTrainingKit': EventTrainingKit

    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    props: {
        TrainingKitInfo: {
            type: Array
        }
    },
    data () {
        return {
            PackgeSelected: {},
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            state: 0,
            EventType:'',
            SPCId: '',
            TrainKitList:[],
            Events: [],
            dialogTableVisible: false,
            EventDetails: {},
            PackageText: '',
            PackageId: '',
            Package: [],
            SelectedId:''
        
        };
    },
    methods: {
        addEvents() {
            this.state = 1;
        },
        SelectPackage() {
            this.PackgeSelected = this.Package.find(x => x.packageId === this.PackageId);
            this.SPCId= this.PackageId
            this.GetEvents(this.pageNo);
        },
        GoToAgend(EventId) {  
            this.$router.push('/EventAgenda/' + EventId);
        },
        GoToPromotedKit(EventId) {
            this.$router.push('/EventsPromotFiles/' + EventId);
        },

        GoToFiles(Event) {
           
            this.state = 3;
            this.TrainKitList = Event;
            // this.$router.push('/EventTrainingKit/' + Event);

        },
        SelectEventType() {
            this.SPCId = '';
            this.Package = [];
            this.PackageId = '';
            this.GetEvents(this.pageNo);
            this.GetPackageByeventType();
            if (this.EventType==1) {
                this.PackageText = "بـاقة حزم";
            } else if (this.EventType==2) {
                this.PackageText = "حـزم";
            } else {
                this.PackageText = "دورات";
            }
        },
        ShowMore(Info) {
            this.EventDetails = Info;
        },
        GetPackageByeventType() {
            this.$http.GetPackageByeventType(this.EventType)
                .then(response => {
                    this.Package = response.data.package;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.pages = 0;
                });
        },

        GetEvents(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            // debugger;
            this.$http.GetEvents(this.pageNo, this.pageSize, this.EventType, this.SPCId)
                .then(response => {
                    this.$blockUI.Stop();
                   
                    this.Events = response.data.events;
                     this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },
      
    }
};
