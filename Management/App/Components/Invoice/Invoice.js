﻿//import addEvents from './AddEvent/AddEvent.vue';
//import editEvents from './EditEvents/EditEvents.vue';
import MainInvoice from './MainInvoice/MainInvoice.vue';
import moment from 'moment';
import ActivateStudent from './MainInvoice/ActivateStudent/ActivateStudent.vue';
export default {
    name: 'UserRegister',
    created () {
      //  this.GetEvents(this.pageNo);
       
       // this.GetSuperPackagesContainsPackagesAndCourses();
    },
    components: {
        'register-personal': MainInvoice,
        'active-student': ActivateStudent
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            PackgeSelected: {},
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            state: 0,
            EventType:'',
            SPCId: '',
            dialogTableVisible: false,
            EventDetails: {},
            PackageText: '',
            PackageId: '',
            Package: [],
            Tab1: 'tab fancyTab',
            Tab2: 'tab fancyTab active',
            TabContent1: 'tab-pane  fade',
            TabContent2: 'tab-pane  fade active in'
        
        };
    },
    methods: {
        OpentTab(tab) {
            if (tab == 1) {
                this.Tab1 = 'tab fancyTab active';
                this.Tab2 = 'tab fancyTab';

                this.TabContent1 = 'tab-pane  fade active in';
                this.TabContent2 = 'tab-pane  fade';
            } else {
                this.Tab2 = 'tab fancyTab active';
                this.Tab1 = 'tab fancyTab';

                this.TabContent2 = 'tab-pane  fade active in';
                this.TabContent1 = 'tab-pane  fade';
            }

        }


      
    }
};
