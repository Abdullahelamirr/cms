﻿import moment from 'moment';
export default {
    name: 'Payments',
    created() {       
     
        var obj = this.$parent.Payment;
        this.paidTotal = parseFloat(this.$parent.Payment.totalPriceAfterDescount).toFixed(2);


        this.GetPayments();
   
    },

    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {       
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            state: 0,
            SubscribType:'',
            Payments: [],
            SubscribeTxt: '',
            SubscriberId: '',
            Subscriber: [],
            dialogTableVisible: false,
            PaymentInvoice: {},
            paid: 0,
            SumPaids:0,
          paidTotal:0,
            form: {
                Paids:'',
                PaymentType: '',
                PaymentRefrence: '',
                NextPaidDate: '',
                InvoiceId:''
            }

        }
        
    },
    methods: {
        Back() {
            this.$parent.state = 0;
            this.$parent.GetInvoice(this.pageNo);
        },

        Paid() {
         
            if (parseFloat(this.$parent.Payment.totalPriceAfterDescount - this.SumPaids).toFixed(2) == 0) {
                this.$message({
                    type: 'error',
                    message: 'هذه فاتوره خالصة '
                });
                return;

            }
          
         
            if (!this.form.Paids) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال قيمة المبلغ'
                });
                return;

            } else if (parseFloat(this.form.Paids).toFixed(2)   > parseFloat(this.$parent.Payment.totalPriceAfterDescount - this.SumPaids).toFixed(2)  ){
                this.$message({
                    type: 'error',
                    message: 'الرجاء التاكد من قيمة المبلغ '
                });
                return;
            }
            if (!this.form.PaymentType) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال طريقة الدفع'
                });
                return;
            }

            if (parseFloat(this.$parent.Payment.totalPriceAfterDescount - this.SumPaids).toFixed(2)  - parseFloat(this.form.Paids) != 0) {
                if (!this.form.NextPaidDate) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إدخال تاريخ الدفعه القادمة'
                    });
                  return;  

                } }

            this.form.InvoiceId = this.$parent.Payment.invoiceId;
         
         
            this.$http.AddPayments(this.form)
                .then(response => {
                    this.state = 0;
                
                    this.form.Paids = '',
                        this.form.PaymentType = '',
                        this.form.PaymentRefrence = '',
                        this.form.NextPaidDate = '',
                       
                        this.GetPayments()
           
                 
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: "الرجاء التاكد من بيانات"
                    });
                });




       
        },
        DeletePaid(PaymentsId) {
            this.$confirm('سيؤدي ذلك إلى حذف الدفعات نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeletePaid(PaymentsId,this.$parent.Payment.invoiceId)
                    .then(response => {
                        if (this.Payments.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الدفعات بنجاح"
                        });
                        this.GetPayments();
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: "خطا "
                        });
                    });
            });

        },
          GetPayments() {
            this.$blockUI.Start();
              this.$http.GetPayments(this.$parent.Payment.invoiceId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Payments = response.data.payment;
                    this.SumPaids = response.data.sumPaid;
                    this.pages = response.data.count;
                 
                    
                  })
            
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.pages = 0;
                });

        },

       
      
    }
};
