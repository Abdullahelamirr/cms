﻿import moment from 'moment';
export default {
    name: 'AddNewActiveStudent',
    created() {

    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('DD-MM-YYYY');
        },
        EmptyString(Strings) {
            if (Strings === '') {
                return "غير متوفر";
            }
            return Strings;
        },

        Addmonth: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).add(1, 'M').format('DD-MM-YYYY');
        }
    },
    props: ['companyId'],
    data() {
        return {

            pageNo: 1,
            pageSize: 10,
            pages: 0,
            CompanyID: this.companyId,
            form: {
                NameAr: '',
                NameEn: '',
                Nationality: '',
                BirthDate: '',
                PhoneNumber1: '',
                PhoneNumber2: '',
                CurrentJob: '',
                Photo: '',
                ExperinceNumbers: '',
                Email: '',
                Location: '',
                CompanyId: ''

            },
            Selectedfile: '',
            isclose:false
        };
    },
    computed: {

        ChairesLeft() {
            return 0;

        }
    },
    methods: {
        CalculateChirsLeft() {
            return true;
        },
        FileChanged(e) {
            var files = e.target.files;
            this.Selectedfile = files[0].name;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                this.$message({
                    type: 'error',
                    message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                });
                this.photo = null;
                return;
            }

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.photo = reader.result;
                // $this.UploadImage();
            };
            reader.onerror = function (error) {
                $this.photo = null;
            };
            reader.readAsDataURL(files[0]);
        },

        Save() {

            //if (!this.form.NameAr) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الإسم بالعربي'
            //    });
            //    return;
            //}

            //if (!this.form.NameEn) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الاسم بالانجليزي'
            //    });
            //    return;
            //}

            //if (!this.form.Nationality) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الجنسـية'
            //    });
            //    return;
            //}

            //if (!this.form.BirthDate) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال تاريخ الميلاد'
            //    });
            //    return;
            //}
            //if (!this.form.PhoneNumber1) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال رقم الهاتف'
            //    });
            //    return;
            //}
            //if (!this.form.CurrentJob) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الرظيفة الحالية'
            //    });
            //    return;
            //}

            //if (!this.form.ExperinceNumbers) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال سنوات الخبرة'
            //    });
            //    return;
            //}

            //if (!this.form.Email) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال البريد الإلكتروني'
            //    });
            //    return;
            //}
            if (this.CompanyID == 0) {
                this.form.CompanyId = '';
            }
            else {
                this.form.CompanyId = this.CompanyID;
            }
           
            this.form.Photo = this.photo;
            
            this.$blockUI.Start();
            debugger;
            this.$http.AddStudent(this.form)
                .then(response => {

                    this.$blockUI.Stop();
                    //setInterval(() => {
                    //    this.$parent.GetCustomers(1);
                    //}, 1000);
                    this.cleartext();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });



        },

        Back() {

            this.$parent.dialogFormVisible = false
        },
        cleartext() {

                form.NameAr = '',
                form.NameEn = '',
                form.Nationality = '',
                form.BirthDate = '',
                form.PhoneNumber1 = '',
                form.PhoneNumber2 = '',
                form.CurrentJob = '',
                form.Photo = '',
                form.ExperinceNumbers = '',
                form.Email = '',
                form.Location = '',
                form.CompanyId = ''

        }

    }
}
