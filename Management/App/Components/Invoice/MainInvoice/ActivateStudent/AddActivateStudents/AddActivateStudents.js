﻿import moment from 'moment';

export default {
    name: 'AddActiveStudent',
    created() {

        this.CustomersInformationList = this.$parent.ActiveStudents;
        this.companyId = this.$parent.ActiveStudents.companyId;
        this.GetCustomers(this.pageNo);
        debugger;
        // this.CurrentStudentCount = this.$parent.ActiveStudents.numberOfChair;

        // setInterval(() => {
        //}, 1000);
        //console.log(this.CurrentStudentCounts + 'pages');
        //console.log(this.CustomerList + 'customerList');

    },
    //mounted() {
    //    console.log(this.CustomerList + 'customerListmounted');
    //},
    //components: {
    //    'add-new-active-student': AddNewActiveStudent
    //},
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('DD-MM-YYYY');
        },
        EmptyString(Strings) {
            if (Strings === '') {
                return "غير متوفر";
            }
            return Strings;
        },
        Addmonth: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).add(1, 'M').format('DD-MM-YYYY');
        }
    },
    data() {
        return {

            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Event: [],
            SubscriberId: '',
            state: 0,
            CustomerList: [],
            CustomerId: '',
            ActualStudentList: [],
            CountChairleft: 0,
            companyId: '',
            dialogFormVisible: false,
            SubscriberSelected: '',
            Studentindex: '',
            NowActiveStudents: [],
            CurrentStudentCount: 0,
            Selectedfile:'',
            form: {
                NameAr: '',
                NameEn: '',
                Nationality: '',
                BirthDate: '',
                PhoneNumber1: '',
                PhoneNumber2: '',
                CurrentJob: '',
                Photo: '',
                ExperinceNumbers: '',
                Email: '',
                Location: '',
                CompanyId: ''

            },

        };
    },
   
    methods: {
        cleartext() {

               this.form.NameAr = '',
               this.form.NameEn = '',
               this.form.Nationality = '',
               this.form.BirthDate = '',
               this.form.PhoneNumber1 = '',
               this.form.PhoneNumber2 = '',
               this.form.CurrentJob = '',
               this.form.Photo = '',
               this.form.ExperinceNumbers = '',
               this.form.Email = '',
               this.form.Location = '',
               this.form.CompanyId = ''

        },


        FileChanged(e) {
            var files = e.target.files;
            this.Selectedfile = files[0].name;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                this.$message({
                    type: 'error',
                    message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                });
                this.photo = null;
                return;
            }

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.photo = reader.result;
                // $this.UploadImage();
            };
            reader.onerror = function (error) {
                $this.photo = null;
            };
            reader.readAsDataURL(files[0]);
        },

        Savenew() {

            //if (!this.form.NameAr) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الإسم بالعربي'
            //    });
            //    return;
            //}

            //if (!this.form.NameEn) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الاسم بالانجليزي'
            //    });
            //    return;
            //}

            //if (!this.form.Nationality) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الجنسـية'
            //    });
            //    return;
            //}

            //if (!this.form.BirthDate) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال تاريخ الميلاد'
            //    });
            //    return;
            //}
            //if (!this.form.PhoneNumber1) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال رقم الهاتف'
            //    });
            //    return;
            //}
            //if (!this.form.CurrentJob) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال الرظيفة الحالية'
            //    });
            //    return;
            //}

            //if (!this.form.ExperinceNumbers) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال سنوات الخبرة'
            //    });
            //    return;
            //}

            //if (!this.form.Email) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء إدخال البريد الإلكتروني'
            //    });
            //    return;
            //}
            if (this.companyId == 0) {
                this.form.CompanyId = '';
            }
            else {
                this.form.CompanyId = this.companyId;
            }

            this.form.Photo = this.photo;

            this.$blockUI.Start();
            debugger;
            this.$http.AddStudent(this.form)
                .then(response => {

                    this.$blockUI.Stop();
                    this.GetCustomers(1);
                    this.cleartext();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });



        },

        NowActiveStudent(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            var studentList = [];
            for (var i in this.CustomerList) {

                studentList.push(this.CustomerList[i].studentId);
            }

            this.$http.GetActiveStudents(this.pageNo, this.pageSize,
                this.CustomersInformationList.eventId,
                [studentList], this.CustomersInformationList.invoiceId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.NowActiveStudents = response.data.activeStudents;
                    this.CurrentStudentCount = response.data.count;
                    this.CountChairleft = this.$parent.ActiveStudents.numberOfChair - this.CurrentStudentCount;


                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.nowEvent = 0;
                });
        },

        unique() {
            return this.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
        },

        //CalculateChirsLeft() {
        //   //// this.CountChairleft = this.ActualStudentList.map(a => a.ActualStudentId).length
        //   const surnames = this.ActualStudentList.map(a => a.ActualStudentId);
        //   // this.CountChairleft = this.unique(surnames);
        //    const unique = (value, index, self) => {
        //        return self.indexOf(value) === index;
        //    }
        //    this.CountChairleft = surnames.filter(unique).length;

        //},
        AddAsActiveStudent(StudentId) {

            var students = this.CustomerList.find(x => x.studentId === this.SubscriberId);

            var found = this.NowActiveStudents.some(function (el) {
                return el.studentId === StudentId;
            });
            if (!found) {
                //add
                var isfound = this.ActualStudentList.some(function (el) {
                    return el.studentId === StudentId;
                });
                if (!isfound) {

                    this.ActualStudentList.push({
                        studentId: students.studentId,
                        nameAr: students.nameAr,
                        nameEn: students.nameEn,
                        email: students.email

                    });
                    this.CountChairleft--;
                }
                else {
                    this.$message({
                        type: 'error',
                        message: 'المتدرب تم اختياره مسبقا'
                    });
                    return;
                }
            }
            else {
                this.$message({
                    type: 'error',
                    message: 'المتدرب موجود'
                });
                return;
            }

            //this.Studentindex=this.ActualStudentList.push({
            //    ActualStudentId: StudentId
            //}) -1;
            // this.backgrond = this.ActualStudentList[this.Studentindex].ActualStudentId;
            // this.CalculateChirsLeft();


        },
        deleteFromActiveStudentList(StudentId) {



            var found = this.ActualStudentList.some(function (el) {
                return el.studentId === StudentId;
            });

            if (!found) {
                console.log("ActualStudentId not found");
            } else {
                this.CountChairleft++;
                for (var i in this.ActualStudentList) {
                    if (this.ActualStudentList[i].studentId === StudentId) {

                        var removeIndex = this.ActualStudentList.map(function (item) { return item.studentId; })
                            .indexOf(this.ActualStudentList[i].studentId);
                        ~removeIndex && this.ActualStudentList.splice(removeIndex, 1);
                        break;//Stop this loop, we found it!
                    }

                }
            }

        },

        DeleteActiveStudent(StudentId) {
            this.$confirm('سيؤدي ذلك إلى حذف المتدرب نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {

                this.$http.DeleteActiveStudent(StudentId, this.CustomersInformationList.eventId,
                    this.CustomersInformationList.invoiceId)
                    .then(response => {
                        if (this.CustomerList.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح المتدرب بنجاح"
                        });
                        this.GetCustomers(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });

        },


        GetCustomers(pageNo) {

            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();

            this.$http.GetCustomers(this.pageNo, this.pageSize,
                this.CustomersInformationList.customerType,
                this.CustomersInformationList.companyId, this.CustomersInformationList.studentId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.CustomerList = response.data.student;
                    this.pages = response.data.count;
                    this.NowActiveStudent(this.pageNo);
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },
        Add() {

            this.dialogFormVisible = true;

            //if (this.companyId != 0) {
            //    this.$router.push(`/Students/${this.companyId}`);
            //}


            //if (!this.form.EventId) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء ادخال اسم الحدث'
            //    });
            //    return;
            //}

            //if (!this.form.NumberOfChair) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء ادخال عدد المقاعد'
            //    });
            //    return;
            //}

            //if (!this.form.TotalPrice) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء ادخال السعر'
            //    });
            //    return;
            //}



            //if (!this.form.TotalPriceAfterDescount) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء ادخال السعر النهائي'
            //    });
            //    return;
            //}

        },

        Save() {
            //if (!this.CountChairleft) {
            //    this.$message({
            //        type: 'error',
            //        message: 'الرجاء اخيار طلبه'
            //    });
            //    return;
            //}

            if (this.CountChairleft > this.$parent.ActiveStudents.numberOfChair) {
                this.$message({
                    type: 'error',
                    message: 'لقد تجاوزت عدد المقاعد المحجوزه'
                });
                return;
            }


            let Count = 0;
            for (var i in this.ActualStudentList) {
                if (this.ActualStudentList.hasOwnProperty(i)) Count++;
            }

            this.$confirm(' هل حقا تريد حفظ عدد متدرب ' + Count + ' استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                //    var Register = 0;
                //    if (this.RegistryType == true) {
                //        Register = 1;
                //    } 
                this.$http.AddActiveStudents(this.$parent.ActiveStudents.invoiceId,
                    this.$parent.ActiveStudents.eventId,
                    this.ActualStudentList)
                    .then(response => {
                        //this.$parent.state = 0;
                        this.ActualStudentList = [];
                        this.GetCustomers(this.pageNo);
                        this.$message({
                            type: 'info',
                            message: response.data
                        });
                    })
                    .catch((err) => {

                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });


            });

        },

        Back() {
            this.$parent.state = 0;
        },


    }
}
