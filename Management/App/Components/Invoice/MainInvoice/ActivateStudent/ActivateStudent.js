﻿import moment from 'moment';
import AddActiveStudent from './AddActivateStudents/AddActivateStudents.vue';
export default {
    name: 'ActivateStudent',
    created() {

        this.GetEventsV3();
        this.GetInvoicesByEventId(this.pageNo);
    },
    components: {
        'add-activeStudent': AddActiveStudent
    },
   
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('DD-MM-YYYY');
        },
        Addmonth: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).add(1, 'M').format('DD-MM-YYYY');
        }
    },
    data() {
        return {
            pageNo: 1,
            pageSize:4,
            pages: 0,
            state: 0,
            Events: [],
            EventId: '',
            InvoicesEvents: [],
            SubscriberType: '',
            InvoiceIdChild: 0,
            SubscribeTxt: '',
            SubscriberId: '',
            Subscriber: [],
            ActiveStudents:[]

        };
    },
    methods: {
      
        AddActiveStudent(invoiceEvent) {
            console.log('this invoiceEvent is ', invoiceEvent);
            this.ActiveStudents = invoiceEvent;
                this.state = 1;
        },

        SelectSubscriber() {
            this.GetInvoicesByEventId();
        },

        SelectEvent() {
            var Event = this.Events.find(x => x.eventId === this.EventId);
            this.GetInvoicesByEventId(this.pageNo)
            console.log('this event is ' ,Event);
        },
        SelectSubscribeType() {
            this.SubscriberId = '';
            this.Subscriber = [];
            if (this.SubscriberType == 1) {
                this.GetSubscribersByEventId();
                this.GetInvoicesByEventId();
                this.SubscribeTxt = 'اختيار الشركة';
            } else {
                this.GetSubscribersByEventId();
                this.GetInvoicesByEventId();
                this.SubscribeTxt = 'اختيار الفرد';
            }
        },
        GetSubscribersByEventId() {
            
            this.$http.GetSubscribersByEventId(this.EventId, this.SubscriberType)
                .then(response => {
                    this.Subscriber = response.data.subscriber;

                })
                .catch((err) => {

                });

        },
        GetEventsV3() {
            this.$http.GetEventsV3()
                .then(response => {
                    this.Events = response.data.events;
                })
                .catch((err) => {
                    console.log(err);
                    this.pages = 0;
                });
        },
        GetInvoicesByEventId(pageNo) {
          
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
           
            this.$http.GetInvoicesByEventId(this.pageNo, this.pageSize, this.EventId, this.SubscriberType, this.SubscriberId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.InvoicesEvents = response.data.invoices;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        }
    }
   
}
