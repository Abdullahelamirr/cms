﻿import Packages from "../../../Packages/Packages";

export default {
    name: 'AddMaininvoice',    
    created() {
        this.GetEventsV3();
        //this.GetHosts(this.pageNo);
        //this.GetTrainers(this.pageNo);
        //this.form.Discount = 0;
        //this.form.PriceCompany = this.$parent.PackgeSelected.priceCompany;
        //this.form.PricePersonal = this.$parent.PackgeSelected.pricePersonal;
        //this.form.Discount = this.$parent.PackgeSelected.discount;
    },
    data() {
        return {
            AllDiscount:0,
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Event: [],
            Trainers: [],
            FromToDate: '',
            form: {
                EventId: '',
                NumberOfChair: 1,
                Discount: '',
                TotalPrice: '',
                TotalPriceAfterDescount:''
            },
            TotalPriceAll: '',
            TotalPriceAfterDescountAll: '',
            RegistryType:false,
            invoiceList:[]
          
         
        };
    },
    methods: {
        
        Add() {
            if (!this.form.EventId) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال اسم الحدث'
                });
                return;
            }

            if (!this.form.NumberOfChair) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال عدد المقاعد'
                });
                return;
            }

            if (!this.form.TotalPrice) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال السعر'
                });
                return;
            }

         

            if (!this.form.TotalPriceAfterDescount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء ادخال السعر النهائي'
                });
                return;
            }
            var Event = this.Event.find(x => x.eventId === this.form.EventId);
          
            this.invoiceList.push({
                Event: Event.name,
                EventId: this.form.EventId,
                NumberOfChair: this.form.NumberOfChair,
                Discount: this.form.Discount,
                TotalPrice: parseFloat(this.form.TotalPrice).toFixed(2) ,
                TotalPriceAfterDescount: parseFloat(this.form.TotalPriceAfterDescount).toFixed(2)
            });
            this.TotalPriceAll = parseFloat(this.sumAll(this.invoiceList, 'TotalPrice')).toFixed(2);
            this.TotalPriceAfterDescountAll = parseFloat(this.sumAll(this.invoiceList, 'TotalPriceAfterDescount')).toFixed(2);
            var x = (this.sumAll(this.invoiceList, 'TotalPrice') - this.sumAll(this.invoiceList, 'TotalPriceAfterDescount'));
            var y = ((this.sumAll(this.invoiceList, 'TotalPrice') + this.sumAll(this.invoiceList, 'TotalPriceAfterDescount')) / 2);
            this.AllDiscount = parseInt(((x / y) * 100));
            this.form.EventId = '';
            this.form.NumberOfChair = 1;
            this.form.Discount = '';
            this.form.TotalPrice = '';
            this.form.TotalPriceAfterDescount = '';
            
        },

        Save() {
           
            this.$confirm(' هل حقا تريد حفظ الفاتورة بقيمة ' + parseFloat(this.TotalPriceAfterDescountAll).toFixed(2)+'د.ل استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                var Register = 0;
                if (this.RegistryType == true) {
                    Register = 1;
                } 
                this.$http.AddInvoice(this.$parent.SubscriberId, this.$parent.SubscribType, Register, this.TotalPriceAll, this.AllDiscount, this.TotalPriceAfterDescountAll, this.invoiceList)
                    .then(response => {
                    this.$parent.state = 0;
                        this.$parent.GetInvoice();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                   
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });


            });

        },

        sumAll(items, prop) {
            return items.reduce(function (a, b) {
                return parseFloat(a) + parseFloat(b[prop]);
            }, 0);
        },

        KeypressDiscountAll(value) {
            if (!value) {
                value = 0;
            }
            this.TotalPriceAfterDescountAll = parseFloat(this.TotalPriceAll - (this.TotalPriceAll * (value / 100))).toFixed(2);
        },


        KeypressDiscount(value) {
            if (value >= 0) {
                this.form.TotalPriceAfterDescount = parseFloat((this.form.TotalPrice - (this.form.TotalPrice * (value / 100)))).toFixed(2);
            }
        },

        Keypress(value) {
            var Event = this.Event.find(x => x.eventId === this.form.EventId);
            console.log(Event);
            if (value>1) {
                //this.form.TotalPrice = this.form.TotalPrice * value;
                //this.form.TotalPriceAfterDescount = this.form.TotalPriceAfterDescount * value;
                if (this.$parent.SubscribType == 1) {
                    this.form.Discount = Event.discount;
                    this.form.TotalPrice = Event.priceCompany * value;
                    this.form.TotalPriceAfterDescount = parseFloat((Event.priceCompany - (Event.priceCompany * (Event.discount / 100))) * value).toFixed(2);

                } else {
                    this.form.Discount = Event.discount;
                    this.form.TotalPrice = Event.pricePersonal * value;;
                    this.form.TotalPriceAfterDescount = parseFloat((Event.pricePersonal - (Event.pricePersonal * (Event.discount / 100))) * value).toFixed(2);
                }
            } else if (value<=0) {
                this.form.NumberOfChair = 1;
            }else {
                if (this.$parent.SubscribType == 1) {
                    this.form.Discount = Event.discount;
                    this.form.TotalPrice = Event.priceCompany;
                    this.form.TotalPriceAfterDescount = parseFloat(Event.priceCompany - (Event.priceCompany * (Event.discount / 100))).toFixed(2);
                } else {
                    this.form.Discount = Event.discount;
                    this.form.TotalPrice = Event.pricePersonal;
                    this.form.TotalPriceAfterDescount = parseFloat(Event.pricePersonal - (Event.pricePersonal * (Event.discount / 100))).toFixed(2);                
                }
            }
            
        },
        SelectEvent() {
            this.form.NumberOfChair = 1;
            var Event = this.Event.find(x => x.eventId === this.form.EventId);
            // Company or personal
            if (this.$parent.SubscribType==1) {
                this.form.Discount = Event.discount;
                this.form.TotalPrice = Event.priceCompany;
                this.form.TotalPriceAfterDescount = Event.priceCompany - (Event.priceCompany * (Event.discount / 100));
                
            } else {
                this.form.Discount = Event.discount;
                this.form.TotalPrice = Event.pricePersonal;
                this.form.TotalPriceAfterDescount = Event.pricePersonal - (Event.pricePersonal * (Event.discount / 100));
            }
        },
        Back() {
            this.$parent.state = 0;
        },

        GetTrainers(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetTrainers(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Trainers = response.data.trainer;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
        GetEventsV3() {
            this.$http.GetEventsV3()
                .then(response => {
                    this.Event = response.data.events;
                })
                .catch((err) => {
                    console.log(err);
                    this.pages = 0;
                });
        }
    }    
}
