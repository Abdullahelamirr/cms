﻿import addInvoice from './AddMainInvoice/AddMainInvoice.vue';
import editInvoice from './EditMainInvoice/EditMainInvoice.vue';
import Payments from './Payments/Payments.vue';
import viewInvoice from './ViewMainInvoice/ViewMainInvoice.vue';
import moment from 'moment';
import Invoice from '../Invoice';
export default {
    name: 'MainInvoice',
    created() {
        this.GetInvoice(this.pageNo);
    },
    components: {
        'add-Invoice': addInvoice,
        'edit-Invoice': editInvoice,
        'Payments': Payments,
        'viewInvoice': viewInvoice
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            state: 0,
            SubscribType: '',
            InvoiceIdChild: 0,
            Invoices: [],
            SubscribeTxt: '',
            SubscriberId: '',
            Subscriber: [],
            dialogTableVisible: false,
            PaymentInvoice: {},
            InvoiceDetails: [],
            Payment: [],
            EditInvocie: [],
        };
    },
    methods: {

        BackToFirst() {
            
            this.state = 0;
           this.SubscribType = '';
           this.SubscribeTxt = '';
           this.SubscriberId = '';
            this.Subscriber = [];
            this.GetInvoice(this.pageNo);
        },

        ShowMore(InvoiceId) {

            this.GetInvoiceDetails(InvoiceId);

        },
        DeleteInvoice(InvoiceId, invoicePaid) {
            this.$confirm('سيؤدي ذلك إلى حذف الفاتوره نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {

                var paid = invoicePaid;
                if (paid != 0) {
                    this.$message({
                        type: 'info',
                        message: "الرجاء الغاء الدفعات قبل"
                    });
                    return;
                };

                this.$http.DeleteInvoice(InvoiceId)
                    .then(response => {
                        if (this.Invoices.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الفاتوره بنجاح"
                        });
                        this.GetInvoice(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });

        },
        GetInvoiceDetails(InvoiceId) {


            this.$http.GetInvoiceDetails(InvoiceId)
                .then(response => {


                    this.EditInvocie = response.data.invoice;
                    this.state = 2;

                })
                .catch((err) => {
                    console.log(err);

                });

        },
        Payments(Invoice) {

            this.Payment = Invoice;
            this.state = 3;
        },
        EditInvoice(InvoiceId) {

            this.GetInvoiceDetails(InvoiceId);
            this.InvoiceIdChild = InvoiceId;

            this.state = 2;
        },
        ViewInvoice(InvoiceId) {
            this.InvoiceIdChild = InvoiceId;
            this.state = 4;
        },
        ChangeStatus(invoice, status, invoicePaid) {
            var txt = '';
            if (status == 1) {
                var txt = 'سيؤذي ذلك لارجاع الفاتورة الي الحالة المبدئية';
            } else {
                var txt = 'سيؤدي ذلك إلى تأكيد الفاتورة نهائيا. استمر؟';
            }

            this.$confirm(txt, 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                var paid = invoicePaid;
                if (paid != 0) {
                    this.$message({
                        type: 'info',
                        message: "الرجاء الغاء الدفعات قبل"
                    });
                    return;
                };

                this.$http.ConfirmInvoice(invoice.invoiceId, status)
                    .then(response => {
                        //this.Subscriber = response.data.subscriber;
                        this.GetInvoice(this.pageNo);
                        this.$message({
                            type: 'info',
                            message: 'تـم تأكيد الفاتورة بنجـاح'
                        });

                    })
                    .catch((err) => {

                    });

            });
        },

        SelectSubscribeType() {
            this.SubscriberId = '';
            this.Subscriber = [];
            if (this.SubscribType == 1) {
                this.GetSubscriber();
                this.GetInvoice();
                this.SubscribeTxt = 'اختيار الشركة';
            } else {
                this.GetSubscriber();
                this.GetInvoice();
                this.SubscribeTxt = 'اختيار الفرد';
            }
        },
        SelectSubscriber() {
            this.GetInvoice();
        },
        addSubscriber() {
            this.state = 1;
        },
        GetSubscriber() {
            this.$http.GetSubscriber(this.SubscribType)
                .then(response => {
                    this.Subscriber = response.data.subscriber;

                })
                .catch((err) => {

                });

        },
        GetInvoice(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();

            this.$http.GetInvoice(this.pageNo, this.pageSize, this.SubscribType, this.SubscriberId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Invoices = response.data.invoice;
                    this.PaymentInvoice = response.data;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.pages = 0;
                });

        },

    }
};
