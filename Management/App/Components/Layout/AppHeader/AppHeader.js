﻿export default {
    name: 'AppHeader',    
    created() { 
        this.GetNotificationInHeader();

        this.GetMessagesUnRead();
        setInterval(() => this.GetMessagesUnRead(), 10000);       
    },
    data() {
        return {            
            loginDetails: null,
            active: 1,
            counts: 0,
            UnReadCount: 0
        };
    },
  
    methods: {
        Logout() {
            window.location.href = '/Users/Login';
        },
        OpenDropDownUser() {
            var root = document.getElementById("DropDownUser");
            if (root.getAttribute('class') == 'dropdown') {
                root.setAttribute('class', 'dropdown open');
            } else {
                root.setAttribute('class', 'dropdown');
            }

        },
        GetMessagesUnRead() {
            this.$http.GetMessages(1)
                .then(response => {
                    this.UnReadCount = response.data.unred;
                })
                .catch((err) => {
                });
        },
        href(url) {
            this.$router.push(url);
        },
        OpenDropDown() {
            var root = document.getElementById("DropDown");
            if (root.getAttribute('class') == 'dropdown') {
                root.setAttribute('class', 'dropdown open');
            } else {
                root.setAttribute('class', 'dropdown');
            }

            this.GetNotificationInHeader();
        },


        GetNotificationInHeader() {

                this.$blockUI.Start();
            this.$http.GetUnReadNotificationsInHeader()
                .then(response => {
                    this.$blockUI.Stop();
                    this.Notifications = response.data.notifications;
                    this.counts = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        // ********************** Template InterActive ***********
        OpenMenuByToggle() {
            var root = document.getElementsByTagName('html')[0]; // '0' to assign the first (and only `HTML` tag)
            if (root.getAttribute('class') == 'nav-open') {
                root.setAttribute('class', '');
            } else {
                root.setAttribute('class', 'nav-open');
            }
        },
        OpenNotificationMenu() {
            var root = document.getElementById("Notifications");
            if (root.getAttribute('class') == 'dropdown open') {
                root.setAttribute('class', 'dropdown');
            } else if (root.getAttribute('class') == 'dropdown') {
                root.setAttribute('class', 'dropdown open');
            }
        }
        //****************************************************************

      
    }    
}
