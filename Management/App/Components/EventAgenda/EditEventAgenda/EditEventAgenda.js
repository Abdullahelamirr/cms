﻿
import moment from 'moment';
export default {
    name: 'EditEventAgenda',
    created () {
        this.EditAgendaInfo();
    },
    
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
           
               
                pageNo: 1,
                FromToDate:[],
                form: {
                    Title: '',
                    Description: '',
                    DateTimeFrom: '',
                    DateTimeTo: '',
                    FromToDate: '',
                    EventId: 0,

                },
           
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        EditAgendaInfo() {
            debugger;
            var obj = this.$parent.EditAgenda;
            if (obj != null) {
                debugger;
                this.form.Title = obj.title;
                this.form.Description = obj.description;
                this.form.DateTimeFrom = moment(obj.dateTimeFrom).format("hh:mm:ss");
                this.form.DateTimeTo = moment(obj.dateTimeTo).format('hh:mm:ss');
                this.form.EventId = obj.eventId;
                this.form.AgendaId = obj.agendaId;
                this.form.FromToDate = moment(obj.fromToDate).format('YYYY-MM-DD HH:mm');
               
                //this.FromToDate.push(this.form.DateTimeTo);
            } else {
                this.$parent.state = 0;
            }
            
        },
        edit() {
            if (!this.form.Title) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عنوان النشاط المقام'
                });
                return;
            }

            if (!this.form.Description) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال معلومات جول النشاط المقام'
                });
                return;
            }

            if (!this.form.FromToDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التاريخ '
                });
                return;
            }

            if (!this.form.DateTimeFrom) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال  بدايه التوقيت'
                });
                return;
            }
            if (!this.form.DateTimeTo) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال نهايه وقت النشاط'
                });
                return;
            }
            this.form.FromToDate = moment(this.form.FromToDate).format('YYYY-MM-DD HH:mm');
            this.$http.EditAgenda(this.form)
                .then(response => {
                    this.$parent.state = 0;

                    this.$parent.GetAgendByEventId(1);
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });


        },
    }
  
};
