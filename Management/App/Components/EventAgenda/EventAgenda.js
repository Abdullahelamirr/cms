﻿import addEventAgenda from './AddEventAgenda/AddEventAgenda.vue';
import editEventAgenda from './EditEventAgenda/EditEventAgenda.vue';
import moment from 'moment';
export default {
    name: 'EventAgenda',
    created() {
        // debugger;
        // get Agenda By Eventid
        // use this id to get agend details and add new one
        console.log(this.$route.params.EventId);
        //first task
        this.GetAgendByEventId(this.pageNo);
        //this.GetEveGetEventsntsV2(this.pageNo, this.$route.params.EventId);


    },
    components: {
        'add-EventAgenda': addEventAgenda,
        'edit-EventAgenda': editEventAgenda
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('YYYY-MM-DD');
        },
        fromnow(date) {
            return moment(date).format('a hh:mm')
            //moment(date).toNow();
        },
        dayss(date) {
            return moment(date).format("dddd, MMMM Do YYYY");
        },

        days(date) {
            return moment(date).format('DD')
            //moment(date).toNow();
        },
        months(date) {
            return moment(date).format('MMMM')
            //moment(date return moment(date)).toNow();
        },
        dayofweek(date) {
            return moment(date).format('dddd');
        }

    },
    data() {
        return {
            pageNo: 1,
            pageSize: 3,
            pages: 0,
            state: 0,
            EventId: '',
            Agenda: [],
            Events: [],
            Iscourse: '',
            today: moment(),
            obj: {},
            gropby: []
           
        };
    },
    methods: {
        groupBy: function (objectArray, property) {
            
            return objectArray.reduce(function (acc, obj) {
               
                var key =  obj[property] ;
                //   var key = moment(obj[property]).format('YYYY-MM-DD');
                if (!acc[key]) {
                    acc[key] = [];
                }
               
                acc[key].push(obj);
               
                return acc;
            }, []);
        },

        Back() {

            this.$router.push('/Events');
            this.state = 0;
        },
        AddAgendaPage() {
            this.state = 1;
        },
        GetEvents(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            // debugger;
            this.$http.GetEventsV2(this.pageNo, this.$route.params.EventId)
                .then(response => {
                    this.$blockUI.Stop();

                    this.Events = response.data.events;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },
        GetAgendByEventId(pageNo) {
           
           // debugger;
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.EventId = this.$route.params.EventId;
            this.$blockUI.Start();

            this.$http.GetAgendByEventId(this.pageNo, this.pageSize, this.EventId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Agenda = response.data.eventAgenda;
                    this.pages = response.data.count;
                   
                    this.gropby= this.groupBy(this.Agenda, 'fromToDate');
                 
                    if (!!this.Agenda[0].course) {
                        this.Iscourse = this.Agenda[0].course;
                    }
                    if (!!this.Agenda[0].package) {
                        this.Iscourse = this.Agenda[0].package;
                    }
                    if (!!this.Agenda[0].superPackage) {
                        this.Iscourse = this.Agenda[0].superPackage;
                    }

                    console.log(this.Iscourse);
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },
        GetEventsAgenda(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            // debugger;
            this.$http.GetEventsAgenda(this.pageNo, this.pageSize, this.EventId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Agenda = response.data.eventAgenda;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },

        DeleteAgenda(AgendaId) {
           // debugger;
            this.$confirm('سيؤدي ذلك إلى النشاط الحالي نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteAgenda(AgendaId)
                    .then(response => {
                        if (this.Agenda.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: 'تم مسح النشاط بنجاح'
                        });
                        this.GetAgendByEventId(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
        EditAgendaInfo(Agenda) {
            this.EditAgenda = Agenda;
            this.state = 2;
        },


       
    }
}
