﻿
import moment from 'moment';


export default {
    name: 'AddEventAgenda',
    created() {
        this.$route.params.EventId
    },

    data() {
        return {
            FromToDate: '',
            pageNo: 1,
            
            form: {
                Title: '',
                Description: '',
                DateTimeFrom: '',
                DateTimeTo: '',
                FromToDate:'',
                EventId: 0,
               
            },
        };

    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },
        Save() {
           
            debugger;
           
            //this.form.DateTimeTo = moment(this.DateTimeTo).format('YYYY-MM-DD HH:mm');
            //const momentDate = momentTz(this.form.DateTimeFrom, 'YYYY-MM-DD HH:mm')
            //const momentDateEnd = momentTz(this.form.DateTimeTo, 'YYYY-MM-DD HH:mm')

          //  const momentDateTz = momentTz.tz(momentDate, 'YYYY-MM-DD HH:mm', timeZone)
           // console.log(momentDate); console.log(momentDateEnd);
            if (!this.form.Title) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عنوان النشاط المقام'
                });
                return;
            }

            if (!this.form.Description) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال معلومات جول النشاط المقام'
                });
                return;
            }

            if (!this.form.FromToDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التاريخ '
                });
                return;
            }

            if (!this.form.DateTimeFrom) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال  بدايه التوقيت'
                });
                return;
            }
            if (!this.form.DateTimeTo) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال نهايه وقت النشاط'
                });
                return;
            }
            debugger;
            this.form.EventId = this.$route.params.EventId;
            this.form.FromToDate = moment(this.form.FromToDate ).format('YYYY-MM-DD HH:mm');
            this.$http.AddEventAgenda(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    //this.$router.push('/EventAgenda/' + EventId);
                    this.$parent.GetAgendByEventId(1);
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });

       
        },

        DeleteAgenda(AgendaId) {

        }


    }
}
