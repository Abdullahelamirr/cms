﻿
import moment from 'moment';
export default {
    name: 'ShowMessages',
    created () {
        this.GetMessagesUnRead();
    },
    //components: {
    //    'add-Municipals': AddMunicipals,
       
    //},
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            Messages: [],
            UnReadCount: 0,
            ReadCount: 0,
            later: 0,
            all:0,
            IsReadOrNot:1
        };
    },
    methods: {
        Notification(status) {
            this.IsReadOrNot = status;
        },

        MarkAsUnRead(Message) {
            this.$confirm('هل حقا تريد حفظ التنبيه كغير مقروء من قبل . استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.MarkMessages(Message.messageTransactionId, 1)
                    .then(response => {
                        this.$message({
                            type: 'info',
                            message: 'تم تعديل حالة القراء بنجاح'
                        });
                        this.GetMessagesUnRead();
                        this.$blockUI.Stop();
                    })
                    .catch((err) => {
                        console.log(err);
                        this.$blockUI.Stop();
                    });

            });

        },

        MarkAsRead(Message) {
            this.$confirm('هل حقا تريد حفظ التنبيه كمقروء من قبل . استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.MarkMessages(Message.messageTransactionId, 2)
                    .then(response => {
                        this.$message({
                            type: 'info',
                            message: 'تم الاضافة للقراءة لاحقا بنجاح'
                        });
                        this.GetMessagesUnRead();
                        this.$blockUI.Stop();
                    })
                    .catch((err) => {
                        console.log(err);
                        this.$blockUI.Stop();
                    });

            });

        },

        SaveForLater(Message) {
            
            this.$confirm('هل حقا تريد حفظ التنبيه للقراءة لاحقا . استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {             
                this.$http.MarkMessages(Message.messageTransactionId,3)
                    .then(response => {
                        this.$message({
                            type: 'info',
                            message: 'تم الاضافة للقراءة لاحقا بنجاح'
                        });
                        this.GetMessagesUnRead();
                        this.$blockUI.Stop();
                    })
                    .catch((err) => {
                        console.log(err);
                        this.$blockUI.Stop();
                    });

            });
        },
        //GetMessages
        GetMessagesUnRead() {
            this.$blockUI.Start();
            this.$http.GetMessages(1)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Messages = response.data.messageList; 
                    this.UnReadCount = response.data.unred; 
                    this.ReadCount = response.data.red; 
                    this.all = response.data.all; 
                    this.later = response.data.later; 
                    this.$blockUI.Stop();
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                });
        }
       
   





    }
};
