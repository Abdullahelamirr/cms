﻿import Editor from '../../../Shared/Editor/Editor.vue';
import moment from 'moment';
export default {
    name: 'SentMessages',
    created() {
        this.Permissions = [
            {
                id: 1,
                name: "المدير"
            },
            {
                id: 2,
                name: 'المبيعات'
            },
            {
                id: 3,
                name: 'قسم التنسيق'
            },
            {
                id: 4,
                name: 'المشرف العام'
            }

        ];
       
    },
    components: {
        'editor': Editor    
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            Permissions: [],
            PermissionModale:[],
            options: [{
                value: 'Option1',
                label: 'Option1'
            }, {
                value: 'Option2',
                label: 'نورالدين الهنشيري'
            }, {
                value: 'Option3',
                label: 'محمد الامير'
            }, {
                value: 'Option4',
                label: 'عبدالله الامير'
            }, {
                value: 'Option5',
                label: 'Option5'
            }],
            value5: [],
            value11: [],
            SentType: null,
            text: null,
            Subject:null
        };
    },
    methods: {
        GetUsers(ByUserType) {

        },
        SelectedPermission() {

        },


        Sent() {
            this.$blockUI.Start();
            
            if (!this.Subject) {
                this.$blockUI.Stop();
                this.$notify.error({
                    title: 'Error',
                    message: 'الـرجاء إدخال العنوان'
                });
                return;
            }
            if (!this.text) {
                this.$blockUI.Stop();
                this.$notify.error({
                    title: 'Error',
                    message: 'الـرجاء إدخال محتوي الرسالة'
                });
                return;
            }
            let size = this.text.toString();
            if (size.length<=20) {
                this.$blockUI.Stop();
                this.$notify.error({
                    title: 'Error',
                    message: 'الـرجاء تعبئة محتوي الرسالة'
                });
                return;
            }


            if (this.PermissionModale.length == 0) {
                this.$blockUI.Stop();
                this.$notify.error({
                    title: 'Error',
                    message: 'الـرجاء ادخال المجموعة المستهدفة'
                });
                return;
            }
            var obj = {
                PayLoad: this.text.toString(),
                PermissionModale: this.PermissionModale,
                Subject: this.Subject
            };
            this.$http.Sent(obj)
                .then(response => {      
                    this.$message({
                        type: 'info',
                        message: response.data
                    });

                    // Using DOM element To clear the code 
                    this.ClearTextEditor();
                    //Multi Select 
                    this.PermissionModale = [];
                    this.SentType = null;
                    this.Subject = null;
                    this.$blockUI.Stop();
                   
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        }, 

        ClearTextEditor() {       
            var element = document.getElementsByClassName("ql-editor");
            element[0].innerHTML = "";      
        }






    }
};
