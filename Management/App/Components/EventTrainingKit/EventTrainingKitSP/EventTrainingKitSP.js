﻿export default {
    name: 'EventTrainingKitSP',
    created() {
        console.log(this.$route.params.SelectedPackageId + 'ChildTrainingKitSPparent');
        if (this.$route.params.classType == 1) {
            console.log(this.$route.params.classType + 'classType1');
            this.SPId = this.$route.params.SelectedPackageId;
        } else if (this.$route.params.classType == 2) {
            console.log(this.$route.params.classType + 'classType2');
            this.SUBId = this.$route.params.SelectedPackageId;
        } else {
            console.log(this.$route.params.classType + 'classType3');
            this.CourseId = this.$route.params.SelectedPackageId;
        }
      
    },

    props: {
        TrainingKitInfo: {
            type: Array
        }
    },
    data() {
        return {
            SPId: null,
            SUBId: null,
            CourseId: null
            
        };
    },
    methods: {
        GetSuperPackages(SPId) {
           
            this.$blockUI.Start();
            this.$http.GetPackages(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.SuperPackages = response.data.superPackage;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

    }
}
