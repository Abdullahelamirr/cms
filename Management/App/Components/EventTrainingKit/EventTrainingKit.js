﻿import EventTrainingKitSP from './EventTrainingKitSP/EventTrainingKitSP.vue';

export default {
    name: 'EventTrainingKit',
    created() {

       
           

        this.objPackage.packageId = this.TrainingKitList.packageId;
        this.objPackage.superPackageId = this.TrainingKitList.superPackageId;
        this.objPackage.courseId = this.TrainingKitList.courseId;
        this.objPackage.classType = this.TrainingKitList.classType;
        this.GetPackages(this.objPackage)

    },
    components: {
        'eventTrainingKitSP': EventTrainingKitSP

    },
    props: {
        TrainingKitInfo: {
            type: Object
        }
    },
    computed: {
        TrainingKitList() {

            return this.TrainingKitInfo;
        }
    },
    data() {
        return {
            IconUp: 'glyphicon glyphicon-chevron-down',
            isActive: null,
            Test: [
                {
                    id: 1,
                    name: 'abdullah',
                    array: [
                        { x: 1 },
                        { x: 2 },
                    ]
                },
                {
                    id: 2,
                    name: 'Marwa',
                    array: [
                        { x: 44 },
                        { x: 66 },
                    ]
                },
                {
                    id: 3,
                    name: 'Asma',
                    array: [
                        { x: 33 },
                        { x: 55 },
                    ]
                }
            ],
            collapsed: false,
            Packages: [],
            objPackage: {
                packageId: null,
                superPackageId: null,
                courseId: null,
                classType: null
            },
            FAQflage: 0,
            CRflage: 0,
            rawHtml: `<div style="color:red;"></div>`

        };
    },
    methods: {
        OpenPanel(id) {
            console
            if (id == this.isActive) {
                this.isActive = -1;
            } else {
                this.isActive = id;
            }
        },


        openFAQ(num) {
            if (num == this.FAQflage) {
                this.FAQflage = 0;
            } else {
                this.FAQflage = num;
            }
        },
        openCR(num) {
            if (num == this.CRflage) {
                this.CRflage = 0;
            } else {
                this.CRflage = num;
            }
        },

        openProfile() {
            var root = document.getElementById("collapseOne");
            var prof = document.getElementById("headingOne");
            var arrow = document.getElementById("aroDown");


            if (root.getAttribute('class') == 'collapse show') {
                prof.setAttribute('aria-expanded', false);
                root.setAttribute('class', 'collapse');
                document.querySelector(".card-collapse .card-header  a i").setAttribute('aria-hidden', false);
            } else {
                root.setAttribute('class', 'collapse show');
                prof.setAttribute('aria-expanded', true);
                document.querySelector(".card-collapse .card-header  a i").setAttribute('aria-hidden', true);


            }

        },
        GetPackages(objPackage) {

            this.$http.GetPackagesAndCoursesBySuperPackageId(objPackage)
                .then(response => {
                    this.Packages = response.data.package;
                })
                .catch((err) => {
                    console.error(err);
                });
        }


    }
}
