﻿import addCourseFiles from './AddCourseFiles/AddCourseFiles.vue';
import editCourseFiles from './EditCourseFiles/EditCourseFiles.vue';
import moment from 'moment';
export default {
    name: 'CourseFiles',    
    created() {
        
        
        this.GetCourseFilesByCourseId(this.pageNo);  
    },
    components: {
        'add-CourseFiles': addCourseFiles,
        'edit-CourseFiles': editCourseFiles
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            CourseEdit:[],
         
            pageNo: 1,
            pageSize: 6,
            pages: 0,  
            CourseFiles: [],
            state:0,
          
        };
    },
    methods: {
        EditCourse(Course) {
            this.CourseEdit = Course;
            this.state = 2;
        },
        AddCourse() {
            this.state = 1;
        },

        Back() {
            this.$router.push("/Packages");
        },

        DeleteCourse(courseId) {
            this.$confirm('سيؤدي ذلك إلى حذف الدورة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteCourse(courseId)
                    .then(response => {
                        if (this.Courses.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الدورة بنجاح"
                        });
                        this.GetCoursesBySuperPackageId(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
       
        GetCourseFilesByCourseId(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCourseFilesByCourseId(this.pageNo, this.pageSize, this.$route.params.CourseId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.CourseFiles = response.data.courseFiles;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });

        },
       
    }    
}
