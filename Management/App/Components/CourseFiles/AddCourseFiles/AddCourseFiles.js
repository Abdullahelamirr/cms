﻿export default {
    name: 'AddCourseFiles',    
    created() {
       
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            pdf: null,
            UploadLable:"تــحميل ورقـة العمل",
            form: {
                Name: '',
                Description: '',
                Pdf: '',
                CourseId: ''
            }
          
        };
    },
    methods: {
        FileChanged(e) {
            console.log(e.target.files);
            var files = e.target.files;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'application/pdf') {
                this.$message({
                    type: 'error',
                    message: 'Only PDF Files Allowed'
                });
                return;
            }
            this.UploadLable = e.target.files[0].name.slice(0,20);

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.pdf = reader.result;
            };
            reader.onerror = function (error) {

            };
            reader.readAsDataURL(files[0]);
        },



        Back() {
            this.$parent.state = 0;
        },

        Save() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الدورة'
                });
                return;
            }

            if (!this.pdf) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء رفع ورقة العمل'
                });
                return;
            }

            this.form.Pdf = this.pdf;
            this.form.CourseId = this.$route.params.CourseId;
            this.$http.AddCourseFiles(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetCourseFilesByCourseId();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        },
    }    
}
