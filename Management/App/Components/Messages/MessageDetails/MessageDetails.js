﻿import moment from 'moment';
export default {
    name: 'MessageDetails',
    created() {
        console.log(this.$route.params.MessageId);
        this.GetMessageDetails(this.$route.params.MessageId);
       
    },
    components: {
       
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            MessageDetails: []
        };
    },
    methods: {
        GetMessageDetails(MessageId) {
            this.$http.GetMessageDetails(MessageId)
                .then(response => {
                    this.MessageDetails = response.data.messageDetails.payload;
                    this.$blockUI.Stop();
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                });
        }
       



    }
};
