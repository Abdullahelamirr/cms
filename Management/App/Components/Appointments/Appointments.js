﻿import addAppointments from './AddAppointments/AddAppointments.vue';
import editAppointments from './EditAppointments/EditAppointments.vue';
import checkFilter from '../Appointments/AppointmentCheck/check-filter.vue'


import moment from 'moment';

export default {
    name: 'Appointments',

    created() {
        this.GetAppointments(this.pageNo);

    },

    components: {
        'add-Appointments': addAppointments,
        'edit-Appointments': editAppointments,
        'check-filter': checkFilter
    },

    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        },
        fromnow(date) {
            return moment(date).format('a hh:mm')
            //moment(date).toNow();
        }, dayss(date) {
            return moment(date).format("dddd, MMMM Do YYYY");
        },

        days(date) {
            return moment(date).format('DD')
            //moment(date).toNow();
        },


        months(date) {
            return moment(date).format('MMMM')
            //moment(date return moment(date)).toNow();
        },
        dayofweek(date) {
            return moment(date).format('dddd');
        }
    },
    data() {
        return {


            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Appointments: [],
            apstatus: [],
            AppoStatus: [
                { id: 1, name: 'فعال' },
                { id: 2, name: 'الغاء' },
                { id: 3, name: 'تأجيل' },
                { id: 4, name: 'مقابله ناجحه' }

            ],
            today: moment(),
            isSamedate: false,
            state: 0,
            filterChecked: false,
            EditCompany: ''

        };
    },
    computed: {
        groups() {

            return this.filtredlist(this.Appointments, 'dateTimeFrom')
        },
        appointmetFilter() {
           
                return this.Appointments.filter(this.appointmentPassesStatusFilter)
            
        }

    },
    methods: {
        appointmentPassesStatusFilter(Appointments) {
            if (!this.apstatus.length) {
                return true;
            } else {
                return this.apstatus.find(apstatu => Appointments.appointmentStatus === apstatu);
            }
        },

        goto() {

            console.log("Marwa");
            return true;
        },
        CheckFilter(AppoStatus, title, ids, filterChecked) {
            if (filterChecked) {
                this.apstatus.push(ids);
            }
            else {
                let index = this.apstatus.indexOf(ids)
                if (index > -1) {
                    this.apstatus.splice(index, 1);
                }
            }
            
        },

        filterred(app) {
            
            return app.filter(ss => {

                return moment(ss.dateTimeFrom).isSame(this.today, 'day');
            })
        },
        inminits(date) {
           
            var current = moment();

            // get difference between event and current
            var diffTime = date.diff(current);

            // let moment.js make the duration out of the timestamp
            var duration = moment.duration(diffTime, 'milliseconds', true);

            // set interval to milliseconds
            var interval = 1000;

            return a = moment.duration(duration - interval, 'milliseconds');
        },

        getTime(dta) {
            if (moment(dta).format('a hh:mm') == moment(dta).format('a hh:mm')) {
                return true
            }
            else {
                return false
            }
        },

        testFilter(obj, predicate) {
           
            Object.keys(obj)
                .filter(key => predicate(obj[key]))
                .reduce((res, key) => Object.assign(res, { [key]: obj[key] }), {})
        },
        lastFun(obj) {
           
            return this.testFilter(obj, score => score == this.today);


        },

        filtredlist: function (objectArray, property) {


            return objectArray.reduce(function (acc, obj) {

                //  var key = obj[property];
                var key = moment(obj[property]).format('YYYY-MM-DD');
                if (!acc[key]) {
                    acc[key] = [];
                }

                acc[key].push(obj);

                return acc;
            }, {});

        },

        GetAppointments(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetAppointments(this.pageNo, this.pageSize)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Appointments = response.data.appointments;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });
        },

        AddAppointmentsPage() {
            this.state = 1;
        }

        //EditCompanyInfo (Company) {
        //    this.EditCompany = Company;
        //    this.state = 2;
        //},

        //DeleteCompany (CompanyId) {
        //    this.$confirm('سيؤدي ذلك إلى حذف الشركة و افـراد الشركة نهائيًا. استمر؟', 'تـحذير', {
        //        confirmButtonText: 'نـعم',
        //        cancelButtonText: 'لا',
        //        type: 'warning'
        //    }).then(() => {
        //        this.$http.DeleteCompany(CompanyId)
        //            .then(response => {
        //                if (this.Students.lenght === 1) {
        //                    this.pageNo--;
        //                    if (this.pageNo <= 0) {
        //                        this.pageNo = 1;
        //                    }
        //                }
        //                this.$message({
        //                    type: 'info',
        //                    message: 'تم مسح الشركة بنجاح'
        //                });
        //                this.GetCompanies();
        //            })
        //            .catch((err) => {
        //                this.$message({
        //                    type: 'error',
        //                    message: err.response.data
        //                });
        //            });
        //    });
        //}
    }
}

