﻿export default {
    name: 'EditStudents',
    created () {
        this.EditCompanyData();
    },
    data () {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                CompanyName: '',
                Description: '',
                PhoneNumber: '',
                City: '',
                Email: '',
                Address: '',
                BoughtBy: '',
                Communicator: '',
                CompanyId: ''
            }
        };
    },
    methods: {
        EditCompanyData () {
            var obj = this.$parent.EditCompany;
            if (obj != null) {
                this.form.CompanyName = obj.companyName;
                this.form.Description = obj.description;
                this.form.PhoneNumber = obj.phoneNumber;
                this.form.City = obj.city;
                this.form.Email = obj.email;
                this.form.Address = obj.address;
                this.form.BoughtBy = obj.boughtBy;
                this.form.Communicator = obj.communicator;
            } else {
                this.$parent.state = 0;
            }
        },

        Back () {
            this.$parent.state = 0;
        },

        edit () {
            if (!this.form.CompanyName) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الشركة'
                });
                return;
            }

            if (!this.form.PhoneNumber) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }

            if (!this.form.City) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم المدينة'
                });
                return;
            }

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الالكتروني'
                });
                return;
            }
            if (!this.form.Address) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عنـوان الشركة'
                });
                return;
            }
            if (!this.form.BoughtBy) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم صاحب الصفقة'
                });
                return;
            }

            if (!this.form.Communicator) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال جهة الاتصال'
                });
                return;
            }

            this.form.CompanyId = this.$parent.EditCompany.companyId;

            this.$http
                .EditCompany(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetCompanies();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        }
    }
};
