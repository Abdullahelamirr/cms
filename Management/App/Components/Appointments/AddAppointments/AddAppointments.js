﻿import moment from 'moment';
export default {
    name: 'AddAppointments',
    created() {
        this.GetStudents(this.pageNo);
        this.GetCompanies();

    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },

    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Students: [],
            Companies: [],
            UserType: 0,
            CompanyNameSelect: '',
            StudentNameSelect: '',

            form: {
                Title: '',
                Description: '',
                AppointmentType: '',  //morning or evening
                CustomerType: '',
                CompanyId: '',
                StudentId: '',
                DateTimeFrom: null,
                AppointmentStatus: null,

            }
        }

    },

    methods: {
        Back() {
            this.$parent.state = 0;
        },
        Save() {
            if (!this.form.Title) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال عنوان '
                });
                return;
            }

            if (!this.form.Description) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال حول الموعد'
                });
                return;
            }
            if (!this.form.DateTimeFrom) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التاريخ والوقت'
                });
                return;
            }


            debugger;
            if (!this.form.CustomerType || this.form.CustomerType==0) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال نوع العميل '
                });
                return;
            }
            if (this.form.CustomerType == 1) {
                if (this.StudentNameSelect == '') {
                    this.$message({
                        type: 'error',
                        message: "الرجاء إختيار اسم العميل اولا"
                    });
                    return;
                }
            }
            if (this.form.CustomerType == 2) {
                if (this.CompanyNameSelect == '') {
                    this.$message({
                        type: 'error',
                        message: "الرجاء إختيار اسم الشركة اولا"
                    });
                    return;
                }
            }
            this.form.CompanyId = this.CompanyNameSelect
            this.form.StudentId = this.StudentNameSelect
            this.form.DateTimeFrom = moment(this.form.DateTimeFrom).format('YYYY-MM-DD HH:mm');

            debugger;
            this.$blockUI.Start();
            this.$http.AddAppointment(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$blockUI.Start();
                    this.$parent.GetAppointments();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    } );
        });

        },

               

        StudentTypeSelect() {
            this.UserType = this.form.CustomerType;

            this.StudentNameSelect = '';
            this.CompanyNameSelect = '';
            this.GetStudents(this.pageNo);
        },


        GetStudents(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetStudents(this.pageNo, this.pageSize, this.UserType, this.CompanyNameSelect)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Students = response.data.student;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        GetCompanies() {
            this.$http.GetCompanies()
                .then(response => {
                    this.Companies = response.data.companies;
                }).catch((err) => {
                    console.error(err);
                });
        },

    }
};
