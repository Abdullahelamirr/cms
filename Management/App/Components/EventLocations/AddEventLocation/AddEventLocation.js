﻿
import moment from 'moment';
export default {
    name: 'AddEventLocation',
    created() {
       
    },

    data() {
        return {

            pageNo: 1,
            form: {
                Name: '',
                Description: '',
                Email: '',
                EventAddress: '',
                ContactName: '',
                ContactNumber: '',
                HostId:0
            },
        };

    },
    methods: {

        Back() {
            this.$parent.state = 0;
        },
        Save() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الموقع'
                });
                return;
            }

            if (!this.form.Description) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال معلومات حول الموقع'
                });
                return;
            }


            if (!this.form.ContactName) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الشخص المنسق'
                });
                return;
            }
            if (!this.form.ContactNumber) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }
           
          

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الإلكتروني'
                });
                return;
            }

            this.form.CountriesId = this.$parent.CityId;
           
            this.$blockUI.Start();

            this.$http.AddLocations(this.form)
                .then(response => {
                    this.$blockUI.Stop();
                    this.$parent.state = 0;
                    this.$parent.GetLocations();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });


           
        }
}

}
