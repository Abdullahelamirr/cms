﻿import addEventLocation from './AddEventLocation/AddEventLocation.vue';
import editEventLocation from './EditEventLocation/EditEventLocation.vue';
import moment from 'moment';
export default {
    name: 'EventLocations',
    created() {
        // debugger;
        this.GetCountries(this.pageNo);
        this.GetLocations(this.pageNoLocation);
        this.GetCountriesByType(this.pageNo);
        // console.log(this.GetCountries(this.pageNo));

    },
    components: {
        'add-EventLocation': addEventLocation,
        'edit-EventLocation': editEventLocation
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            CountryTable:[],
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            pageNoLocation: 1,
            pageSizeLocation: 10,
            pagesLocation: 0,
            Countries: [],
            Cities: [],
            Locations: [],
            dialogFormVisible: false,
            CountryId: '',
            CountryIdDialog: '',
            CityName: '',
            CountryName:'',
            CitiesPaceholder: '',
            CoutryIndex: 0,
            CityId: '',
            state: 0,
            visible: false,
            formLabelWidth: '120px',
            IsCountry: true,
            type:1,
        };
    },
    methods: {
        SelectCountry() {
            console.log(this.IsCountry);
            if (this.IsCountry) {
                this.type = 1;
                this.GetCountriesByType(this.pageNo);
            } else {
                this.type = 0;
                this.GetCountriesByType(this.pageNo);
            }
        },

        SaveCountry() {
            var form = {};
            // save city
            
            if (this.IsCountry) {
                if (!this.CountryIdDialog) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إختيار البلاد '
                    });
                    return;
                }
                if (!this.CityName) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إختيار المدينة '
                    });
                    return;
                }
            } else {
                if (!this.CountryName) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إختيار البلاد '
                    });
                    return;
                }
            }
            form.IsCountry = this.IsCountry;
            form.CountryIdDialog = this.CountryIdDialog;
            form.CityName = this.CityName;
            form.CountryName = this.CountryName;

            //save country
            this.$http.AddCountries(form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                    this.GetCountriesByType(this.pageNo);
                    this.GetCountries(this.pageNo);

                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
            form = {};
        },

        OpenLocations() {
            this.dialogFormVisible = true;
        },
        DeleteCountry(countriesId) {
            this.$confirm('سيؤدي ذلك إلى حذف البلد و مدنها نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
             
                this.$http.DeleteCountry(countriesId)
                    .then(response => {
                        if (this.Countries.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: response.data
                        });
                        this.GetCountriesByType(this.pageNo);
                        this.GetCountries(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },

        GetLocations(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
           // debugger;
            this.$http.GetLocations(this.pageNoLocation, this.pageSizeLocation, this.CityId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Locations = response.data.locations;
                    this.pagesLocation = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pagesLocation = 0;
                });
        },

        EditLocation(Location) {
            this.CourseEdit = Location;
            this.state = 2;
        },
        AddLocations() {
            if (this.CityId == '') {
                
                    this.$message({
                        type: 'error',
                        message: "الرجاء إختيار المدينه"
                    });
                    return;
                
            }
            this.state = 1;
          
        },

        Back() {
            
            this.$router.push('/EventLocation');
            this.state = 0;
        },

        GetCountries() {
            //this.$blockUI.Start();
                this.$http.GetCountries(0)
                    .then(response => {
                        this.Countries = response.data.countries;
                    }).catch((err) => {
                        console.log(err);
                        //this.$blockUI.Stop();
                    });
        },

        getCity() {
            this.$http.GetCountries(this.CountryId)
                .then(response => {

                    this.Cities = response.data.countries;

                }).catch((err) => {
                    console.log(err);
                    //this.$blockUI.Stop();   
                });
        },


        SelectedCountry() {
            this.CitiesPaceholder = 'المدينه'
            this.getCity()
            this.CityId = '';

        },
        SelectedCity() {
            this.GetLocations(this.pageNo);
        },

        GetCountriesByType(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCountriesByType(this.pageNo, this.pageSize,this.type)
                .then(response => {
                    this.$blockUI.Stop();
                    this.CountryTable = response.data.country;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pages = 0;
                });

        }




    }
}
