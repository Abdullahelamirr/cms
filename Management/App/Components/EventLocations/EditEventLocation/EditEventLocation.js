﻿
import moment from 'moment';
export default {
    name: 'Courses',
    created () {
      
    },
    
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
           
        };
    }
  
};
