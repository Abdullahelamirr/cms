﻿import AddSubPackages from './AddSubPackages/AddSubPackages.vue';
import EditSubPackages from './EditSubPackages/EditSubPackages.vue';
import moment from 'moment';
export default {
    name: 'SubPackages',    
    created() {
        //if (this.$parent.SuperPackageParent==null) {
        //    this.$router.push("/Packages/SuperPackages");
        //}
        this.GetPackages(this.pageNo);  
        this.GetSuperPackagesContainsPackagesOnly();
    },
    components: {
        'add-Packages': AddSubPackages,
        'edit-Packages': EditSubPackages
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            PackageEdit:[],
            //SuperPackageParent: this.$parent.SuperPackageParent,
            pageNo: 1,
            pageSize: 10,
            pages: 0,  
            Packages: [],
            state: 0,
            SuperPackages: [],
            SuperPacakgeValue:0,
          
        };
    },
    methods: {
        EditPackage(Package) {
            this.PackageEdit = Package;
            this.state = 2;
        },
        AddPackage() {
            this.state = 1;
        },

        Back() {
            this.$router.push("/Packages");
        },

        DeletePackage(PackageId) {
            this.$confirm('سيؤدي ذلك إلى حذف الحزمة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeletePackage(PackageId)
                    .then(response => {
                        if (this.Packages.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الحزمة بنجاح"
                        });
                        this.GetPackages(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
       
        GetPackages(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetPackages(this.pageNo, this.pageSize, this.SuperPacakgeValue)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Packages = response.data.packages;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },
       
        GetSuperPackagesContainsPackagesOnly() {
            this.$blockUI.Start();
            this.$http.GetSuperPackagesContainsPackagesOnly()
                .then(response => {
                    this.$blockUI.Stop();
                    this.SuperPackages = response.data.superPackage;   
                    this.SuperPackages.unshift({ name: "الكل", superPackageId: 0 });
                    
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                   
                });
        },

        SelectedSupaerPackage() {
            this.GetPackages(this.pageNo);
        },

    }    
}
