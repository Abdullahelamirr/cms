﻿export default {
    name: 'EditCourse',    
    created() {
        var Packages = this.$parent.PackageEdit;
        this.form.Name = Packages.name;
        this.form.Description = Packages.description;
        this.form.PricePersonal = Packages.pricePersonal;
        this.form.PriceCompany = Packages.priceCompany;
        this.form.Discount = Packages.discount;
        this.form.Color = Packages.color;
        this.form.PackageId = Packages.packageId;
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                Name: '',
                Description: '',
                PricePersonal: '',
                PriceCompany: '',
                Discount: '',
                Color: '',
                PackageId:'',
            },     
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        Edit() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الدورة'
                });
                return;
            }

            if (!this.form.PricePersonal) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للأفراد'
                });
                return;
            }

            if (!this.form.PriceCompany) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للشركات'
                });
                return;
            }

            if (!this.form.Discount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التخفيض'
                });
                return;
            }

            if (!this.form.Color) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اللون'
                });
                return;
            }
            console.log(this.form);
            this.$http.EditPackage(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetPackages();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });    
        },

    }    
}
