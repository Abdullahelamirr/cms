﻿import addStudents from './AddStudents/AddStudents.vue';
import editStudents from './EditStudents/EditStudent.vue';
import moment from 'moment';
export default {
    name: 'Students',
    created() {
        
        this.SelectCompany();
        this.StudentTypeSelect();
        this.SearchBy.push({
            id: 1,
            name: "رقم الهاتف"

        }),

            this.SearchBy.push({
                id: 2,
                name: "الاسم"
            });

        this.GetStudents(this.pageNo);
        this.GetCompanies();

    },
    components: {
        'add-Students': addStudents,
        'edit-Students': editStudents
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            SearchBy: [],
            SearchModel: null,
            SearchResult: [],
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Students: [],
            Companies: [],
            UserType: 0,
            StudentType: '',
            CompanyNameSelect: '',
            state: 0,
            visible: false,
            EditStudent: '',
            SearchGeneral: null,
            SearchName: null,
            dialogTableVisible: false,
            dialogEvents:false,
            EventDetails: [],
            pageNoEvent: 1,
            pageSizeEvent: 10,
            pagesEvent: 0,
            EventStudent:[]

        };
    },
    methods: {
        ShowMore(Student) {
            this.EventDetails = Student;
        },
        GetEventsByStudentId(StudentId) {

            
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            // debugger;
            this.$http.GetLocations(this.pageNoEvent, this.pageSizeEvent, StudentId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.EventStudent = response.data.eventsStudent;
                    this.pagesEvent = response.data.count;
                })
                .catch((err) => {
                    console.log(err);
                    this.$blockUI.Stop();
                    this.pagesEvent = 0;
                });
        },

        SelectedSearch() {
            this.SearchGeneral = null;
            this.SearchName = null;
            this.SearchLName = null;
            this.SearchSurName = null;
        },
        SearchMethod() {
            var Object = {};

            if (!this.SearchModel) {
                this.$message({
                    type: 'error',
                    message: 'الرجاءاختيار نوع البحت'
                });
                return;
            }
            
            if (this.SearchModel == 1) {
                if (!this.SearchGeneral) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إدخال رقم الهاتف'
                    });
                    return;
                }
                //if (this.SearchGeneral.length <= 2) {
                //    this.$message({
                //        type: 'error',
                //        message: 'الرجاء ادخال رقم الهاتف اقل طول رقمين'
                //    });
                //    return;
                //}
               
                Object = {

                    pageNo: this.pageNo,
                    pageSize: this.pageSize,
                    UserType: this.UserType,
                    CompanyNameSelect: this.CompanyNameSelect,
                    SearchType: this.SearchModel,
                    FirstName: null,
                    Phone: this.SearchGeneral,
                };
            } else if (this.SearchModel == 2) {
                if (!this.SearchName) {
                    this.$message({
                        type: 'error',
                        message: 'الرجاء إدخال الاسم الاول'
                    });
                    return;
                }


                // searching .... 

                Object = {
                    pageNo: this.pageNo,
                    pageSize: this.pageSize,
                    UserType: this.UserType,
                    CompanyNameSelect: this.CompanyNameSelect,
                    SearchType: this.SearchModel,
                    FirstName: this.SearchName,
                    Phone: null
                };
            }
        
            this.$blockUI.Start();

            this.$http.Search(Object)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Students = response.data.student;
                    this.pages = response.data.count;
                    this.SearchName = '';
                    this.SearchGeneral= null;
                        
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });


        },

        SelectCompany() {
            this.GetStudents(this.pageNo);
        },

        StudentTypeSelect() {
            this.UserType = this.StudentType;
            this.CompanyNameSelect = '';
            this.GetStudents(this.pageNo);
        },

        GetStudents(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetStudents(this.pageNo, this.pageSize, this.UserType, this.CompanyNameSelect)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Students = response.data.student;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        GetCompanies() {
            this.$http.GetCompanies()
                .then(response => {
                    this.Companies = response.data.companies;
                }).catch((err) => {
                    console.error(err);
                });
        },

        AddStudentPage() {
            if (this.StudentType == 2) {
                if (this.CompanyNameSelect == '') {
                    this.$message({
                        type: 'error',
                        message: "الرجاء إختيار اسم الشركة اولا"
                    });
                    return;
                }
            }
            this.state = 1;
        },
        EditStudentInfo(Student) {
            this.EditStudent = Student;
            this.state = 2;
        },
        DeleteStudent(StudentId) {
            this.$confirm('سيؤدي ذلك إلى حذف المتدرب نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteStudent(StudentId)
                    .then(response => {
                        if (this.Students.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح المتدرب بنجاح"
                        });
                        this.GetStudents();
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });

        },

    }
}
