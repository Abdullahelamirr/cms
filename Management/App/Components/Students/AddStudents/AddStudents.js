﻿var emailRE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default {
    name: 'AddStudents',
    created() {
       
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                NameAr: '',
                NameEn: '',
                Nationality: '',
                BirthDate: '',
                PhoneNumber1: '',
                PhoneNumber2: '',
                CurrentJob: '',
                Photo:'',
                ExperinceNumbers: '',
                Email: '',
                Location: '',
                CompanyId: ''
               
            },
            Selectedfile: ''
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
            this.$parent.GetStudents();
            this.$parent.GetCompanies();
        },
        FileChanged(e) {
            var files = e.target.files;
            this.Selectedfile = files[0].name;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'image/jpeg' && files[0].type !== 'image/png') {
                this.$message({
                    type: 'error',
                    message: 'عفوا يجب انت تكون الصورة من نوع JPG ,PNG'
                });
                this.photo = null;
                return;
            }

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.photo = reader.result;
               // $this.UploadImage();
            };
            reader.onerror = function (error) {
                $this.photo = null;
            };
            reader.readAsDataURL(files[0]);
        },
        Save() {
            if (!this.form.NameAr) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الإسم بالعربي'
                });
                return;
            }

            if (!this.form.NameEn) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الاسم بالانجليزي'
                });
                return;
            }

            if (!this.form.Nationality) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الجنسـية'
                });
                return;
            }

            if (!this.form.BirthDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال تاريخ الميلاد'
                });
                return;
            }
            if (!this.form.PhoneNumber1) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }
            if (!this.form.CurrentJob) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الرظيفة الحالية'
                });
                return;
            }

            if (!this.form.ExperinceNumbers) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال سنوات الخبرة'
                });
                return;
            }

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الإلكتروني'
                });
                return;
            }

            this.form.CompanyId = this.$parent.CompanyNameSelect;
            this.form.Photo = this.photo;
            console.log(this.form.Photo + 'photo');
            this.$blockUI.Start();
            
            this.$http.AddStudent(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetStudents();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });


            //if (!this.SirName) {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter SirName please!'
            //    });
            //    return;
            //}

            //if (!this.PhoneNumber) {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter PhoneNumber please!'
            //    });
            //    return;
            //}

            //if (this.Country == '') {
            //    this.$message({
            //        type: 'error',
            //        message: 'Enter Country name please!'
            //    });
            //    return;

            //}

            //this.$http.AddCustomer({
            //    CustomerID: 0,
            //    Name: this.Name,
            //    FatherName: this.FatherName,
            //    SirName: this.SirName,
            //    PhoneNumber: this.PhoneNumber,
            //    Age: this.Age,
                
            //    Address: this.Address,
            //    NID: this.NID,
            //    Country: this.Country
            //})
            //    .then(response => {
            //        this.$parent.state = 0;
            //        this.$parent.GetCustomers();
                 
            //        this.$message({
            //            type: 'info',
            //            message: response.data
            //        });
            //    })
            //    .catch((err) => {
            //        this.$message({
            //            type: 'error',
            //            message: err
            //        });
            //    });
        },

   
       // Back() {
       //     this.$parent.state = 0;
       // },
       //// GetCustomers
       // GetCustomers(pageNo) {
            
       //     this.pageNo = pageNo;
       //     if (this.pageNo === undefined) {
       //         this.pageNo = 1;
       //     }
       //     this.$http.GetCustomers(this.pageNo, this.pageSize)
       //         .then(response => {
                   
       //             this.Customers = response.data.customers;
       //             this.pages = response.data.count;
       //         })
       //         .catch((err) => {
       //             console.log(err);
       //             this.survey = [];
       //             this.pages = 0;
       //         });
       // },
    }    
}
