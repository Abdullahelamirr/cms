﻿import addCourses from './AddCourses/AddCourses.vue';
import editCourses from './EditCourses/EditCourses.vue';
import moment from 'moment';
export default {
    name: 'Courses',
    created () {
        // console.log(this.$route.params.SuperPackageId)
        // console.log(this.$parent.SuperPackageParent);
        // if (this.$parent.SuperPackageParent==null) {
        //    this.$router.push("/Packages/SuperPackages");
        // }
        this.GetSuperPackagesContainsPackagesAndCourses();
    },
    components: {
        'add-Courses': addCourses,
        'edit-Courses': editCourses
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data () {
        return {
            CourseEdit: [],
            pageNo: 1,
            pageSize: 6,
            pages: 0,
            Courses: [],
            state: 0,
            PackageType: '',
            PlaceHoldeerPackageName: '',
            Packages: [],
            SuperPackages: [],
            PackageAppend: '',
            PackagesFlage: false,
            SuperPackageId: '',
            SelectedPackage: '',
            contentType: 0
        };
    },
    methods: {
        SelectSuperPackage () {
            this.SelectedPackage = '';
            this.superPackageId = this.PackageAppend.split(',')[0];
            console.log(this.superPackageId);
            this.contentType = this.PackageAppend.split(',')[1];
            console.log(this.contentType);
            // if contentType == 1 >> Packages
            if (this.contentType === '1') {
                this.PackagesFlage = true;
                this.Courses = [];
                this.GetPackages(this.superPackageId);
            } else {
                this.PackagesFlage = false;
                // if contentType == 2 >> Courses
                this.GetCoursesBySuperPackageId(this.pageNo);
            }
        },

        SelectPackage () {
            this.GetCoursesByPackageId(this.pageNo);
        },

        OpenCourseFile(CourseId) {
            this.$router.push('/Courses/CourseFiles/' + CourseId);

        },

        GetCoursesByPackageId (pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCoursesByPackageId(this.pageNo, this.pageSize, this.SelectedPackage)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Courses = response.data.courses;
                    console.log(this.Courses);
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        GetCoursesBySuperPackageId (pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetCoursesBySuperPackageId(this.pageNo, this.pageSize, this.superPackageId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.Courses = response.data.courses;
                    console.log(this.Courses);
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });
        },

        GetSuperPackagesContainsPackagesAndCourses () {
            this.$http.GetSuperPackagesContainsPackagesAndCourses()
                .then(response => {
                    this.SuperPackages = response.data.superPackage;
                })
                .catch(err => {
                    console.error(err);
                });
        },

        GetPackages (superPackageId) {
            this.$http.GetPackagesV2(superPackageId)
                .then(response => {
                    this.Packages = response.data.packages;
                })
                .catch((err) => {
                    console.error(err);
                });
        },

        EditCourse (Course) {
            this.CourseEdit = Course;
            this.state = 2;
        },
        AddCourse () {
            this.state = 1;
        },

        Back () {
            this.$router.push('/Packages');
        },

        DeleteCourse (courseId) {
            this.$confirm('سيؤدي ذلك إلى حذف الدورة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteCourse(courseId)
                    .then(response => {
                        if (this.Courses.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: 'تم مسح الدورة بنجاح'
                        });

                        if (this.PackagesFlage === false) {
                            this.GetCoursesBySuperPackageId(this.pageNo);
                        } else {
                            this.GetCoursesByPackageId(this.pageNo);
                        }
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        }
    }
};
