﻿export default {
    name: 'AddCourse',    
    created() {
       
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                Name: '',
                Description: '',
                PricePersonal: '',
                PriceCompany: '',
                Discount: '',
                Color: '',
                SuperPackageId: '',
                PackageId:''
            },
          
         
        };
    },
    methods: {
        Back() {
            this.$parent.state = 0;
        },

        Save() {
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم الدورة'
                });
                return;
            }

            if (!this.form.PricePersonal) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للأفراد'
                });
                return;
            }

            if (!this.form.PriceCompany) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال السعر للشركات'
                });
                return;
            }

            if (!this.form.Discount) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال التخفيض'
                });
                return;
            }
            if (!this.form.Color) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اللون'
                });
                return;
            }
            if (this.$parent.contentType == 2) {
                this.form.SuperPackageId = this.$parent.superPackageId;
            } else {
                this.form.PackageId = this.$parent.SelectedPackage;
            }       

            this.$blockUI.Start();
            this.$http.AddCourse(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    
                    if (this.$parent.PackagesFlage == false) {
                        this.$parent.GetCoursesBySuperPackageId(this.pageNo);
                    } else {
                        this.$parent.GetCoursesByPackageId(this.pageNo);
                    }

                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        },
    }    
}
