﻿import addEventFile from './AddEventsPromotFiles/AddEventsPromotFiles.vue';
//import editEvents from './EditEvents/EditEvents.vue';
import moment from 'moment';
export default {
    name: 'EventsPromotFiles',
    created() {
        this.GetEventFilesByEventId(this.pageNo);
        console.log(this.$route.params.EventId);

        // this.GetSuperPackagesContainsPackagesAndCourses();
    },
    components: {
        'add-EventFile': addEventFile,
       // 'edit-Events': editEvents
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return 'فارغ';
            }
            // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {
            EventEdit: [],

            pageNo: 1,
            pageSize: 6,
            pages: 0,
            EventFiles: [],
            state: 0,

        };
    },
    methods: {
        EditCourse(Course) {
            this.CourseEdit = Course;
            this.state = 2;
        },
        AddFile() {
            this.state = 1;
        },

        Back() {
            this.$router.push("/Events");
        },

        DeleteCourse(eventId) {
            this.$confirm('سيؤدي ذلك إلى حذف الدورة نهائيا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteCourse(courseId)
                    .then(response => {
                        if (this.Courses.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.$message({
                            type: 'info',
                            message: "تم مسح الدورة بنجاح"
                        });
                        this.GetCoursesBySuperPackageId(this.pageNo);
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },

        GetEventFilesByEventId(pageNo) {
            this.pageNo = pageNo;
            if (this.pageNo === undefined) {
                this.pageNo = 1;
            }
            this.$blockUI.Start();
            this.$http.GetEventFilesByEventId(this.pageNo, this.pageSize, this.$route.params.EventId)
                .then(response => {
                    this.$blockUI.Stop();
                    this.EventFiles = response.data.eventFiles;
                    this.pages = response.data.count;
                })
                .catch((err) => {
                    this.$blockUI.Stop();
                    console.error(err);
                    this.pages = 0;
                });

        },

    }
}