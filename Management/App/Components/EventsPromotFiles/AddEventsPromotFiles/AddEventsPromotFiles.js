﻿export default {
    name: 'AddEventsPromotFiles',
    created() {

    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            pdf: null,
            wordFile: null,
            picture:null,

            UploadLable: "تــحميل PDF",
            imgLable: "تــحميل صورة",
            wordLable: "تــحميل وورد",
            form: {
                Name: '',
                Description: '',
                Pdf: '',
                WordFile:'',
                Picture:'',
                EventId: ''
            }

        };
    },
    methods: {
        FileChanged(e) {
            console.log(e.target.files);
            var files = e.target.files;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'application/pdf') {
                this.$message({
                    type: 'error',
                    message: 'Only PDF Files Allowed'
                });
                return;
            }
           
            this.UploadLable = e.target.files[0].name.slice(0, 20);

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.pdf = reader.result;
            };
            reader.onerror = function (error) {

            };
            reader.readAsDataURL(files[0]);
        },
        WordChanged(e) {
            
            var files = e.target.files;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                this.$message({
                    type: 'error',
                    message: 'Only .docx Files Allowed'
                });
                return;
            }

            this.wordLable = e.target.files[0].name.slice(0, 20);

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.wordFile = reader.result;
            };
            reader.onerror = function (error) {

            };
            reader.readAsDataURL(files[0]);
        },
        ImageChanged(e) {
            
            console.log(e.target.files);
            var files = e.target.files;

            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'image/jpeg') {
                this.$message({
                    type: 'error',
                    message: 'Only JPEG Files Allowed'
                });
                return;
            }

           
            this.imgLable = e.target.files[0].name.slice(0, 20);

            var $this = this;

            var reader = new FileReader();
            reader.onload = function () {
                $this.picture = reader.result;
            };
            reader.onerror = function (error) {

            };
            reader.readAsDataURL(files[0]);
        },

        Back() {
            this.$parent.state = 0;
        },

        Save() {
          
            if (!this.form.Name) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال اسم للملف'
                });
                return;
            }

            if (!this.pdf + !this.wordFile + !this.picture == 3) {
               
                this.$message({
                    type: 'error',
                    message: 'الرجاء تحميل ملف علي الاقل '
                });
                return;
            }

           

            this.form.Pdf = this.pdf;
            this.form.WordFile = this.wordFile;
            this.form.Picture = this.picture;
            this.form.EventId = this.$route.params.EventId;
            this.$http.AddEventFile(this.form)

                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetEventFilesByEventId();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        },
    }
}
