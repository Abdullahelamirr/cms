﻿import addTrainers from './AddTrainers/AddTrainers.vue';
import editTrainers from './EditTrainers/EditTrainers.vue';
import moment from 'moment';
export default {
    name: 'Trainers',    
    created() {     
        this.GetTrainers(this.pageNo);
    },
    components: {
        'add-Trainers': addTrainers,
        'edit-Trainers': editTrainers,
    },
    filters: {
        moment: function (date) {
            if (date === null) {
                return "فارغ";
            }
           // return moment(date).format('MMMM Do YYYY, h:mm:ss a');
            return moment(date).format('MMMM Do YYYY');
        }
    },
    data() {
        return {      
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            Trainers: [],  
            state: 0,
            visible: false,
            EventDetails: [],
            dialogTableVisible: false
           
        };
    },
    methods: {  
        ShowMore(Trainer) {
            this.EventDetails = Trainer;
        },
        GetTrainers(pageNo) {      
                this.pageNo = pageNo;
                if (this.pageNo === undefined) {
                    this.pageNo = 1;
                }
                this.$blockUI.Start();
                this.$http.GetTrainers(this.pageNo, this.pageSize)
                    .then(response => {
                        this.$blockUI.Stop();
                        this.Trainers = response.data.trainer;
                        this.pages = response.data.count; 
                    })
                    .catch((err) => {
                        this.$blockUI.Stop();
                        console.error(err);
                        this.pages = 0;
                    });
        },

        AddTrainerPage()  {
            this.state = 1;         
        },

        EditTrainerInfo(Trainer) {
            this.EditTrainer = Trainer;
            this.state = 2;
        },
        EventTrainer(TrainerId) {
            this.$router.push('/Events/' + TrainerId);
        },
        DeleteTrainer(TrainerId) {
                    this.$confirm('سيؤدي ذلك إلى حذف المتدرب نهائيًا. استمر؟', 'تـحذير', {
                confirmButtonText: 'نـعم',
                cancelButtonText: 'لا',
                type: 'warning'
            }).then(() => {
                this.$http.DeleteTrainer(TrainerId)
                    .then(() => {
                       
                        if (this.Trainers.lenght === 1) {
                            this.pageNo--;
                            if (this.pageNo <= 0) {
                                this.pageNo = 1;
                            }
                        }
                        this.GetTrainers();
                        this.$message({
                            type: 'info',
                            message: "تم مسح المدرب بنجاح"
                        });
                        this.FetchTrainers();
                    })
                    .catch((err) => {
                        this.$message({
                            type: 'error',
                            message: err.response.data
                        });
                    });
            });
        },
    }    
}
