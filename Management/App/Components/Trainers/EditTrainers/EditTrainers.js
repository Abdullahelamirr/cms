﻿export default {
    name: 'EditTrainers',
    created() {
        this.EditStudentData();
    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            form: {
                NameAr: '',
                NameEn: '',
                Nationality: '',
                BirthDate: '',
                PhoneNumber1: '',
                PhoneNumber2: '',
                CurrentJob: '',
                ExperinceNumbers: '',
                Email: '',
                Location: '',
                TrainerId: ''
            }


        };
    },
    methods: {

        EditStudentData() {
            var obj = this.$parent.EditTrainer;       
            if (obj !== null) {
                this.form.NameAr = obj.nameAr;
                this.form.NameEn = obj.nameEn;
                this.form.Nationality = obj.nationality;
                this.form.BirthDate = obj.birthDate;
                this.form.PhoneNumber1 = obj.phoneNumber1;
                this.form.PhoneNumber2 = obj.phoneNumber2;
                this.form.CurrentJob = obj.currentJob;
                this.form.ExperinceNumbers = obj.experinceNumbers;
                this.form.Email = obj.email;
                this.form.Location = obj.location;
               
            } else {
                this.$parent.state = 0;
            }
        },

        Back() {
            this.$parent.state = 0;
        },

        edit() {
            if (!this.form.NameAr) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الإسم بالعربي'
                });
                return;
            }

            if (!this.form.NameEn) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الاسم بالانجليزي'
                });
                return;
            }

            if (!this.form.Nationality) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الجنسـية'
                });
                return;
            }

            if (!this.form.BirthDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال تاريخ الميلاد'
                });
                return;
            }
            if (!this.form.PhoneNumber1) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }
            if (!this.form.CurrentJob) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الرظيفة الحالية'
                });
                return;
            }

            if (!this.form.ExperinceNumbers) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال سنوات الخبرة'
                });
                return;
            }

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الإلكتروني'
                });
                return;
            }

            this.form.TrainerId = this.$parent.EditTrainer.trainerId;
           
            this.$http.EditTrainer(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$parent.GetTrainers();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });  
        }
    }
}
