﻿export default {
    name: 'AddTrainers',
    created() {

    },
    data() {
        return {
            pageNo: 1,
            pageSize: 10,
            pages: 0,
            success: false,
            UploadLable: "تــحميل السيرة الذاتية",
            CV:null,
            form: {
                NameAr: '',
                NameEn: '',
                Nationality: '',
                BirthDate: '',
                PhoneNumber1: '',
                PhoneNumber2: '',
                CurrentJob: '',
                ExperinceNumbers: '',
                Email: '',
                Location: '',
                CV:null

            },
        };
    },

    methods: {
        FileChanged(e) {
            var files = e.target.files;
            if (files.length <= 0) {
                return;
            }

            if (files[0].type !== 'application/pdf') {
                this.$message({
                    type: 'error',
                    message: 'Only PDF Files Allowed'
                });
                return;
            }
            this.UploadLable = e.target.files[0].name.slice(0, 20);
            var $this = this;
            var reader = new FileReader();
            reader.onload = function () {
                $this.CV = reader.result;
            };
            reader.onerror = function (error) {

            };
            reader.readAsDataURL(files[0]);
        },

        Back() {
            this.$parent.state = 0;
        },

        Save() {
            if (!this.form.NameAr) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الإسم بالعربي'
                });
                return;
            }

            if (!this.form.NameEn) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الاسم بالانجليزي'
                });
                return;
            }

            if (!this.form.Nationality) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الجنسـية'
                });
                return;
            }

            if (!this.form.BirthDate) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال تاريخ الميلاد'
                });
                return;
            }
            if (!this.form.PhoneNumber1) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال رقم الهاتف'
                });
                return;
            }
            if (!this.form.CurrentJob) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال الرظيفة الحالية'
                });
                return;
            }

            if (!this.form.ExperinceNumbers) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال سنوات الخبرة'
                });
                return;
            }

            if (!this.form.Email) {
                this.$message({
                    type: 'error',
                    message: 'الرجاء إدخال البريد الإلكتروني'
                });
                return;
            }
            var emailRG = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!(emailRG.test(this.form.Email))) {
                this.$message({
                    type: 'error',
                    message: 'master@gmail.com إدخال البريد الإلكتروني بطريقة صحيحة'
                });
                return;
            }
            this.form.CV = this.CV;
            this.$blockUI.Start();
            console.log(this.form);
            this.$http.AddTrainer(this.form)
                .then(response => {
                    this.$parent.state = 0;
                    this.$blockUI.Stop();
                    this.success=true,
                        this.$parent.GetTrainers();
                    this.$message({
                        type: 'info',
                        message: response.data
                    });
                })
                .catch((err) => {
                    this.$message({
                        type: 'error',
                        message: err.response.data
                    });
                });
        }

      
    }
}