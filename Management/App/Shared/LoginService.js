﻿import axios from 'axios';

axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

const baseUrl = 'https://localhost:4810/api';


export default {

    loginUserAccount(user) {       
        return axios.post(`/Users/loginUser`, user);
    },
   

    //ResetPassword(email) {
    //    return axios.post(baseUrl + '/security/ResetPassword/' + email);
    //},   
}