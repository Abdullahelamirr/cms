﻿import Quill from 'quill';

export default {
    name: 'editor',  
    
    mounted() {   
        var quill = new Quill('#editor', {
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['link', 'image'],
                    ['blockquote', 'code-block'],

                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction

                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],

                    ['clean']                                         // remove formatting button
                ]

               
            },
            theme: 'snow'
        });

        var $this = this;


        quill.on('text-change', function (delta, oldDelta, source) {             
            $this.$parent.text = quill.root.innerHTML;                 

            //console.log(quill.root.innerHTML);
            //console.log(quill.container.firstChild.innerHTML);
        });
    },
    props: ['html'],
    data() {
        return {
           
        };
        
    },
    methods: {
        
    }   
}
