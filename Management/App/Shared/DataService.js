﻿﻿import axios from 'axios';

axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

const baseUrl = '/Api';
const userUrl = '/';

export default {

    GetUnReadNotificationsInHeader() {

        axios.defaults.headers.common['Authorization'] = 'Bearer '
            + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Notifications/GetUnReadNotificationsInHeader`);

    },
    Login(loginName, password, secretNo) {
        return axios.post(baseUrl + '/security/login', { loginName, password, secretNo });
    },
    Logout() {
        return axios.post(baseUrl + '/security/logout');
    },
    ChangePassword(userPassword) {
        return axios.post(userUrl + `/ChangePassword`, userPassword);
    },
    EditUsersProfile(User) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');

        return axios.post(userUrl + '/EditUsersProfile', User);
    },
    UploadImageUser(obj) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(userUrl + `/UploadImageUser`, obj);
    },
    GetUsers(pageNo, pageSize, UserType) {
        console.log(UserType);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(userUrl + `Users/GetUsers?pageno=${pageNo}&pagesize=${pageSize}&UserType=${UserType}`);
    },
    AddUser(User) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(userUrl + 'Users/AddUser', User);
    },
    ActivateUser(UserId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(userUrl + `/Users/${UserId}/Activate`);
    },
    DeactivateUser(UserId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(userUrl + `/Users/${UserId}/Deactivate`);
    },
    EditUser(User) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(userUrl + '/Users/EditUser', User);
    },

    CheckLoginStatus() {
        return axios.post('/security/checkloginstatus');
    },
    //*********************  Notifications Service *****************************
    AddNotification(Notification) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Notifications/SentNotification`, Notification);
    },
    GetUnReadNotificationsByUsers(pageNo, pageSize, IsRead) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Notifications/GetUnReadNotificationsByUsers?pageno=${pageNo}&pagesize=${pageSize}&IsRead=${IsRead}`);
    },
    ReadUserNotification(Notification) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Notifications/ReadUserNotification`, Notification);
    },

    //*********************  Appointments Service *****************************
    GetAppointments(pageNo, pageSize) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Appointments/Get?pageno=${pageNo}&pagesize=${pageSize}`);
    },

    DeleteAppointments(AppointmentId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Appointments/${AppointmentId}/delete`);
    },
    AddAppointment(Appointment) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Appointments/Add`, Appointment);
    },
    EditAppointments(Appointment) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Appointments/Edit`, Appointment);
    },

    //********************* Location Service *****************************
    GetLocations(pageNo, pageSize, CityId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Locations/AllLocations?pageno=${pageNo}&pagesize=${pageSize}&CityId=${CityId}`);

    },
    AddLocations(Location) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Locations/Add`, Location);
    },
    //********************* Cities Service *****************************

    GetCountries(CountryId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Locations/Get?CountryId=${CountryId}`);
    },
    GetCountriesByType(pageNo, pageSize, type) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/Countries/GetCountriesByType?pageno=${pageNo}&pagesize=${pageSize}&type=${type}`);
    },
    AddCountries(form) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Countries/Add`, form);
    },

    DeleteCountry(countriesId) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');


        return axios.post(baseUrl + `/admin/Countries/${countriesId}/DeleteCountry`);
    },
    //********************* Agenda Service *****************************
    GetEventsAgenda(pageNo) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/EventsAgenda/Get?pageNo=${pageNo}`);
    },

    GetAgendByEventId(pageNo, pageSize, EventId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/EventsAgenda/Get?pageNo=${pageNo}&pagesize=${pageSize}&EventId=${EventId}`);
    },
    AddEventAgenda(form) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/EventsAgenda/Add`, form);
    },

    DeleteAgenda(AgendaId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/EventsAgenda/${AgendaId}/delete`);
    },
    GetEventsV2(pageNo, EventId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetEventsV2?pageNo=${pageNo}&EventId=${EventId}`);
    },
    GetEventsV3() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetEventsV3`);
    },
    EditAgenda(agenda) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/EventsAgenda/Edit`, agenda);
    },


    //************Events Service******************************************
    GetEventsByStudentId(pageNo, pageSize, StudentId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetEventsByStudentId?pageno=${pageNo}&pagesize=${pageSize}&StudentId=${StudentId}`);
    },
    GetEvents(pageNo, pageSize, EventType, SPCId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/Get?pageno=${pageNo}&pagesize=${pageSize}&EventType=${EventType}&SPCId=${SPCId}`);
    },

    GetCustomers(pageNo, pageSize, CustomerType, CompanyId, studentId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetCustomers?pageno=${pageNo}&pagesize=${pageSize}&CustomerType=${CustomerType}&CompanyId=${CompanyId}&studentId=${studentId}`);

    },
    DeleteActiveStudent(StudentId, eventId, InvoiceId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Events/${StudentId}/${eventId}/${InvoiceId}/delete`);
    },
    GetActiveStudents(pageNo, pageSize, eventId, studentId, InvoiceId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetActiveStudents?pageno=${pageNo}&pagesize=${pageSize}&eventId=${eventId}&studentId=${studentId}&InvoiceId=${InvoiceId}`);
    },
    GetInvoicesByEventId(pageNo, pageSize, EventId, SubscriberType, Subscriber) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetInvoicesByEventId?pageno=${pageNo}&pagesize=${pageSize}&EventId=${EventId}&SubscriberType=${SubscriberType}&Subscriber=${Subscriber}`);
    },
    GetSubscribersByEventId(EventId, SubscriberType) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetSubscribersByEventId?&EventId=${EventId}&SubscriberType=${SubscriberType}`);
    },
    GetPackageByeventType(EventType) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Events/GetPackageBytype?EventType=${EventType}`);
    },
    AddEvents(events) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Events/Add`, events);
    },
    AddActiveStudents(InvoiceId, EventId, ActualStudentList) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Events/${InvoiceId}/${EventId}/AddActiveStd`, ActualStudentList);
    },

    //**Event promotes Files
    AddEventFile(EventFile) {
       
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/EventFiles/Add`, EventFile);
    },
    GetEventFilesByEventId(pageNo, pageSize, EventId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/EventFiles/GetEventFilesByEventId?pageno=${pageNo}&pagesize=${pageSize}&EventId=${EventId}`);
    },

    //********************* Student Service *****************************
    GetStudents(pageNo, pageSize, UserType, CompanyId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]')
            .getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/Students/Get?pageno=${pageNo}&pagesize=${pageSize}&UserType=${UserType}&CompanyId=${CompanyId}`);
    },

    DeleteStudent(StudentId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Students/${StudentId}/delete`);
    },
    AddStudent(Student) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Students/Add`, Student);
    },
    EditStudent(Student) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Students/Edit`, Student);
    },
    Search(Object) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Students/Search`, Object);
    },
    UploadImage(obj) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + '/Admin/Students/UploadImage', obj);
    },

    //********************* Trainers Service *****************************

    AddTrainer(trainer) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Trainers/Add`, trainer);
    },
    GetTrainers(pageNo, pageSize) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/Trainers/GetTrainers?pageno=${pageNo}&pagesize=${pageSize}`);
    },
    EditTrainer(trainer) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Trainers/Edit`, trainer);
    },
    DeleteTrainer(TrainerId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Trainers/${TrainerId}/delete`);
    },

    //******************************************* Company Service *********************************
    GetCompanies() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Companies/Get`);
    },
    GetCompaniesV1(pageNo, pageSize) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Companies/GetCompanies?pageno=${pageNo}&pagesize=${pageSize}`);
    },
    DeleteCompany(CompanyId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Companies/${CompanyId}/delete`);
    },
    AddCompany(Company) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Companies/Add`, Company);
    },
    EditCompany(Company) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Companies/Edit`, Company);
    },
    //********************* Super Packages Service *****************************

    GetPackagesAndCoursesBySuperPackageId(objPackage) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/SuperPackages/GetPackagesAndCoursesBySuperPackageId?superPackageId=${objPackage.superPackageId}&packageId=${objPackage.packageId}&courseId=${objPackage.courseId}&classType=${objPackage.classType}`);
    },
    GetSuperPackages() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/SuperPackages/Get`);
    },
    GetSuperPackagesV1(pageNo, pageSize) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/SuperPackages/GetSuperPackages?pageno=${pageNo}&pagesize=${pageSize}`);
    },
    GetSuperPackagesContainsPackagesOnly() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/SuperPackages/GetSuperPackagesContainsPackagesOnly`);
    },
    GetSuperPackagesContainsPackagesAndCourses() {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/SuperPackages/GetSuperPackagesContainsPackagesAndCourses`);
    },
    DeleteSuperPackage(SuperPackageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/SuperPackages/${SuperPackageId}/delete`);
    },
    UploadImagesuperPackage(obj) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/SuperPackages/UploadImagesuperPackage`, obj);
    },
    UploadImagePackage(obj) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Packages/UploadImagePackage`, obj);
    },
    AddSuperPackage(SuperPackage) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/SuperPackages/Add`, SuperPackage);
    },
    EditSuperPackage(SuperPackage) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/SuperPackages/Edit`, SuperPackage);
    },
    //************* Courses ************
    GetCoursesBySuperPackageId(pageNo, pageSize, superPakcageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Courses/GetCoursesBySuperPackageId?pageno=${pageNo}&pagesize=${pageSize}&SuperPackageId=${superPakcageId}`);
    },
    UploadImageCourse(obj) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Courses/UploadImageCourse`, obj);
    },
    GetCoursesByPackageId(pageNo, pageSize, PakcageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Courses/GetCoursesByPackageId?pageno=${pageNo}&pagesize=${pageSize}&PackageId=${PakcageId}`);
    },
    DeleteCourse(CourseId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Courses/${CourseId}/delete`);
    },
    AddCourse(Course) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Courses/Add`, Course);
    },
    EditCourse(Course) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Courses/Edit`, Course);
    },
    AddCourseFiles(CourseFile) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/CourseFiles/Add`, CourseFile);
    },
    GetCourseFilesByCourseId(pageNo, pageSize, CourseId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/CourseFiles/GetCourseFilesByCourseId?pageno=${pageNo}&pagesize=${pageSize}&CourseId=${CourseId}`);
    },
    //****************** Packages *************************
    GetPackagesBySuperPackageId(pageNo, pageSize, superPakcageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl
            + `/Admin/Packages/GetPackagesBySuperPackageId?pageno=${pageNo}&pagesize=${pageSize}&SuperPackageId=${superPakcageId}`);
    },
    GetPackagesV2(superPackageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Packages/Get?superPackageId=` + superPackageId);
    },
    DeletePackage(PackageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Packages/${PackageId}/delete`);
    },
    AddPackage(Package) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Packages/Add`, Package);
    },
    EditPackage(Package) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/admin/Packages/Edit`, Package);
    },
    GetPackages(pageNo, pageSize, superPakcageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl +
            `/Admin/Packages/GetPackages?pageno=${pageNo}&pagesize=${pageSize}&SuperPackageId=${superPakcageId}`);
    },

    // ************************* Invoices *******************************************
    GetSubscriber(SubscriberType) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Invoice/GetSubscribers?SubscriberType=${SubscriberType}`);
    },
    GetInvoice(pageNo, pageSize, SubscribType, Subscriber) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Invoice/Get?pageno=${pageNo}&pagesize=${pageSize}&SubscriberType=${SubscribType}&Subscriber=${Subscriber}`);
    },
    AddInvoice(SubscriberId, SubscribType, RegistryType, TotalPriceAll, AllDiscount, TotalPriceAfterDescountAll, invoiceList) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Invoice/${SubscriberId}/${SubscribType}/${RegistryType}/${TotalPriceAll}/${AllDiscount}/${TotalPriceAfterDescountAll}/Add`, invoiceList);
    },

    EditInvoice(invoiceDetailsId, InvoiceId, RegistryType, TotalPriceAll, AllDiscount, TotalPriceAfterDescountAll, form) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Invoice/${invoiceDetailsId}/${InvoiceId}/${RegistryType}/${TotalPriceAll}/${AllDiscount}/${TotalPriceAfterDescountAll}/Edit`, form);
    },
    GetInvoiceDetails(InvoiceId) {
        // axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Invoice/GetInvoiceDetails/${InvoiceId}`);
    },
    DeleteInvoice(InvoiceId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Invoice/${InvoiceId}/DeleteInvoice`);
    },
    //Payments
    GetPayments(invoiceId) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/Payments/GetPayments/${invoiceId}`);
    },
    AddPayments(Payment) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Payments/AddPayments`, Payment);
    },
    DeletePaid(PaymentsId, invoiceId) {
        console.log(PaymentsId, PaymentsId);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(baseUrl + `/Admin/Payments/${PaymentsId}/${invoiceId}/DeletePaid`);
    },


    ConfirmInvoice(InvoiceId, Status) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/admin/Invoice/${InvoiceId}/${Status}/ConfirmInvoice`);
    },
    Sent(MessageContent) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(`/api/admin/Messages/SendMessage`, MessageContent);
    },
    GetMessages(isRead) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(`/api/admin/Messages/GetMessages?IsRead=${isRead}`);
    },
    MarkMessages(MessageTransactionId, IsRead) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(`/api/admin/Messages/MarkMessages?MessageTransactionId=${MessageTransactionId}&isRed=${IsRead}`);
    },
    MarkAllMessages(Messages) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.post(`/api/admin/Messages/MarkAllMessages`, Messages);
    },
    GetMessageDetails(MessageId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(`/api/admin/Messages/GetMessagesDetails?MessageId=${MessageId}`);
    },
    GetUserSelect(UserType, officeType, officeId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + document.querySelector('meta[name="api-token"]').getAttribute('content');
        return axios.get(baseUrl + `/Admin/User/GetUserSelect?UserType=${UserType}&officeType=${officeType}&officeId=${officeId}`);
    }

}