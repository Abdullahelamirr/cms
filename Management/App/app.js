﻿import Vue from 'vue';
import VueI18n from 'vue-i18n';
import VueRouter from 'vue-router';
import VueGoogleCharts from 'vue-google-charts'
import ElementUI from 'element-ui';
import Vuetify from 'vuetify';
import locale from 'element-ui/lib/locale/lang/en';
import BlockUIService from './Shared/BlockUIService';
import Layout from './Components/Layout/Layout.vue';
import Home from './Components/Home/Home.vue';
import Students from './Components/Students/Students.vue';
import Companies from './Components/Companies/Companies.vue';
import Packages from './Components/Packages/Packages.vue';
import SuperPackages from './Components/Packages/SuperPackages/SuperPackages.vue';
import SubPackages from './Components/Packages/SubPackages/SubPackages.vue';
import SubPackagesMain from './Components/SubPackages/SubPackages.vue';
import Courses from './Components/Packages/Courses/Courses.vue';
import CoursesMain from './Components/Courses/Courses.vue';
import SubPackageCourses from './Components/Packages/SubPackages/Courses/Courses.vue';
import CourseFiles from './Components/CourseFiles/CourseFiles.vue';

import Trainers from './Components/Trainers/Trainers.vue';
import Users from './Components/Users/Users.vue';
import EventLocation from './Components/EventLocations/EventLocations.vue';
import Events from './Components/Events/Events.vue';
import EventAgenda from './Components/EventAgenda/EventAgenda.vue';
import EventFiles from './Components/EventsPromotFiles/EventsPromotFiles.vue'
import Appointments from './Components/Appointments/Appointments.vue';
import EditUsersProfile from './Components/Users/EditUsersProfile/EditUsersProfile.vue';
import ChangePassword from './Components/Users/ChangePassword/ChangePassword.vue';
import Invoice from './Components/Invoice/Invoice.vue';
import EventTrainingKit from './Components/EventTrainingKit/EventTrainingKit.vue'
//import EventTrainingKitSP from './Components/Events/EventTrainingKit/EventTrainingKitSP/EventTrainingKitSP.vue'
//import Notifications from './Components/Notifications/Notifications.vue'
import ShowMessages from './Components/Messages/ShowMessages/ShowMessages.vue';
import SentMessages from './Components/Messages/SentMessages/SentMessages.vue';
import MessageDetails from './Components/Messages/MessageDetails/MessageDetails.vue';



import DataService from './Shared/DataService';

import messages from './i18n';
Vue.use(Vuetify);
Vue.use(VueI18n);
Vue.use(VueRouter);
Vue.use(VueGoogleCharts)
Vue.use(ElementUI, { locale });

Vue.config.productionTip = false;

Vue.prototype.$http = DataService;
Vue.prototype.$blockUI = BlockUIService;

export const eventBus = new Vue();

const i18n = new VueI18n({
    locale: 'ar', // set locale
    messages
});

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    linkActiveClass: 'active',
    routes: [
        { path: '/', component: Home },
        //{ path: '/Students/:companyId?/:city?', component: Students },
        { path: '/Students', component: Students },
        { path: '/Companies', component: Companies },
        {
            path: '/Packages',
            component: Packages,
            children: [
                { path: '', redirect: 'SuperPackages' },
                { path: 'SuperPackages', component: SuperPackages },
                { path: 'SubPackages/:SuperPackageId', component: SubPackages },
                { path: 'Courses/:SuperPackageId', component: Courses },
                { path: 'Courses/CourseFiles/:CourseId', component: CourseFiles },

                { path: 'SubPackages/Courses/:PackageId', component: SubPackageCourses }
            ]
        },
        { path: '/SubPackages', component: SubPackagesMain },
        { path: '/Courses', component: CoursesMain },

        { path: '/Courses/CourseFiles/:CourseId', component: CourseFiles },
        { path: '/Trainers', component: Trainers },
        { path: '/Users', component: Users },
        { path: '/EventLocation', component: EventLocation },
        //{ path: '/EventTrainingKit/:Event', component: EventTrainingKit }  // when use object as httpGet parameters,
        { path: '/EventTrainingKit/:Event', component: EventTrainingKit },
        { path: 'EventTrainingKitSUP/:SubPackageId', component: SubPackages },
        { path: 'EventTrainingKitCourses/:CourseId', component: Courses },
        { path: 'Courses/CourseFiles/:CourseId', component: CourseFiles },
        { path: '/Events', component: Events },
        { path: '/EventAgenda/:EventId', component: EventAgenda },
        { path: '/EventsPromotFiles/:EventId', component: EventFiles },
        { path: '/Appointments', component: Appointments },
        { path: '/EditUsersProfile', component: EditUsersProfile },
        { path: '/ChangePassword', component: ChangePassword },
        { path: '/UserRegister', component: Invoice },
        { path: '/ShowMessages', component: ShowMessages },
        { path: '/SentMessages', component: SentMessages },
 { path: '/MessageDetails/:MessageId', component: MessageDetails },

        //{ path: '/UsersNotifications', component: Notifications } ,
        {
            path: '*', component: { template: '<h1>الصفحة غير موجودة</h1>' }
        }
    ]

});

//router.beforeEach((to, from, next) => {
//    if (!to.matched.length) {
//        next('/notFound');
//    } else {
//        next();
//    }
//})

Vue.filter('toUpperCase', function (value) {
    if (!value) return '';
    return value.toUpperCase();
});

new Vue({
    i18n,
    router,
    render: h => {
        return h(Layout);
    }

}).$mount('#app');
