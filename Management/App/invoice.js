﻿import Vue from 'vue'
import VueI18n from 'vue-i18n';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ar'
import VueRouter from 'vue-router';
import { Alert } from 'element-ui';

import Layout from './Invoice/Layout.vue';
import FirstInvoice from './Invoice/FisrtInvoice/FisrtInvoice.vue';
import DataService from './Shared/DataService';
import messages from './i18n';

Vue.use(VueI18n);
Vue.use(VueRouter);
Vue.use(ElementUI, { locale });
Vue.use(Alert);


Vue.config.productionTip = false;
Vue.prototype.$http = DataService;

const i18n = new VueI18n({
    locale: 'ar', // set locale
    messages
});

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    linkActiveClass: 'active',
    routes: [
        {
            path: '/Api/Admin/Invoice/Print/:InvoiceId', component: FirstInvoice
        },
        { path: '*', component: { template: '<h1>الصفحة غير موجودة</h1>' } }
    ]

});

Vue.filter('toUpperCase', function (value) {
    if (!value) return '';
    return value.toUpperCase();
});

new Vue({
    i18n,
    router,
    render: h => {
        return h(Layout);
    }

}).$mount('#appInvoice');


