﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Classess
{
    public class NotificationModal
    {
        public string MsgTitle { get; set; }
        public string Msg { get; set; }
        public short?[] UserType { get; set; }
        public short? Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
    }
}
