﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Classess
{
    public partial class CourseFilesClasse
    {
        public long CourseFileId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public String Pdf { get; set; }
        public long? CourseId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }
    }
}
