﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;



namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Companies")]
    public class CompaniesController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public CompaniesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetCompanies()
        {
            try
            {
                var CompaniesQuery = from p in db.Companies
                                     where p.Status == 1
                                     select p;

                var CompaniesList = (from p in CompaniesQuery
                                     orderby p.CreatedOn
                                     select new
                                     {
                                         CompanyName = p.CompanyName,
                                         CompanyId = p.CompanyId,
                                     }).ToList();

                return Ok(new { Companies = CompaniesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetCompanies")]
        public IActionResult GetCompanies(int pageNo, int pageSize)
        {
            try
            {

                IQueryable<Companies> CompaniesQuery;

                CompaniesQuery = from p in db.Companies
                                 where p.Status == 1
                                 select p;

                var CompaniesCount = (from p in CompaniesQuery
                                      select p).Count();

                var CompaniesList = (from p in CompaniesQuery
                                     orderby p.CreatedOn descending
                                     select new
                                     {
                                         p.Address,
                                         p.BoughtBy,
                                         p.City,
                                         p.Communicator,
                                         p.CompanyId,
                                         p.CompanyName,
                                         p.Description,
                                         p.Email,
                                         p.PhoneNumber,
                                         p.Status
                                     }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Companies = CompaniesList, count = CompaniesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Add")]
        public IActionResult AddCompany([FromBody] Companies Company)
        {
            try
            {
                if (Company == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                


                Company.CreatedBy = userId;
                Company.CreatedOn = DateTime.Now;
                Company.Status = 1;
                db.Companies.Add(Company);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الشركة بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditCompany([FromBody] Companies Company)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Companies = (from p in db.Companies
                                 where p.CompanyId == Company.CompanyId
                                 && (p.Status == 1)
                                 select p).SingleOrDefault();

                if (Companies == null)
                {
                    return BadRequest("خطأ بيانات الشركة غير موجودة");
                }

                Companies.CompanyName = Company.CompanyName;
                Companies.Description = Company.Description;
                Companies.Address = Company.Address;
                Companies.BoughtBy = Company.BoughtBy;
                Companies.City = Company.City;
                Companies.Communicator = Company.Communicator;
                Companies.Email = Company.Email;
                Companies.PhoneNumber = Company.PhoneNumber;
                Companies.Status = 1;
                Companies.UpdatedBy = userId;
                Companies.UpdatedOn = DateTime.Now;  
                db.SaveChanges();

                return Ok("تم تعديل بينات الشركة بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("{CompanyId}/delete")]
        public IActionResult DeleteCompany(long CompanyId)
        {
            try
            {
              
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Student = (from p in db.Students
                               where p.CompanyId == CompanyId
                               && (p.Status == 1)
                               select p).ToList();

                if (Student!=null)
                {
                    return StatusCode(402, "عفوا :لايمكنك المسح, يجب مسح افراد الشركة اولا ");
                }

                var Company = (from p in db.Companies
                               where p.CompanyId == CompanyId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Company == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Company.Status = 9;
                Company.UpdatedBy = userId;
                Company.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Company Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}
