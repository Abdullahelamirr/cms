﻿using System;
using System.Linq;
using Management.Models;
using Microsoft.AspNetCore.Mvc;
//using Management.Services;
using Microsoft.AspNetCore.Authorization;

namespace Management.Controllers
{

    [Produces("application/json")]
    [Route("api/admin/Messages")]
    public class MessagesController : Controller
    {

        private readonly CMSContext db;
        // private MessagesService messagesService;
        private long currentUserId;
        private Helper help;
        public MessagesController(CMSContext context)
        {
            this.db = context;
            //  this.messagesService = new MessagesService(this.db);
            this.currentUserId = new Helper().GetCurrentUser(HttpContext);
            help = new Helper();
        }

        //[AllowAnonymous]
        //[HttpGet("GetMessages")]
        //public IActionResult GetMessages(int userId)
        //{
        //    try
        //    {
        //        var result = this.messagesService.GetForUserId(userId).ToList();
        //        return Ok(new { Messages = result, count = result.Count() });
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(500, e.Message);
        //    }
        //}

        public class MessageContent
        {
            public string PayLoad { get; set; }
            public string Subject { get; set; }
            public int[] PermissionModale { get; set; }
            public int SentType { get; set; }
            public int[] UserModel { get; set; }
        }


        [HttpGet("GetMessages")]
        public IActionResult GetMessages(int IsRead)
        {
            // 1 - unread
            // 2 - Read
            //3- Read later
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                var MessageQuery = from p in db.MessageTransaction
                                   where p.RecivedBy == userId 
                                   select p;

                var MessageList = (from p in MessageQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                       p.IsRead,
                                       p.MessageId,
                                       p.MessageTransactionId,
                                       p.SentBy,
                                       p.RecivedBy,
                                       p.CreatedOn,
                                       p.Message.Subject,
                                       //TotalDays=(p.CreatedOn.Date - DateTime.Now.Date).TotalDays  
                                   }).Take(100).ToList();
                return Ok(new
                {
                    MessageList = MessageList,
                    all = MessageList.Count(),
                    Unred = MessageList.Where(x => x.IsRead == 1).Count(),
                    red = MessageList.Where(x => x.IsRead == 2).Count(),
                    later = MessageList.Where(x => x.IsRead == 3).Count()
                });

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("GetMessagesDetails")]
        public IActionResult GetMessagesDetails(long MessageId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                var MessageQuery = from p in db.MessageTransaction
                                   where p.RecivedBy == userId && p.MessageId == MessageId
                                   select p;

                var MessageDetails = (from p in MessageQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          p.Message.Payload,
                                          p.Message.CreatedOn,
                                          p.SentBy,
                                          p.Message.Subject,
                                          //TotalDays=(p.CreatedOn.Date - DateTime.Now.Date).TotalDays  
                                      }).SingleOrDefault();
                return Ok(new { MessageDetails = MessageDetails });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("MarkMessages")]
        public IActionResult MarkMessages(long MessageTransactionId, int isRed)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                var MessageQuery = (from p in db.MessageTransaction
                                    where p.MessageTransactionId == MessageTransactionId
                                    && p.RecivedBy == userId
                                    select p).SingleOrDefault();
                if (MessageQuery == null)
                {
                    StatusCode(404, "عفوا لايوجد اشعارات للتعديل");
                }

                MessageQuery.IsRead = isRed;
                MessageQuery.ModifiedBy = userId;
                MessageQuery.ModifiedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("تم قراءة الإشعار");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("MarkAllMessages")]
        public IActionResult MarkAllMessages([FromBody] MessageTransaction[] Messages)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                if (Messages == null)
                {
                    StatusCode(404, "عفوا لايوجد اشعارات للتعديل");
                }

                Messages.Where(o => o.IsRead == 1).ToList().ForEach(u =>
                {
                    u.IsRead = 2;
                    u.ModifiedBy = userId;
                    u.ModifiedOn = DateTime.Now;
                });

                db.UpdateRange(Messages);
                db.SaveChanges();
                return Ok("تم قراءة الإشعار");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        [HttpPost("SendMessage")]
        public IActionResult SendMessage([FromBody] MessageContent Msg)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                if (Msg.SentType == 2)
                {

                    Messages MsgData = new Messages();
                    MsgData.Subject = Msg.Subject;
                    MsgData.Payload = Msg.PayLoad;
                    MsgData.CreatedBy = userId;
                    MsgData.CreatedOn = DateTime.Now;
                    db.Messages.Add(MsgData);
                    db.SaveChanges();
                    foreach (var userType in Msg.PermissionModale)
                    {
                        // get userid array by userType             
                        long[] UsersIdList = db.Users.AsEnumerable().Where(x => x.UserType == userType)
                   .Select(r => (long)r.UserId)
                   .ToArray();
                        foreach (long RecivedBy in UsersIdList)
                        {
                            MessageTransaction MsgTran = new MessageTransaction();
                            MsgTran.IsRead = 1;
                            MsgTran.MessageId = MsgData.MesssageId;
                            MsgTran.SentBy = userId;
                            MsgTran.RecivedBy = RecivedBy;
                            MsgTran.CreatedBy = userId;
                            MsgTran.CreatedOn = DateTime.Now;
                            db.MessageTransaction.Add(MsgTran);
                        }
                    }
                    db.SaveChanges();
                    return Ok("تم ارسال التنبيه للمستخدمين بنجاح");

                }
                else
                {
                    Messages MsgData = new Messages();
                    MsgData.Subject = Msg.Subject;
                    MsgData.Payload = Msg.PayLoad;
                    MsgData.CreatedBy = userId;
                    MsgData.CreatedOn = DateTime.Now;
                    db.Messages.Add(MsgData);
                    db.SaveChanges();

                    foreach (long RecivedBy in Msg.UserModel)
                    {
                        MessageTransaction MsgTran = new MessageTransaction();
                        MsgTran.IsRead = 1;
                        MsgTran.MessageId = MsgData.MesssageId;
                        MsgTran.SentBy = userId;
                        MsgTran.RecivedBy = RecivedBy;
                        MsgTran.CreatedBy = userId;
                        MsgTran.CreatedOn = DateTime.Now;
                        db.MessageTransaction.Add(MsgTran);
                    }
                    db.SaveChanges();
                    return Ok("تم ارسال التنبيه للمستخدمين بنجاح");
                }

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet("ReadMessage")]
        public IActionResult ReadMessage(long messageId, long userId)
        {
            try
            {
                // var result = this.messagesService.ReadMessage(messageId, userId);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        //[AllowAnonymous]
        //[HttpPost("SendMessageV1")]
        //public IActionResult SendMessage([FromBody] Message msg)
        //{
        //    try
        //    {
        //        this.messagesService.SendMessage(msg);
        //        return Ok(msg);
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(500, e.Message);
        //    }
        //}
    }
}