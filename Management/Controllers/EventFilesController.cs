﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Management.Classess;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/EventFiles")]
    public class EventFilesController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public EventFilesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("GetEventFilesByEventId")]
        public IActionResult GetEventFilesByEventId(int pageNo, int pageSize, int EventId)
        {
            try
            {
                IQueryable<EventFiles> EventFilesQuery;
                EventFilesQuery = from p in db.EventFiles
                                  where p.Status == 1 && p.EventId == EventId
                                  select p;

                var EventFilesCount = (from p in EventFilesQuery
                                       select p).Count();

                var EventFilesList = (from p in EventFilesQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          Name = p.Name,
                                          Description = p.Description,
                                          EventId = p.EventId,
                                          EventFileId = p.EventFileId,
                                          p.Pdf,
                                          p.Picture,
                                          p.WordFile
                                      }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { EventFiles = EventFilesList, count = EventFilesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{CourseFileId}/delete")]
        public IActionResult DeleteCourse(long CourseFileId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Course = (from p in db.CourseFiles
                              where p.CourseFileId == CourseFileId
                              && (p.Status == 1)
                              select p).SingleOrDefault();

                if (Course == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Course.Status = 9;
                Course.UpdatedBy = userId;
                Course.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Course File Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddEventFiles([FromBody] EventFilesModal EventFile)
        {
            try
            {
                if (EventFile == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                EventFiles EventObj = new EventFiles();
                EventObj.EventId = EventFile.EventId;
                EventObj.Name = EventFile.Name;
                EventObj.Description = EventFile.Description;
                EventObj.CreatedBy = userId;

                if (EventFile.Pdf == null)
                {
                    EventObj.Pdf = null;
                }
                else
                {
                    EventObj.Pdf = Convert.FromBase64String(EventFile.Pdf.Substring(EventFile.Pdf.IndexOf(",") + 1));
                }  
                if (EventFile.Picture == null)
                {
                    EventObj.Picture = null;
                }
                else
                {
                    EventObj.Picture = Convert.FromBase64String(EventFile.Picture.Substring(EventFile.Picture.IndexOf(",") + 1));
                }



                if (EventFile.WordFile == null)
                {
                    EventObj.WordFile = null;
                }
                else
                {
                    EventObj.WordFile = Convert.FromBase64String(EventFile.WordFile.Substring(EventFile.WordFile.IndexOf(",") + 1));

                }
                EventObj.CreatedOn = DateTime.Now;
                EventObj.Status = 1;
                db.EventFiles.Add(EventObj);
                db.SaveChanges();
                return Ok("لقد قمت بتسـجيل الملفات بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Edit")]
        public IActionResult EditCourseFile([FromBody] CourseFiles CourseFile)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Courses = (from p in db.CourseFiles
                               where p.CourseFileId == CourseFile.CourseFileId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Courses == null)
                {
                    return BadRequest("خطأ بيانات الملف غير موجودة");
                }

                            Courses.Name = CourseFile.Name;
                            Courses.Description = CourseFile.Description;
                            Courses.UpdatedBy = userId;
                            Courses.UpdatedOn = DateTime.Now;
                            db.SaveChanges();
                return Ok("تم تعديل بينات الملف بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }



        [HttpGet("{EventFileId}/File")]
        public IActionResult GetEventFile(long EventFileId)
        {
            try
            {

                var EventFile = (from p in db.EventFiles
                                 where p.EventFileId == EventFileId
                                 select p.Pdf).SingleOrDefault();
                string mimeType = "application/pdf";
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "PdfFile" + EventFileId);
                return File(EventFile, mimeType);


            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpGet("{EventFileId}/img")]
        public IActionResult GetEventImg(long EventFileId)
        {
            try
            {
                var EventFile = (from p in db.EventFiles
                                 where p.EventFileId == EventFileId
                                 select p.Picture).SingleOrDefault();
                string mimeType = "image/jpg";
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "img" + EventFileId);
                return File(EventFile, mimeType);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }
        [HttpGet("{EventFileId}/wordFile")]
        public IActionResult GetEventWordFile(long EventFileId)
        {
            try
            {
                var EventFile = (from p in db.EventFiles
                                 where p.EventFileId == EventFileId
                                 select p.WordFile).SingleOrDefault();
                string mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "wordFile" + EventFileId);
                return File(EventFile, mimeType);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }



    }
}
