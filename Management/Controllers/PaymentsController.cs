﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Payments")]
    public class PaymentsController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public PaymentsController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }





        [HttpGet("GetPayments/{invoiceId}")]
        public IActionResult GetPayments(long invoiceId)
        {
            try
            {

                var paymentQuery = from p in db.Payments
                               where p.Status == 1 && p.InvoiceId== invoiceId
                                   select p;
                var PaymentList = (from p in paymentQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                       PaymentsId= p.PaymentsId,
                                       Paids=p.Paids,
                                       NextPaidDate=p.NextPaidDate,
                                       CurrentPaidDate=p.CurrentPaidDate,
                                               PaymentType=p.PaymentType,
                                       PaymentRefrence= p.PaymentRefrence
                                   }).ToList();
                Double SumPaid = PaymentList.Select(x => x.Paids.Value).Sum();
                return Ok(new { Payment = PaymentList , SumPaid = SumPaid });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("AddPayments")]
        public IActionResult AddPayments([FromBody] Payments Payment)
        {
            try
            {
                if (Payment == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }
                var Invoice = (from p in db.Invoices
                               where p.InvoiceId == Payment.InvoiceId
                               select p).SingleOrDefault();
                if (Invoice is null)
                {
                    return StatusCode(401, "بيانات الفاتورة غير موجودة");
                }
              
                Invoice.Paid = Invoice.Paid+(Payment.Paids).Value;
                db.SaveChanges();
            

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                Payment.CreatedBy = userId;
                Payment.CreatedOn = DateTime.Now;
                Payment.CurrentPaidDate= DateTime.Now;
                Payment.Status = 1;
                db.Payments.Add(Payment);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الدفعات بنــجاح");
            }
            catch (Exception e)

            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("{PaymentsId}/{invoiceId}/DeletePaid")]
        public IActionResult DeletePaid(long PaymentsId,long invoiceId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Payment = (from p in db.Payments
                               where p.PaymentsId == PaymentsId
                               && (p.Status == 1)
                               select p).SingleOrDefault();
          
                if (Payment == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Payment.Status = 9;
                Payment.UpdatedBy = userId;
                Payment.UpdatedOn = DateTime.Now;
                db.SaveChanges();

                var Invoice = (from p in db.Invoices
                               where p.InvoiceId == Payment.InvoiceId
                               select p).SingleOrDefault();
                if (Invoice is null)
                {
                    return StatusCode(401, "بيانات الفاتورة غير موجودة");
                }

                Invoice.Paid = Invoice.Paid - (Payment.Paids).Value;
                db.SaveChanges();
                return Ok("Payment Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}