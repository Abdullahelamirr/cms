﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Classess;
using Management.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Packages")]
    public class PackagesController : Controller
    {

        private readonly CMSContext db;
        private Helper help;
        public PackagesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("GetPackagesBySuperPackageId")]
        public IActionResult GetPackagesBySuperPackageId(int pageNo, int pageSize, int SuperPackageId)
        {
            try
            {
                IQueryable<Packages> PackagesQuery;
                PackagesQuery = from p in db.Packages
                               where p.Status == 1 && p.SuperPackageId == SuperPackageId && p.SuperPackage.Status == 1 && p.SuperPackage.ContentType == 1
                                select p;

                var PackagesCount = (from p in PackagesQuery
                                    select p).Count();

                var PackagesList = (from p in PackagesQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                       Photo= p.Photo,
                                       Name = p.Name,
                                       Description = p.Description,
                                       PackageId = p.PackageId,
                                       Color = p.Color,
                                       Discount = p.Discount,
                                       PriceCompany = p.PriceCompany,
                                       PricePersonal = p.PricePersonal,
                                       Status = p.Status,
                                       SuperPackageId = p.SuperPackageId,
                                   }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Packages = PackagesList, count = PackagesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("Get")]
        public IActionResult GetPackages(int superPackageId)
        {
            try
            {
                var PackagesQuery = from p in db.Packages
                                         where p.Status == 1 && p.SuperPackageId== superPackageId
                                    select p;

                var PackagesList = (from p in PackagesQuery
                                    orderby p.CreatedOn
                                    select new
                                    {
                                       content= p.SuperPackage.ContentType,
                                        p.Color,
                                        p.Photo,
                                        p.Description,
                                        p.Discount,
                                        p.Name,
                                        p.PriceCompany,
                                        p.PricePersonal,
                                        p.SuperPackageId,
                                        id = p.PackageId  
                                        
                                    }).ToList();
                return Ok(new { Packages = PackagesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }



        [HttpGet("GetPackages")]
        public IActionResult GetPackages(int pageNo, int pageSize, int SuperPackageId)
        {
            try
            {
                IQueryable<Packages> PackagesQuery;

                if (SuperPackageId == 0)
                {
                    PackagesQuery = from p in db.Packages
                                    where p.Status == 1 && p.SuperPackage.Status==1 && p.SuperPackage.ContentType==1
                                    select p;
                }
                else { 
                PackagesQuery = from p in db.Packages
                                where p.Status == 1 && p.SuperPackageId == SuperPackageId && p.SuperPackage.Status == 1 && p.SuperPackage.ContentType == 1
                                select p;
                }
                var PackagesCount = (from p in PackagesQuery
                                     select p).Count();

                var PackagesList = (from p in PackagesQuery
                                    orderby p.CreatedOn descending
                                    select new
                                    {
                                        Name = p.Name,
                                        Photo = p.Photo,
                                        Description = p.Description,
                                        PackageId = p.PackageId,
                                        Color = p.Color,
                                        Discount = p.Discount,
                                        PriceCompany = p.PriceCompany,
                                        PricePersonal = p.PricePersonal,
                                        Status = p.Status,
                                        SuperPackageId = p.SuperPackageId,
                                    }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Packages = PackagesList, count = PackagesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [HttpGet]
        public IActionResult GetPackageImage(long PackageId)
        {
            try { 
            var userimage = (from p in db.Packages
                             where p.PackageId == PackageId
                             select p.Photo).SingleOrDefault();

            return File(userimage, "image/jpg");}
              catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [HttpPost("UploadImagePackage")]
        public IActionResult UploadImagePackage([FromBody] PackagesObject Package)
        {
            var userId = this.help.GetCurrentUser(HttpContext);

            var cPackages = (from u in db.Packages
                            where (u.PackageId == Package.PackageId) && u.Status != 9
                            select u).SingleOrDefault();

            if (cPackages == null)
            {
                return BadRequest("عفوا هدا الحزمه غير موجود");
            }

            cPackages.Photo = Convert.FromBase64String(Package.Photo.Substring(Package.Photo.IndexOf(",") + 1));
            //    cStudent.UpdatedBy = SuperPackage.;
            cPackages.UpdatedOn = DateTime.Now;
            db.SaveChanges();
            return Ok("تم تغير الصورة بنـجاح");

        }

        [HttpPost("{PackageId}/delete")]
        public IActionResult DeletePackage(long PackageId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Courses = (from p in db.Courses
                               where p.PackageId == PackageId
                               && (p.Status == 1)
                               select p).ToList();

                if (Courses.Count>0)
                {
                    return StatusCode(401, "عفوا :لايمكنك المسح, يجب مسح الـدورات اولا");
                }

                var Package = (from p in db.Packages
                              where p.PackageId == PackageId
                              && (p.Status == 1)
                              select p).SingleOrDefault();

                if (Package == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

               

                Package.Status = 9;
                Package.UpdatedBy = userId;
                Package.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Package Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddPackage([FromBody] Packages Package)
        {
            try
            {
                if (Package == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                Package.CreatedBy = userId;
                Package.CreatedOn = DateTime.Now;
                Package.Status = 1;
                db.Packages.Add(Package);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الحزمة بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditPackage([FromBody] Packages Package)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Packages = (from p in db.Packages
                               where p.PackageId == Package.PackageId
                               && (p.Status == 1)
                               select p).SingleOrDefault();
                if (Packages == null)
                {
                    return BadRequest("خطأ بيانات الحزمة غير موجودة");
                }
                Packages.Color = Package.Color;
                Packages.Description = Package.Description;
                Packages.Discount = Package.Discount;
                Packages.PricePersonal = Package.PricePersonal;
                Packages.PriceCompany = Package.PriceCompany;
                Packages.Name = Package.Name;
                Packages.Status = 1;
                Packages.UpdatedBy = userId;
                Packages.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("تم تعديل بينات الـحزمة بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
