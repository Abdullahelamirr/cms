﻿using System;
using System.Linq;
using Management.Controllers;
using Management.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Managegment.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Locations")]
    public class LocationsController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public LocationsController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetCountries(int CountryId)
        {  
            IQueryable<Countries> CountriesQuery ;     
            try
            {              
                if (CountryId != 0)
                {
                     CountriesQuery = from p in db.Countries
                                      where   p.Status == 1 && p.IndexOfCity == CountryId
                                      select p;
                 }      
                 else
                {
                    CountriesQuery = from p in db.Countries
                                     where p.Status == 1 && p.IndexOfCity == null
                                     select p;
                }
               
             

               var CountriesList = CountriesQuery.ToList();

                return Ok(new { Countries = CountriesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }
      

        [HttpGet("AllLocations")]
        public IActionResult GetLocations(int pageNo, int pageSize, int? CityId)
        {
            try
            {
                IQueryable<Hosts> LocationsQuery;
                if (CityId != null)
                {
                      LocationsQuery = from p in db.Hosts
                                     where p.Status == 1 && p.CountriesId == CityId
                                       select p;
                }
                else
                {
                    LocationsQuery = from p in db.Hosts
                                     where p.Status == 1 
                                     select p;
                }
                
                var LocationsList = LocationsQuery.ToList();
                                    

                return Ok(new { Locations = LocationsList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

       


        [HttpPost("Add")]
        public IActionResult AddLocations([FromBody] Hosts host)
        {
            try
            {
                if (host == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }



                host.CreatedBy = userId;
                host.CreatedOn = DateTime.Now;
                host.Status = 1;
                db.Hosts.Add(host);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الموقع بنــجاح");
            }
            catch (DbUpdateException e)
            
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditLocation([FromBody] Hosts host)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Locations = (from p in db.Hosts
                                 where p.HostId == host.HostId
                                 && (p.Status == 1)
                                 select p).SingleOrDefault();

                if (Locations == null)
                {
                    return BadRequest("خطأ بيانات الموقع غير موجودة");
                }

                Locations.Name = host.Name;
                Locations.Description = host.Description;
               
                Locations.ContactNumber = host.ContactNumber;
                Locations.Email = host.Email;
                Locations.ContactNumber = host.ContactNumber;
                Locations.Status = 1;
                Locations.UpdatedBy = userId;
                Locations.UpdatedOn = DateTime.Now;  
                db.SaveChanges();

                return Ok("تم تعديل بيانات الموقع بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("{LocationId}/delete")]
        public IActionResult DeleteLocation(long Id)
        {
            try
            {
              
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Location = (from p in db.Hosts
                               where p.HostId == Id
                               && (p.Status == 1)
                               select p).SingleOrDefault();
                           
                if (Location == null)
                {
                    return NotFound("خــطأ :  غير موجود");
                }

                Location.Status = 9;
                Location.UpdatedBy = userId;
                Location.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("مسح بيانات الموقع بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}
