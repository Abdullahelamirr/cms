﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Classes;
using Management.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Trainers")]

    public class TrainersController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public TrainersController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("GetTrainers")]
        public IActionResult GetTrainers(int pageNo, int pageSize)
        {
            try
            {
                var TrainersQuery = from p in db.Trainers
                                     where p.Status == 1
                                     select p;

                var TrainersCount = TrainersQuery.Count();

                var TrainersList = (from p in TrainersQuery
                                    orderby p.CreatedOn descending
                                    select new
                                    {
                                        TrainerId = p.TrainerId,
                                        NameAr = p.NameAr,
                                        NameEn = p.NameEn,
                                        Nationality = p.Nationality,
                                        PhoneNumber1 = p.PhoneNumber1,
                                        PhoneNumber2 = p.PhoneNumber2,
                                        BirthDate = p.BirthDate,
                                        Location = p.Location,
                                        CurrentJob = p.CurrentJob,
                                        ExperinceNumbers = p.ExperinceNumbers,
                                        Status = p.Status,
                                        Email = p.Email,
                                        cv = ((p.Cv != null && p.Cv.Length > 0) ? true:false)
                                    }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Trainer = TrainersList, count = TrainersCount });
            }


            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }


        [HttpPost("Add")]
        public IActionResult AddTrainer([FromBody] TrainerObject trainer)
        {
            try
            {
                if (trainer == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                Trainers t = new Trainers();
                
                t.CreatedBy = userId;
                t.CreatedOn = DateTime.Now;
                t.BirthDate = trainer.BirthDate;
                t.CurrentJob = trainer.CurrentJob;
                t.Email = trainer.Email;
                t.ExperinceNumbers = trainer.ExperinceNumbers;
                t.Location = trainer.Location;
                t.NameAr = trainer.NameAr;
                t.NameEn = trainer.NameEn;
                t.Nationality = trainer.Nationality;
                t.PhoneNumber1 = trainer.PhoneNumber1;
                t.PhoneNumber2 = trainer.PhoneNumber2;
                if (!String.IsNullOrEmpty(trainer.CV))
                {
                    t.Cv = Convert.FromBase64String(trainer.CV.Substring(trainer.CV.IndexOf(",") + 1));
                }   
                t.Status = 1;
                db.Trainers.Add(t);
                db.SaveChanges();
                return Ok("لقد قمت بتسـجيل بيانات المتدرب بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditTrainer([FromBody] Trainers trainer)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Trainers = (from p in db.Trainers
                                where p.TrainerId == trainer.TrainerId
                                && (p.Status == 1)
                                select p).SingleOrDefault();

                if (Trainers == null)
                {
                    return BadRequest("خطأ بيانات المتدرب غير موجودة");
                }

                Trainers.NameAr = trainer.NameAr;
                Trainers.NameEn = trainer.NameEn;
                Trainers.Nationality = trainer.Nationality;
                Trainers.BirthDate = trainer.BirthDate;
                Trainers.Location = trainer.Location;
                Trainers.PhoneNumber1 = trainer.PhoneNumber1;
                Trainers.PhoneNumber2 = trainer.PhoneNumber2;
                Trainers.Email = trainer.Email;
                Trainers.CurrentJob = trainer.CurrentJob;
                Trainers.Location = trainer.Location;
                Trainers.ExperinceNumbers = trainer.ExperinceNumbers;
                Trainers.Status = 1;
                Trainers.UpdatedBy = userId;
                Trainers.UpdatedOn = DateTime.Now;
                db.SaveChanges();

                return Ok("تم تعديل بيانات المدرب بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{TrainerId}/delete")]
        public IActionResult DeleteTrainer(long TrainerId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Trainer = (from p in db.Trainers
                               where p.TrainerId == TrainerId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Trainer == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Trainer.Status = 9;
                Trainer.UpdatedBy = userId;
                Trainer.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Student Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpGet("{TrainerId}/File")]
        public IActionResult GetCourseFile(long TrainerId)
        {
            try
            {
                var TrainerFiles = (from p in db.Trainers
                                    where p.TrainerId == TrainerId
                                    select p.Cv).SingleOrDefault();
                string mimeType = "application/pdf";
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "TrainerFile" + TrainerId);
                return File(TrainerFiles, mimeType);
            } catch(Exception e)
            {
                return StatusCode(500, e.Message);
            }
               
        }





    }
}
