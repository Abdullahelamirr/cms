﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;



namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Appointments")]
    public class AppointmentsController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public AppointmentsController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetAppointments(int pageNo, int pageSize)
        {
            try
            {
                var AppointmentsQuery = from p in db.Appointments
                                     where p.Status == 1
                                     select p;

                var AppointmentsList = (from p in AppointmentsQuery
                                       orderby p.DateTimeFrom descending
                                        select new
                                        {
                                            p.DateTimeFrom,
                                            p.CompanyId,
                                            p.Company.Communicator,
                                            p.Company.CompanyName,
                                            p.Student.NameAr,
                                            p.Title,
                                            p.AppointmentStatus,
                                            p.Description,
                                            p.Status
                                        }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();


                return Ok(new { Appointments = AppointmentsList, count = AppointmentsList.Count() }); ;
                                    

              
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        //[HttpGet("GetAppointments")]
        //public IActionResult GetAppointments(int pageNo, int pageSize)
        //{
        //    try
        //    {

        //        IQueryable<Appointments> AppointmentsQuery;

        //        AppointmentsQuery = from p in db.Appointments
        //                            where p.Status == 1
        //                            select p;

        //        var AppointmentsList = (from p in AppointmentsQuery
        //                                orderby p.DateTimeFrom descending
        //                                select new
        //                                {
        //                                    p.DateTimeFrom,
        //                                    p.CompanyId,
        //                                    p.Company.Communicator ,
        //                                    p.Communicator,
        //                                    p.CompanyId,
        //                                    p.CompanyName,
        //                                    p.Description,
        //                                    p.Email,
        //                                    p.PhoneNumber,
        //                                    p.Status
        //                                }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();


        //        var CompaniesCount = (from p in CompaniesQuery
        //                              select p).Count();

        //        return Ok(new { Companies = CompaniesList, count = CompaniesCount });
        //    }
        //    catch (Exception e)
        //    {
        //        return StatusCode(500, e.Message);
        //    }
        //}


        [HttpPost("Add")]
        public IActionResult AddAppointment([FromBody] Appointments Appointment)
        {
            try
            {
                if (Appointment == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                


                Appointment.CreatedBy = userId;
                Appointment.CreatedOn = DateTime.Now;
                Appointment.Status = 1;
                db.Appointments.Add(Appointment);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الموعد بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditCompany([FromBody] Companies Company)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Companies = (from p in db.Companies
                                 where p.CompanyId == Company.CompanyId
                                 && (p.Status == 1)
                                 select p).SingleOrDefault();

                if (Companies == null)
                {
                    return BadRequest("خطأ بيانات الشركة غير موجودة");
                }

                Companies.CompanyName = Company.CompanyName;
                Companies.Description = Company.Description;
                Companies.Address = Company.Address;
                Companies.BoughtBy = Company.BoughtBy;
                Companies.City = Company.City;
                Companies.Communicator = Company.Communicator;
                Companies.Email = Company.Email;
                Companies.PhoneNumber = Company.PhoneNumber;
                Companies.Status = 1;
                Companies.UpdatedBy = userId;
                Companies.UpdatedOn = DateTime.Now;  
                db.SaveChanges();

                return Ok("تم تعديل بينات الشركة بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("{CompanyId}/delete")]
        public IActionResult DeleteCompany(long CompanyId)
        {
            try
            {
              
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Student = (from p in db.Students
                               where p.CompanyId == CompanyId
                               && (p.Status == 1)
                               select p).ToList();

                if (Student!=null)
                {
                    return StatusCode(402, "عفوا :لايمكنك المسح, يجب مسح افراد الشركة اولا ");
                }

                var Company = (from p in db.Companies
                               where p.CompanyId == CompanyId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Company == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Company.Status = 9;
                Company.UpdatedBy = userId;
                Company.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Company Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}
