﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/EventsAgenda")]
    public class EventsAgendaController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public EventsAgendaController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetEventsAgenda(int pageNo, int pageSize, int? EventId)
        {
            try
            {
                IQueryable<Agenda> EventsAgendaQuery;
                if (EventId != null)
                {
                    EventsAgendaQuery = from p in db.Agenda
                                  where p.Status == 1 && p.EventId == EventId
                                  select p;
                }

                else
                {
                    //EventsAgendaQuery = from p in db.Agenda
                    //              where p.Status == 1
                    //              select p;
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var EventsAgendaList = (from p in EventsAgendaQuery
                                        orderby p.FromToDate descending
                                     select new
                                     {
                                          AgendaId=p.AgendaId,
                                          EventId = p.EventId,
                                          EventName = p.Event.Name,
                                          EventLocation = p.Event.Host.Name,
                                          EventStart=p.Event.StartDate,
                                          EventEnd = p.Event.EndDate,
                                          Package=p.Event.Package.Name,
                                          SuperPackage=p.Event.SuperPackage.Name,
                                          Course=p.Event.Course.Name,
                                          p.Title,
                                          p.DateTimeFrom,
                                          p.Description,
                                          p.DateTimeTo,
                                          p.FromToDate ,
                                          p.Status ,
                                          p.CreatedOn
                                     }).ToList();

                return Ok(new { EventAgenda = EventsAgendaList,count= EventsAgendaList.Count() });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddAgenda([FromBody] Agenda agenda)
        {
            try
            {
                if (agenda == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }



                agenda.CreatedBy = userId;
                agenda.CreatedOn = DateTime.Now;
                agenda.Status = 1;
                db.Agenda.Add(agenda);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الاجنده بنــجاح");
            }
            catch (DbUpdateException e)

            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{AgendaId}/delete")]
        public IActionResult DeleteAgenda(long? AgendaId)
        {
            try
            {

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

               

               var EventsAgenda = (from p in db.Agenda
                                    where p.Status == 1 && p.AgendaId == AgendaId
                                    select p).SingleOrDefault();
               

                if (EventsAgenda == null)
                {
                    return NotFound("خــطأ : الاجنده غير موجود");
                }

                EventsAgenda.Status = 9;
                EventsAgenda.UpdatedBy = userId;
                EventsAgenda.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("تم مسح الاجنده");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Edit")]
        public IActionResult EditAgenda([FromBody] Agenda agenda)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Agendas = (from p in db.Agenda
                                where p.AgendaId == agenda.AgendaId
                                && (p.Status == 1)
                                select p).SingleOrDefault();

                if (Agendas == null)
                {
                    return BadRequest("خطأ بيانات الاجنده غير موجودة");
                }

                Agendas.Title = agenda.Title;
                Agendas.Description = agenda.Description;
                Agendas.EventId = agenda.EventId;
                Agendas.DateTimeFrom = agenda.DateTimeFrom;
                Agendas.DateTimeTo = agenda.DateTimeTo;
                Agendas.FromToDate = agenda.FromToDate;

                Agendas.Status = 1;
                Agendas.UpdatedBy = userId;
                Agendas.UpdatedOn = DateTime.Now;
                db.SaveChanges();

                return Ok("تم تعديل بيانات الاجنده بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }




    }
}