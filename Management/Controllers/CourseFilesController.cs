﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Management.Classess;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/CourseFiles")]
    public class CourseFilesController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public CourseFilesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("GetCourseFilesByCourseId")]
        public IActionResult GetCourseFilesByCourseId(int pageNo, int pageSize, int CourseId)
        {
            try
            {
                IQueryable<CourseFiles> CourseFilesQuery;
                CourseFilesQuery = from p in db.CourseFiles
                                   where p.Status == 1 && p.CourseId == CourseId
                                   select p;

                var CourseFilesCount = (from p in CourseFilesQuery
                                        select p).Count();

                var CourseFilesList = (from p in CourseFilesQuery
                                       orderby p.CreatedOn descending
                                       select new
                                       {
                                           Name = p.Name,
                                           Description = p.Description,
                                           CourseId = p.CourseId,
                                           CourseFileId = p.CourseFileId,
                                           PdfType = p.PdfType
                                       }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { CourseFiles = CourseFilesList, count = CourseFilesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{CourseFileId}/delete")]
        public IActionResult DeleteCourse(long CourseFileId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Course = (from p in db.CourseFiles
                              where p.CourseFileId == CourseFileId
                              && (p.Status == 1)
                              select p).SingleOrDefault();

                if (Course == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Course.Status = 9;
                Course.UpdatedBy = userId;
                Course.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Course File Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddCourseFile([FromBody] CourseFilesClasse CourseFile)
        {
            try
            {
                if (CourseFile == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                CourseFiles CourseObj = new CourseFiles();
                CourseObj.CourseId = CourseFile.CourseId;
                CourseObj.Name = CourseFile.Name;
                CourseObj.Description = CourseFile.Description;
                CourseObj.CreatedBy = userId;
                CourseObj.Pdf = Convert.FromBase64String(CourseFile.Pdf.Substring(CourseFile.Pdf.IndexOf(",") + 1));
                CourseObj.CreatedOn = DateTime.Now;
                CourseObj.Status = 1;
                db.CourseFiles.Add(CourseObj);
                db.SaveChanges();
                return Ok("لقد قمت بتسـجيل الملف بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Edit")]
        public IActionResult EditCourseFile([FromBody] CourseFiles CourseFile)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);
                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Courses = (from p in db.CourseFiles
                               where p.CourseFileId == CourseFile.CourseFileId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Courses == null)
                {
                    return BadRequest("خطأ بيانات الملف غير موجودة");
                }

                Courses.Name = CourseFile.Name;
                Courses.Description = CourseFile.Description;
                Courses.UpdatedBy = userId;
                Courses.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("تم تعديل بينات الملف بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpGet("{CourseFileId}/File")]
        public IActionResult GetCourseFile(long CourseFileId)
        {
            try
            {
                var CourseFiles = (from p in db.CourseFiles
                                   where p.CourseFileId == CourseFileId
                                   select p.Pdf).SingleOrDefault();
                string mimeType = "application/pdf";
                Response.Headers.Add("Content-Disposition", "inline; filename=" + "CourseFileFile" + CourseFileId);
                return File(CourseFiles, mimeType);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }




    }
}
