﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Invoice")]
    public class InvoiceController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public InvoiceController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }
        [HttpGet("Print/{InvoiceId}")]
        public IActionResult Print(int InvoiceId)
        {
            return View();
        }
        [HttpPost("{InvoiceId}/DeleteInvoice")]
        public IActionResult DeleteInvoice(long InvoiceId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Invoice = (from p in db.Invoices
                              where p.InvoiceId == InvoiceId
                              && (p.Status == 1)
                              select p).SingleOrDefault();

                if (Invoice == null)
                {
                    return NotFound("خــطأ : الفاتوره غير موجوده");
                }

                Invoice.Status = 9;
                Invoice.UpdatedBy = userId;
                Invoice.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Invoice Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [HttpGet("Get")]
        public IActionResult Get(int pageNo, int pageSize,int SubscriberType,int Subscriber)
        {
            try
            {
                IQueryable<Invoices> InvoiceQuery;
                // Bring All 
                if (SubscriberType != 0)
                {
                    if (Subscriber != 0)
                    {
                        if (SubscriberType == 1)
                        {
                            InvoiceQuery = from p in db.Invoices
                                           where p.Status == 1 && p.CustomerType == SubscriberType && p.CompanyId ==Subscriber
                                           select p;

                        } else
                        {
                            InvoiceQuery = from p in db.Invoices
                                           where p.Status == 1 && p.CustomerType == SubscriberType && p.StudentId == Subscriber
                                           select p;


                        }


                    }
                    else
                    {
                        InvoiceQuery = from p in db.Invoices
                                       where p.Status == 1 && p.CustomerType == SubscriberType
                                       select p;

                    }


                }
                else
                {
                    InvoiceQuery = from p in db.Invoices
                                   where p.Status == 1
                                   select p;
              
                }
               

                var InvoiceCount = (from p in InvoiceQuery
                                          select p).Count();

                var InvoiceSum = (from p in InvoiceQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                       InvoicePaid = p.Paid,
                                       TotalPriceAfterDescount = p.TotalPriceAfterDescount
                                   }).ToList();
                Double SumPaid= InvoiceSum.Select(x => x.InvoicePaid).Sum();
                Double SumTotal = InvoiceSum.Select(x => x.TotalPriceAfterDescount).Sum();


                var InvoiceList = (from p in InvoiceQuery
                                  orderby p.CreatedOn descending
                                  select new
                                  {
                                    p.InvoiceId,
                                    p.CustomerType,
                                    p.RegistryType,
                                    PersonalName = p.Student.NameAr,
                                    PersonalPhone =p.Student.PhoneNumber1,
                                    PersonalEmail=p.Student.Email,
                                    p.Company.CompanyName,
                                    ConmpanyPhoneNumber=p.Company.PhoneNumber,
                                    CompanyEmail=p.Company.Email,
                                    InvoicePaid = p.Paid,                                    
                                    InvoiceDiscount = p.Discount,
                                    InvoiceTotalPrice = p.TotalPrice,
                                    TotalPriceAfterDescount = p.TotalPriceAfterDescount
                                  }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Invoice = InvoiceList, count = InvoiceCount ,SumPaid=SumPaid,SumTotal=SumTotal});
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }




        [HttpGet("GetInvoiceDetails/{InvoiceId}")]
        public IActionResult GetInvoiceDetails(long InvoiceId)
        {
            try
            {

                var InvoiceQuery = from p in db.InvoicesDetails
                               where p.Status == 1 && p.InvoiceId==InvoiceId
                               select p;
                var InvoiceList = (from p in InvoiceQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                    //invoice
                                    p.Invoice.CreatedOn,
                                     p.Invoice.CustomerType,
                                     p.Invoice.Student.NameAr,
                                     p.Invoice.Student.Email,
                                     companyEmail = p.Invoice.Company.Email  ,
                                     p.Invoice.Company.CompanyName,
                                     p.Invoice.Paid,
                                     p.Invoice.TotalPrice,
                                     p.Invoice.TotalPriceAfterDescount,
                                     p.Invoice.InvoiceId,
                                     p.Invoice.RegistryType,
                                     p.Invoice.Discount,
                                     DetailsNumberOfChair=p.NumberOfChair,
                                     DetailsTotalPrice=p.TotalPrice,
                                     DetailsTotalPriceAfterDescount = p.TotalPriceAfterDescount,
                                     DeatilsDiscount=p.Discount,
                                     DetailsEventName=p.Event.Name,
                                       InvoiceDetailsId=p.InvoiceDetailsId,
                                       EventId=p.EventId
                                   }).ToList();
                int TotalNumberOfChair = InvoiceList.Select(x => x.DetailsNumberOfChair.Value).Sum();
                return Ok(new { Invoice = InvoiceList, TotalNumberOfChair = TotalNumberOfChair });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("{InvoiceId}/{Status}/ConfirmInvoice")]
        public IActionResult ConfirmInvoice(int InvoiceId,int Status)
        {
            try
            {
               var Invoice = (from p in db.Invoices
                                 where p.InvoiceId == InvoiceId
                                 select p).SingleOrDefault();
                if (Invoice is null)
                {
                    return StatusCode(401, "بيانات الفاتورة غير موجودة");
                }
                Invoice.RegistryType = Status;
                db.SaveChanges();
                return Ok("تم تأكيد الفاتورة بنجاح");
            } catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("GetSubscribers")]
        public IActionResult GetSubscribers(int SubscriberType)
        {
            IQueryable<Students> StudentsQuery;
            IQueryable<Companies> CompaniesQuery;

            if (SubscriberType == 1)
            {
                CompaniesQuery = from p in db.Companies
                               where p.Status == 1
                               select p;
                var SubscriberList = (from p in CompaniesQuery
                                   orderby p.CreatedOn descending
                                   select new
                                   {
                                       id=p.CompanyId,
                                       name = p.CompanyName
                                   }).ToList();

                return Ok(new { Subscriber = SubscriberList });
            } else
            {
                StudentsQuery = from p in db.Students
                               where p.Status == 1
                               select p;
                var SubscriberList = (from p in StudentsQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          id = p.StudentId,
                                          name = p.NameAr
                                      }).ToList();

                return Ok(new { Subscriber = SubscriberList });

            }
        }

        public partial class InvoiceData
        {
            public string Event { get; set; }
            public long EventId { get; set; }
            public int NumberOfChair { get; set; }
            public int Discount { get; set; }
            public double TotalPrice { get; set; }
            public double TotalPriceAfterDescount { get; set; }
        }

        [HttpPost("{SubscriberId}/{SubscribType}/{RegistryType}/{TotalPriceAll}/{AllDiscount}/{TotalPriceAfterDescountAll}/Add")]
        public IActionResult Add(long SubscriberId,int SubscribType,int RegistryType,float TotalPriceAll,int AllDiscount, float TotalPriceAfterDescountAll, [FromBody] InvoiceData[] invoice)
        {
            try
            {
                if (invoice == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                Invoices inv = new Invoices();
                if (SubscribType == 1)
                {
                    inv.CompanyId = SubscriberId;
                }
                else
                {
                    inv.StudentId = SubscriberId;
                }
                inv.RegistryType = (RegistryType==0? 1:2);
                inv.TotalPrice = TotalPriceAll;
                inv.CustomerType = SubscribType;
                inv.TotalPriceAfterDescount = TotalPriceAfterDescountAll;
                inv.Discount = AllDiscount;
                inv.Status = 1;
                inv.CreatedBy = userId;
                inv.CreatedOn = DateTime.Now;
                db.Invoices.Add(inv);
                db.SaveChanges();
                
                foreach (var x in invoice)
                {
                    InvoicesDetails invDet = new InvoicesDetails();
                    invDet.InvoiceId = inv.InvoiceId;
                    invDet.NumberOfChair = x.NumberOfChair;
                    invDet.TotalPrice = x.TotalPrice;
                    invDet.TotalPriceAfterDescount = x.TotalPriceAfterDescount;
                    invDet.Discount = x.Discount;
                    invDet.EventId = x.EventId;
                    invDet.Status = 1;
                    invDet.CreatedBy = userId;
                    invDet.CreatedOn = DateTime.Now;
                    db.InvoicesDetails.AddRange(invDet);
                    db.SaveChanges();

                }


                return Ok("لقد قمت بتسـجيل الفاتورة بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{invoiceDetailsId}/{InvoiceId}/{RegistryType}/{TotalPriceAll}/{AllDiscount}/{TotalPriceAfterDescountAll}/Edit")]
        public IActionResult Edit(int invoiceDetailsId, int InvoiceId, int RegistryType, float TotalPriceAll, int AllDiscount, float TotalPriceAfterDescountAll, [FromBody] InvoiceData invoice)
        {
            try
            {
                if (invoice == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                Invoices inv = new Invoices();
                var cInvoices = (from p in db.Invoices
                                where p.InvoiceId == InvoiceId
                                && (p.Status == 1)
                                select p).SingleOrDefault();
                var cinvoiceDetails = (from p in db.InvoicesDetails
                                 where p.InvoiceDetailsId == invoiceDetailsId
                                 && (p.Status == 1)
                                 select p).SingleOrDefault();
             



                if (cInvoices == null)
                {
                    return BadRequest("خطأ بيانات الفاتوره غير موجودة");
                }

                if (cinvoiceDetails == null)
                {
                    return BadRequest("خطأ بيانات تفاصيل الفاتوره غير موجودة");
                }
                cInvoices.RegistryType = RegistryType;
                cInvoices.TotalPrice = TotalPriceAll;
              
                cInvoices.TotalPriceAfterDescount = TotalPriceAfterDescountAll;
                cInvoices.Discount = AllDiscount;
                cInvoices.Status = 1;
                cInvoices.UpdatedBy = userId;
                inv.UpdatedOn = DateTime.Now;
                //db.Invoices.Add(inv);
                db.SaveChanges();



                cinvoiceDetails.NumberOfChair = invoice.NumberOfChair;
                cinvoiceDetails.TotalPrice = invoice.TotalPrice;
                cinvoiceDetails.TotalPriceAfterDescount = invoice.TotalPriceAfterDescount;
                cinvoiceDetails.Discount = invoice.Discount;
                cinvoiceDetails.EventId = invoice.EventId;
                cinvoiceDetails.Status = 1;
                cinvoiceDetails.UpdatedBy = userId;
                cinvoiceDetails.UpdatedOn = DateTime.Now;
        
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل الفاتورة بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }



    }
}