﻿using System;
using System.Linq;
using Management.Controllers;
using Management.Models;
using Microsoft.AspNetCore.Mvc;



namespace Managegment.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Countries")]
    public class CountriesController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public CountriesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetCountries()
        {
            try
            {
                var CountriesQuery = from p in db.Companies
                                     where p.Status == 1
                                     select p;

                var CompaniesList = (from p in CountriesQuery
                                     orderby p.CreatedOn
                                     select new
                                     {
                                         CompanyName = p.CompanyName,
                                         CompanyId = p.CompanyId,
                                     }).ToList();

                return Ok(new { Countries = CompaniesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetCountries")]
        public IActionResult GetCountries(int pageNo, int pageSize)
        {
            try
            {

                IQueryable<Companies> CountriesQuery;

                CountriesQuery = from p in db.Companies
                                 where p.Status == 1
                                 select p;

                var CountriesCount = (from p in CountriesQuery
                                      select p).Count();

                var CompaniesList = (from p in CountriesQuery
                                     orderby p.CreatedOn descending
                                     select new
                                     {
                                         p.Address,
                                         p.BoughtBy,
                                         p.City,
                                         p.Communicator,
                                         p.CompanyId,
                                         p.CompanyName,
                                         p.Description,
                                         p.Email,
                                         p.PhoneNumber,
                                         p.Status
                                     }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Companies = CompaniesList, count = CountriesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpGet("GetCountriesByType")]
        public IActionResult GetCountriesByType(int pageNo, int pageSize,int type)
        {
            try
            {
                IQueryable<Countries> CountriesQuery;
                //country
                if(type == 0)
                {
                    CountriesQuery = from p in db.Countries
                                     where p.Status == 1 && p.IndexOfCity == null
                                     select p;
                } else
                {
                    //city
                    CountriesQuery = from p in db.Countries
                                     where p.Status == 1 && p.IndexOfCity!=null
                                     select p;
                }
               

                var CountriesCount = (from p in CountriesQuery
                                      select p).Count();

                var CompaniesList = (from p in CountriesQuery
                                     orderby p.CreatedOn descending
                                     select new
                                     {
                                         p.CountryCityName,
                                         p.CountriesId,
                                         p.Status,
                                         p.IndexOfCity,              
                                     }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Country = CompaniesList, count = CountriesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        public partial class CountryObj
        {
            public bool IsCountry { get; set; }
            public int? CountryIdDialog { get; set; }
            public string CityName { get; set; }
            public string CountryName { get; set; }
        }

            [HttpPost("Add")]
        public IActionResult Add([FromBody] CountryObj Country)
        {
            try
            {
                if (Country == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                Countries count = new Countries();
                //count.
                if (Country.IsCountry)
                {
                    count.IndexOfCity = Country.CountryIdDialog;
                    count.CountryCityName = Country.CityName;
                }
                else
                {
                    count.CountryCityName = Country.CountryName;
                }

                count.CreatedBy = userId;
                count.CreatedOn = DateTime.Now;
                count.Status = 1;
                db.Countries.Add(count);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات البلاد/المدن بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    

        [HttpPost("{countriesId}/DeleteCountry")]
        public IActionResult DeleteCountry(long countriesId)
        {
            try
            {
                string massage = "";
                var country = (from p in db.Countries
                                    where p.CountriesId == countriesId
                                    && (p.Status == 1)
                                    select p).SingleOrDefault();

                if (country == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

           
                if (country.IndexOfCity != null) {

                   
                    country.Status = 9;
                    country.UpdatedBy = userId;
                    country.UpdatedOn = DateTime.Now;
                db.SaveChanges();

                    massage="لقد قمت الغاء بيانات البلاد/المدن بنــجاح";
                }
                else
                {
                    var city = (from p in db.Countries
                                   where p.IndexOfCity == countriesId
                                   && (p.Status == 1)
                                   select p).SingleOrDefault();
                    if (city == null)
                    {

                        country.Status = 9;
                        country.UpdatedBy = userId;
                        country.UpdatedOn = DateTime.Now;
                        db.SaveChanges();
                        massage = "لقد قمت الغاء بيانات البلاد/المدن بنــجاح";
                    }
                    else
                    {
                        return BadRequest("الرجاء مسح بيانات المدن قبل مسح البلاد");

                    }
                    

                }
               return Ok(massage);
             
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


     


    }
}
