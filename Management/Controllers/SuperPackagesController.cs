﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Classess;
using Management.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/SuperPackages")]
    public class SuperPackagesController : Controller
    {
        private readonly CMSContext db;
        private Helper help;
        public class packegobj
        {
            public int? packageId { get; set; }
            public int? superPackageId { get; set; }
            public int? courseId { get; set; }
            public int? classType { get; set; }

        }
        public SuperPackagesController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        [HttpGet("Get")]
        public IActionResult GetSuperPackages()
        {
            try
            {
                var SuperPackagesQuery = from p in db.SuperPackages
                                         where p.Status == 1
                                         select p;

                var SuperPackagesList = (from p in SuperPackagesQuery
                                         orderby p.CreatedOn
                                         select new
                                         {
                                             p.Color,
                                             p.Description,
                                             p.Discount,
                                             p.Name,
                                             p.PriceCompany,
                                             p.PricePersonal,
                                             p.SuperPackageId,
                                             p.Photo,
                                             id = p.SuperPackageId
                                         }).ToList();

                return Ok(new { SuperPackages = SuperPackagesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetSuperPackages")]
        public IActionResult GetSuperPackages(int pageNo, int pageSize)
        {
            try
            {
                IQueryable<SuperPackages> SuperPackagesQuery;
                SuperPackagesQuery = from p in db.SuperPackages
                                     where p.Status == 1
                                     select p;

                var SuperPackagesCount = (from p in SuperPackagesQuery
                                          select p).Count();

                var SuperPackagesList = (from p in SuperPackagesQuery
                                         orderby p.CreatedOn descending
                                         select new
                                         {
                                             p.Photo,
                                             p.Color,
                                             p.Description,
                                             p.Discount,
                                             p.Name,
                                             p.PriceCompany,
                                             p.PricePersonal,
                                             p.SuperPackageId,
                                             p.ContentType
                                         }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                return Ok(new { superPackage = SuperPackagesList, count = SuperPackagesCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }



        [HttpGet("GetSuperPackagesContainsPackagesOnly")]
        public IActionResult GetSuperPackagesContainsPackagesOnly()
        {
            try
            {
                IQueryable<SuperPackages> SuperPackagesQuery;
                SuperPackagesQuery = from p in db.SuperPackages
                                     where p.Status == 1 && p.ContentType == 1
                                     select p;

                var SuperPackagesList = (from p in SuperPackagesQuery
                                         orderby p.CreatedOn descending
                                         select new
                                         {
                                             p.Color,
                                             p.Description,
                                             p.Discount,
                                             p.Name,
                                             p.PriceCompany,
                                             p.PricePersonal,
                                             p.SuperPackageId,
                                             p.ContentType
                                         }).ToList();
                return Ok(new { superPackage = SuperPackagesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetSuperPackagesContainsPackagesAndCourses")]
        public IActionResult GetSuperPackagesContainsPackagesAndCourses()
        {
            try
            {
                IQueryable<SuperPackages> SuperPackagesQuery;
                SuperPackagesQuery = from p in db.SuperPackages
                                     where p.Status == 1 && (p.ContentType == 1 || p.ContentType == 2)
                                     select p;

                var SuperPackagesList = (from p in SuperPackagesQuery
                                         orderby p.CreatedOn descending
                                         select new
                                         {
                                             p.Color,
                                             p.Description,
                                             p.Discount,
                                             p.Name,
                                             p.PriceCompany,
                                             p.PricePersonal,
                                             p.SuperPackageId,
                                             p.ContentType,
                                             p.Photo
                                         }).ToList();
                return Ok(new { superPackage = SuperPackagesList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetPackagesAndCoursesBySuperPackageId")]
        public IActionResult GetPackagesAndCoursesBySuperPackageId(packegobj objPackage)
        {
            try
            {
                IQueryable<SuperPackages> SuperPackagesQuery;
                IQueryable<Packages> PackagesQuery;
                IQueryable<Courses> CoursesQuery;

                if (objPackage.classType == 1)
                {
                    SuperPackagesQuery = from p in db.SuperPackages
                                         where p.Status == 1 && (p.ContentType == 1 || p.ContentType == 2) &&
                                         p.SuperPackageId == objPackage.superPackageId
                                         select p;

                    var SuperPackagesList = (from p in SuperPackagesQuery
                                             orderby p.CreatedOn descending
                                             select new
                                             {
                                                
                                                 p.Color,
                                                 p.Description,
                                                 p.Discount,
                                                 PackageName = p.Name,
                                                 p.PriceCompany,
                                                 p.PricePersonal,
                                                 p.SuperPackageId,
                                                 p.ContentType,
                                                 p.Photo,

                                                 SubPackages = p.Packages.Select(x =>
                                              new
                                              {
                                                  x.PackageId,
                                                  x.Name,
                                                  x.Status,
                                                  x.Color,
                                                  Corses = x.Courses.Select(m =>
                                                 new
                                                 {
                                                     m.CourseId,
                                                     m.Name,
                                                     m.Status,
                                                     CourseFile = m.CourseFiles.Select(s =>
                                                       new
                                                       {
                                                           s.Pdf,
                                                           s.CourseFileId,
                                                           s.Name,
                                                           s.Status
                                                       }).Where(d => d.Status == 1).ToList()
                                                 }).Where(a => a.Status == 1).ToList()

                                              }).Where(a => a.Status == 1).ToList()
                                             }).ToList();

                    return Ok(new { Package = SuperPackagesList });

                }
                else if (objPackage.classType == 2)
                {
                    PackagesQuery = from p in db.Packages
                                    where p.Status == 1 && p.PackageId == objPackage.packageId
                                    select p;
                    var PackagesList = (from p in PackagesQuery
                                        orderby p.CreatedOn descending
                                        select new
                                        {
                                            PackageName = p.Name,
                                            PackageId = p.PackageId,
                                            PriceCompany = p.PriceCompany,
                                            PricePersonal = p.PricePersonal,
                                            Discount = p.Discount,
                                            p.Color,
                                            Corses = p.Courses.Select(m =>
                                                 new
                                                 {
                                                     m.CourseId,
                                                     m.Name,
                                                     m.Status,
                                                     CourseFile = m.CourseFiles.Select(s =>
                                                         new
                                                         {
                                                             s.Pdf,
                                                             s.CourseFileId,
                                                             s.Name,
                                                             s.Status
                                                         }).Where(d => d.Status == 1)
                                                 }).Where(a => a.Status == 1).ToList()
                                        }).ToList();
                    return Ok(new { Package = PackagesList });
                }
                else
                {
                    CoursesQuery = from p in db.Courses
                                   where p.Status == 1 && p.CourseId == objPackage.courseId
                                   select p;
                    var CourseList = (from p in CoursesQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          PackageName = p.Name,
                                          PackageId = p.CourseId,
                                          PriceCompany = p.PriceCompany,
                                          PricePersonal = p.PricePersonal,
                                          Discount = p.Discount,
                                          pkg = p.Package.Name,
                                          spkg = p.SuperPackage.Name,
                                          CourseFile = p.CourseFiles.Select(s =>
                                                         new
                                                         {
                                                             s.Pdf,
                                                             s.CourseFileId,
                                                             s.Name,
                                                             s.Status
                                                         }).Where(d => d.Status == 1)
                                      }).ToList();
                    return Ok(new { Package = CourseList });
                }

            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }



        [HttpPost("UploadImagesuperPackage")]
        public IActionResult UploadImagesuperPackage([FromBody] SuperPackgesObject SuperPackage)
        {
            var userId = this.help.GetCurrentUser(HttpContext);

            var cSuperPackage = (from u in db.SuperPackages
                                 where (u.SuperPackageId == SuperPackage.SuperPackageId) && u.Status != 9
                                 select u).SingleOrDefault();

            if (cSuperPackage == null)
            {
                return BadRequest("عفوا هدا الحزمه غير موجود");
            }

            cSuperPackage.Photo = Convert.FromBase64String(SuperPackage.Photo.Substring(SuperPackage.Photo.IndexOf(",") + 1));
            //    cStudent.UpdatedBy = SuperPackage.;
            cSuperPackage.UpdatedOn = DateTime.Now;
            db.SaveChanges();
            return Ok("تم تغير الصورة بنـجاح");

        }

        [HttpGet]
        public IActionResult GetsuperPackageImage(long SuperPackageId)
        {
            try
            {
                var userimage = (from p in db.SuperPackages
                                 where p.SuperPackageId == SuperPackageId
                                 select p.Photo).SingleOrDefault();

                return File(userimage, "image/jpg");
            }

            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddSuperPackage([FromBody] SuperPackages SuperPackage)
        {
            try
            {
                if (SuperPackage == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                SuperPackage.CreatedBy = userId;
                SuperPackage.CreatedOn = DateTime.Now;
                SuperPackage.Status = 1;
                db.SuperPackages.Add(SuperPackage);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات باقة الحزم بنــجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("Edit")]
        public IActionResult EditSuperPackage([FromBody] SuperPackages SuperPackage)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var SuperPackages = (from p in db.SuperPackages
                                     where p.SuperPackageId == SuperPackage.SuperPackageId
                                     && (p.Status == 1)
                                     select p).SingleOrDefault();

                if (SuperPackages == null)
                {
                    return BadRequest("خطأ بيانات الشركة غير موجودة");
                }

                SuperPackages.Color = SuperPackage.Color;
                SuperPackages.Description = SuperPackage.Description;
                SuperPackages.Discount = SuperPackage.Discount;
                SuperPackages.ContentType = SuperPackage.ContentType;
                SuperPackages.PricePersonal = SuperPackage.PricePersonal;
                SuperPackages.PriceCompany = SuperPackage.PriceCompany;
                SuperPackages.Name = SuperPackage.Name;
                SuperPackages.Status = 1;
                SuperPackages.UpdatedBy = userId;
                SuperPackages.UpdatedOn = DateTime.Now;
                db.SaveChanges();

                return Ok("تم تعديل بينات المتدرب بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpPost("{SuperPackageId}/delete")]
        public IActionResult DeleteSuperPackage(long SuperPackageId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var SuperPackage = (from p in db.SuperPackages
                                    where p.SuperPackageId == SuperPackageId
                                    && (p.Status == 1)
                                    select p).SingleOrDefault();

                if (SuperPackage == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                if (SuperPackage.ContentType == 1)
                {
                    var packages = (from p in db.Packages
                                    where p.SuperPackageId == SuperPackageId
                                    && (p.Status == 1)
                                    select p).ToList();
                    if (packages.Count > 0)
                    {
                        return NotFound("عفوا :لايمكنك المسح, يجب مسح الحزم الفردية اولا ");
                    }

                }

                if (SuperPackage.ContentType == 2)
                {
                    var Courses = (from p in db.Courses
                                   where p.SuperPackageId == SuperPackageId
                                   && (p.Status == 1)
                                   select p).ToList();
                    if (Courses.Count > 0)
                    {
                        return NotFound("عفوا :لايمكنك المسح, يجب مسح الدورات اولا ");
                    }
                }



                SuperPackage.Status = 9;
                SuperPackage.UpdatedBy = userId;
                SuperPackage.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("Super Package Deleted");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}
