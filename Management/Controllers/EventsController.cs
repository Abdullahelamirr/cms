﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Management.Controllers
{
    [Produces("application/json")]
    [Route("Api/Admin/Events")]
    public class EventsController : Controller
    {
        private readonly CMSContext db;
        private Helper help;

        public EventsController(CMSContext context)
        {
            this.db = context;
            help = new Helper();
        }

        public class Subscribers
        {
            public long id { get; set; }
            public String name { get; set; }
        }



        //SPCid
        // S for superpackage id
        // P for pacakge id
        // C for course id 
        // depending on event type

        [HttpGet("Get")]
        public IActionResult GetEvents(int pageNo, int pageSize, int EventType, long SPCId)
        {
            try
            {
                IQueryable<Events> EventsQuery;
                // Bring All 
                if (EventType == 0)
                {
                    EventsQuery = from p in db.Events
                                  where p.Status == 1
                                  select p;
                }
                else if (EventType == 1)
                {
                    if (SPCId != 0)
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.SuperPackageId == SPCId && p.SuperPackageId != null
                                      select p;
                    }
                    else
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.SuperPackageId != null
                                      select p;
                    }

                }
                else if (EventType == 2)
                {
                    if (SPCId != 0)
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.PackageId == SPCId
                                      select p;
                    }
                    else
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.PackageId != null
                                      select p;
                    }
                }
                else
                {
                    if (SPCId != 0)
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.CourseId == SPCId
                                      select p;
                    }
                    else
                    {
                        EventsQuery = from p in db.Events
                                      where p.Status == 1 && p.CourseId != null
                                      select p;

                    }
                }

                var EventCount = (from p in EventsQuery
                                  select p).Count();

                var EventsList = (from p in EventsQuery
                                  orderby p.CreatedOn descending
                                  select new
                                  {
                                      ClassId = p.EventId,
                                      ClassName = p.Name,
                                      Description = p.Description,
                                      ClassType = p.EventType,
                                      CourseName = p.Course.Name,
                                      PackageName = p.Package.Name,
                                      CourseId = p.CourseId,
                                      PackageId = p.PackageId,
                                      SuperPackageId = p.SuperPackageId,
                                      SuperPackageName = p.SuperPackage.Name,
                                      StartDate = p.StartDate,
                                      EndDate = p.EndDate,
                                      Status = p.Status,
                                      HostId = p.HostId,
                                      HostName = p.Host.Name,
                                      TeacherName = p.Teacher.NameAr,
                                      p.PriceCompany,
                                      p.PricePersonal,
                                      p.Discount,
                                      p.MaximumNumberChair
                                  }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Events = EventsList, count = EventCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetEventsV2")]
        public IActionResult GetEventsV2(int pageNo, long? EventId)
        {
            try
            {
                IQueryable<Events> EventsQuery;

                EventsQuery = from p in db.Events
                              where p.Status == 1 && p.EventId == EventId
                              select p;


                var EventsList = (from p in EventsQuery
                                  orderby p.CreatedOn descending
                                  select new
                                  {
                                      ClassId = p.EventId,
                                      StartDate = p.StartDate,
                                      EndDate = p.EndDate,
                                      Status = p.Status,
                                      HostId = p.HostId,
                                      CourseId = p.CourseId,
                                      PackageId = p.PackageId,
                                      SuperPackageId = p.SuperPackageId,
                                      HostName = p.Host.Name,
                                      TeacherName = p.Teacher.NameAr,
                                      p.PriceCompany,
                                      p.PricePersonal,
                                      p.Discount,
                                      p.MaximumNumberChair
                                  }).ToList();

                return Ok(new { Events = EventsList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpGet("GetEventsV3")]
        public IActionResult GetEventsV3()
        {
            try
            {
                IQueryable<Events> EventsQuery;

                EventsQuery = from p in db.Events
                              where p.Status == 1
                              select p;


                var EventsList = (from p in EventsQuery
                                  orderby p.CreatedOn descending
                                  select new
                                  {
                                      EventId = p.EventId,
                                      p.Name,
                                      p.Description,
                                      p.EventType,
                                      StartDate = p.StartDate,
                                      EndDate = p.EndDate,
                                      Status = p.Status,
                                      HostId = p.HostId,
                                      CourseId = p.CourseId,
                                      PackageId = p.PackageId,
                                      SuperPackageId = p.SuperPackageId,
                                      HostName = p.Host.Name,
                                      TeacherName = p.Teacher.NameAr,
                                      p.PriceCompany,
                                      p.PricePersonal,
                                      p.Discount,
                                      p.MaximumNumberChair
                                  }).ToList();

                return Ok(new { Events = EventsList });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet("GetPackageBytype")]
        public IActionResult GetPackageBytype(int EventType)
        {
            // for Super
            IQueryable<SuperPackages> SuperPackagesQuery;
            IQueryable<Packages> PackagesQuery;
            IQueryable<Courses> CoursesQuery;

            if (EventType == 1)
            {
                SuperPackagesQuery = from p in db.SuperPackages
                                     where p.Status == 1
                                     select p;
                var SuperPackagesList = (from p in SuperPackagesQuery
                                         orderby p.CreatedOn descending
                                         select new
                                         {
                                             PackageName = p.Name,
                                             PackageId = p.SuperPackageId,
                                             PriceCompany = p.PriceCompany,
                                             PricePersonal = p.PricePersonal,
                                             Discount = p.Discount
                                         }).ToList();

                return Ok(new { Package = SuperPackagesList });

            }
            //for Package
            else if (EventType == 2)
            {
                PackagesQuery = from p in db.Packages
                                where p.Status == 1
                                select p;
                var PackagesList = (from p in PackagesQuery
                                    orderby p.CreatedOn descending
                                    select new
                                    {
                                        PackageName = p.Name,
                                        PackageId = p.PackageId,
                                        PriceCompany = p.PriceCompany,
                                        PricePersonal = p.PricePersonal,
                                        Discount = p.Discount
                                    }).ToList();

                return Ok(new { Package = PackagesList });

            }
            // for course
            else
            {
                CoursesQuery = from p in db.Courses
                               where p.Status == 1
                               select p;
                var CourseList = (from p in CoursesQuery
                                  orderby p.CreatedOn descending
                                  select new
                                  {
                                      PackageName = p.Name,
                                      PackageId = p.CourseId,
                                      PriceCompany = p.PriceCompany,
                                      PricePersonal = p.PricePersonal,
                                      Discount = p.Discount,
                                      pkg = p.Package.Name,
                                      spkg = p.SuperPackage.Name
                                  }).ToList();

                return Ok(new { Package = CourseList });

            }
        }



        [HttpGet("GetEventsByStudentId")]
        public IActionResult GetEventsByStudentId(int pageNo, int pageSize, long StudentId)
        {   
            IQueryable<ActiveStudents> EventsQuery;
            EventsQuery = from q in db.ActiveStudents
                          where q.ActualStudentId == StudentId
                          select q;

            var EventList = (from t in EventsQuery
                             orderby t.CreatedOn descending
                             select new
                             {
                                 t.ActualStudent.StudentId,
                                 t.Event.Name,
                                 t.Event.StartDate,
                                 t.Event.EndDate,
                                 HostName= t.Event.Host.Name
                             }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            return Ok(new { EventsStudent = EventList, Count = EventList.Count() });

        }

        [HttpGet("GetInvoicesByEventId")]
        public IActionResult GetInvoicesByEventId(int pageNo, int pageSize, long EventId, int SubscriberType, long Subscriber)
        {
            IQueryable<InvoicesDetails> InvoicesQuery;

            if (SubscriberType != 0)
            {
                if (Subscriber != 0)
                {

                    if (SubscriberType == 1)
                    {

                        InvoicesQuery = from p in db.InvoicesDetails
                                        where
                                         p.Status == 1 &&
                                         p.EventId == EventId &&
                                         p.Invoice.CustomerType == SubscriberType &&
                                         p.Invoice.Company.CompanyId == Subscriber
                                        select p;

                    }

                    else
                    {
                        InvoicesQuery = from p in db.InvoicesDetails
                                        where
                                         p.Status == 1 &&
                                         p.EventId == EventId &&
                                         p.Invoice.CustomerType == SubscriberType &&
                                         p.Invoice.Student.StudentId == Subscriber
                                        select p;

                    }
                }
                else
                {
                    InvoicesQuery = from p in db.InvoicesDetails
                                    where
                                     p.Status == 1 &&
                                     p.EventId == EventId &&
                                     p.Invoice.CustomerType == SubscriberType
                                    
                                    select  p;
                }
            }
            else if(EventId !=0)
            {
                InvoicesQuery = from p in db.InvoicesDetails
                                where
                                 p.Status == 1 &&
                                 p.EventId == EventId
                                select p;
            }
            else
            {
                InvoicesQuery = from p in db.InvoicesDetails
                                where
                                 p.Status == 1 
                                select p;
            }


            var InvoicesList = (from t in InvoicesQuery
                                orderby t.CreatedOn descending
                                select new
                                {
                                    t.Invoice.Student.NameAr,
                                    t.Invoice.RegistryType,
                                    t.InvoiceId,
                                    t.Invoice.Company.CompanyName,
                                    t.NumberOfChair,
                                    EventTitle = t.Event.Name,
                                    t.EventId,
                                    t.Invoice.CustomerType,
                                    CompanyId = t.Invoice.Company.CompanyId == null ? 0 : t.Invoice.Company.CompanyId,
                                    StudentId = t.Invoice.Student.StudentId == null ? 0 : t.Invoice.Student.StudentId,
                                    t.Invoice.CreatedOn
                                }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

       
            return Ok(new { Invoices = InvoicesList, Count = InvoicesList.Count() });

            
        }

        [HttpGet("GetSubscribersByEventId")]
        public IActionResult GetSubscribersByEventId(long EventId, int SubscriberType)
        {
            IQueryable<Invoices> InvoicesQuery;

            long[] CompanyIdList = db.InvoicesDetails.AsEnumerable().Where(x => x.EventId == EventId)
               .Select(r => (long)r.InvoiceId)
               .ToArray();

            if (SubscriberType == 1)
            {
                InvoicesQuery = from p in db.Invoices.Distinct()
                                where
                                 p.Status == 1 &&
                                (CompanyIdList.Contains(p.InvoiceId)) &&
                                 p.CustomerType == SubscriberType
                                select p;


                var SubscriberList = (from p in InvoicesQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          id = p.Company.CompanyId,
                                          name = p.Company.CompanyName
                                      }).Distinct().ToList();

                return Ok(new { Subscriber = SubscriberList });
            }
            else
            {

                InvoicesQuery = from p in db.Invoices
                                where
                                 p.Status == 1 &&
                                (CompanyIdList.Contains(p.InvoiceId)) &&
                                 p.CustomerType == SubscriberType
                                select p;


                var SubscriberList = (from p in InvoicesQuery
                                      orderby p.CreatedOn descending
                                      select new
                                      {
                                          id = p.Student.StudentId,
                                          name = p.Student.NameAr
                                      }).Distinct().ToList();

                return Ok(new { Subscriber = SubscriberList });

            }
        }


        [HttpGet("GetActiveStudents")]
        public IActionResult GetActiveStudents(int pageNo, int pageSize, int eventId, string studentId, long InvoiceId)
        {
            try
            {  List<long> StudentList = new List<long>();

                if (!string.IsNullOrEmpty(studentId)) {

                string[] AllStudent =studentId.Split(',');
               
                foreach (var x in AllStudent)
                {
                    StudentList.Add(long.Parse(x));
                }

                }
               

                IQueryable<ActiveStudents> ActiveStudentsQuery;

                if (eventId != 0 && InvoiceId != 0)
                {
                    ActiveStudentsQuery = from p in db.ActiveStudents
                                          where p.Status == 1 &&
                                          p.EventId == eventId &&
                                           p.InvoiceId == InvoiceId
                                          select p;
                }
                //else if (studentId != 0)
                //{
                //    ActiveStudentsQuery = from p in db.ActiveStudents
                //                          where p.Status == 1 &&
                //                          p.ActualStudentId == studentId &&
                //                          p.ActualStudent.Status == 1 &&
                //                          p.EventId == eventId
                //                          select p;
                //}
                //else if (InvoiceId != 0)
                //{
                //       ActiveStudentsQuery = from p in db.ActiveStudents
                //                      where p.Status == 1 &&
                //                      p.ActualStudentId == studentId && 
                //                      p.ActualStudent.Status==1 &&
                //                      p.EventId== eventId && 
                //                      p.InvoiceId== InvoiceId
                //                      select p; 
                //}  
                
                else
                {
                    ActiveStudentsQuery = from p in db.ActiveStudents
                                          where p.Status == 1
                                          select p;
                }
                
                

                var ActiveStudentsCount = (from p in ActiveStudentsQuery
                                     select p).Count();

                var StudentsList = (from p in ActiveStudentsQuery
                                    orderby p.CreatedOn descending
                                    select new
                                    {
                                        StudentId = p.ActualStudentId,
                                        NameAr = p.ActualStudent.NameAr,
                                        NameEn = p.ActualStudent.NameEn,
                                        PhoneNumber1 = p.ActualStudent.PhoneNumber1,
                                        SubscribeType = (p.ActualStudent.CompanyId == null ? "افــراد" : "شــركة"),
                                        CompanyName = (p.ActualStudent.CompanyId == null ? "لايوجد" : p.ActualStudent.Company.CompanyName),
                                        CompanyId = p.ActualStudent.Company.CompanyId == null ? 0 : p.ActualStudent.Company.CompanyId,
                                        Email = p.ActualStudent.Email,
                                        Photo = p.ActualStudent.Photo,   
                                        isHaveCourse = (StudentList.Contains(p.ActualStudentId))

                                    }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { ActiveStudents = StudentsList, count = ActiveStudentsCount });
            }
            catch (Exception e)                                 
            {
                return StatusCode(500, e.Message);
            }
        }


        [HttpGet("GetCustomers")]
        public IActionResult GetCustomers(int pageNo, int pageSize, int CustomerType, int CompanyId, int studentId)
        {
            try
            {
                  // long[] ActualStudentIds = db.ActiveStudents.AsEnumerable()
                  //.Select(r => (long)r.ActualStudentId)
                  //.ToArray();
               
                IQueryable<Students> StudentsQuery;
                //Company Student
                if (CustomerType == 1)
                {
                    if (CompanyId != 0)
                    {
                        StudentsQuery = from p in db.Students
                                        where p.Status == 1 && p.CompanyId == CompanyId && p.Company.Status == 1
                                        //&&
                                        // !(ActualStudentIds.Contains(p.StudentId))
                                        select p;
                    }
                    else
                    {
                        StudentsQuery = from p in db.Students
                                        where p.Status == 1 && p.CompanyId != null && p.Company.Status == 1
                                        select p;
                    }

                }
                //Student Personal
                else if (CustomerType == 2)
                {
                    StudentsQuery = from p in db.Students
                                    where p.Status == 1 && p.CompanyId == null
                                   // && !(ActualStudentIds.Contains(p.StudentId))
                                    //  && p.StudentId == studentId
                                    select p;
                }
                //All
               
                     else
                {
                        StudentsQuery = from p in db.Students
                                        where p.Status == 1 && ((p.CompanyId != null && p.Company.Status == 1) || (p.CompanyId == null))
                                        select p;
                    }


                var StudentsCount = (from p in StudentsQuery
                                     select p).Count();

                var StudentsList = (from p in StudentsQuery
                                    orderby p.CreatedOn descending
                                    select new
                                    {
                                        StudentId = p.StudentId,
                                        NameAr = p.NameAr,
                                        NameEn = p.NameEn,
                                        Nationality = p.Nationality,
                                        PhoneNumber1 = p.PhoneNumber1,
                                        PhoneNumber2 = p.PhoneNumber2,
                                        BirthDate = p.BirthDate,
                                        Location = p.Location,
                                        CurrentJob = p.CurrentJob,
                                        ExperinceNumbers = p.ExperinceNumbers,
                                        Status = p.Status,
                                        SubscribeType = (p.CompanyId == null ? "افــراد" : "شــركة"),
                                        CompanyName = (p.CompanyId == null ? "لايوجد" : p.Company.CompanyName),
                                        CompanyId = p.CompanyId,
                                        Email = p.Email,
                                        Photo = p.Photo
                                    }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();

                return Ok(new { Student = StudentsList, count = StudentsCount });
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("Add")]
        public IActionResult AddEvent([FromBody] Events events)
        {
            try
            {
                if (events == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }
                events.CreatedBy = userId;
                events.CreatedOn = DateTime.Now;
                events.Status = 1;
                db.Events.Add(events);
                db.SaveChanges();

                return Ok("لقد قمت بتسـجيل بيانات الحدت بنــجاح");
            }
            catch (DbUpdateException e)

            {
                return StatusCode(500, e.Message);
            }
        }

        public partial class ActualStudentList
        {
            public long studentId { get; set; }
        }

        [HttpPost("{InvoiceId}/{EventId}/AddActiveStd")]
        public IActionResult AddActiveStudent(long InvoiceId, long EventId, [FromBody] ActualStudentList[] StudentIds)
        {
            try
            {

                if (StudentIds == null)
                {
                    return BadRequest("حذث خطأ في ارسال البيانات الرجاء إعادة الادخال");
                }

                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }



                foreach (var x in StudentIds)
                {
                    ActiveStudents ActStu = new ActiveStudents();
                    ActStu.EventId = EventId;
                    ActStu.InvoiceId = InvoiceId;
                    ActStu.ActualStudentId = x.studentId;
                    ActStu.Status = 1;
                    ActStu.CreatedBy = userId;
                    ActStu.CreatedOn = DateTime.Now;
                    db.ActiveStudents.Add(ActStu);
                    db.SaveChanges();

                }


                return Ok("لقد قمت بتسـجيل المتدربين بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpPost("{StudentId}/{eventId}/{InvoiceId}/delete")]
        public IActionResult DeleteStudent(long StudentId,long eventId, long InvoiceId)
        {
            try
            {
                var userId = this.help.GetCurrentUser(HttpContext);

                if (userId <= 0)
                {
                    return StatusCode(401, "الرجاء الـتأكد من أنك قمت بتسجيل الدخول");
                }

                var Student = (from p in db.ActiveStudents
                               where p.ActualStudentId == StudentId
                               && p.InvoiceId==InvoiceId
                               && p.EventId == eventId
                               && (p.Status == 1)
                               select p).SingleOrDefault();

                if (Student == null)
                {
                    return NotFound("خــطأ : المستخدم غير موجود");
                }

                Student.Status = 9;
                Student.UpdatedBy = userId;
                Student.UpdatedOn = DateTime.Now;
                db.SaveChanges();
                return Ok("تم مسح المتدرب من الدوره بنجاح");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


    }
}