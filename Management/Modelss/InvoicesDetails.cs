﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class InvoicesDetails
    {
        public long InvoiceDetailsId { get; set; }
        public int? NumberOfChair { get; set; }
        
        public double TotalPrice { get; set; }
        public double TotalPriceAfterDescount { get; set; }
        public int Discount { get; set; }
        public long? EventId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }
        public long? InvoiceId { get; set; }

        public Events Event { get; set; }
        public Invoices Invoice { get; set; }
    }
}
