﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class MessageAttachments
    {
        public long Id { get; set; }
        public long MessageId { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }

        public Messages IdNavigation { get; set; }
    }
}
