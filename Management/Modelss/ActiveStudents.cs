﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class ActiveStudents
    {
        public long ActiveStudeId { get; set; }
        public long EventId { get; set; }
        public long InvoiceId { get; set; }
        public long ActualStudentId { get; set; }
        public int Status { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public Students ActualStudent { get; set; }
        public Events Event { get; set; }
        public Invoices Invoice { get; set; }
    }
}
