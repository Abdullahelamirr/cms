﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class Appointments
    {
        public long AppointmentId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public byte? AppointmentType { get; set; }
        public int? CustomerType { get; set; }
        public long? CompanyId { get; set; }
        public long? StudentId { get; set; }
        public DateTime? DateTimeFrom { get; set; }
        public byte? AppointmentStatus { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }
        public Companies Company { get; set; }
        public Students Student { get; set; }
    }
}
