﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class Payments
    {
        public long PaymentsId { get; set; }
        public double? Paids { get; set; }
        public DateTime? CurrentPaidDate { get; set; }
        public DateTime? NextPaidDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }
        public long? InvoiceId { get; set; }
        public int PaymentType { get; set; }
        public string PaymentRefrence { get; set; }
        public Invoices Invoice { get; set; }
    }
}
