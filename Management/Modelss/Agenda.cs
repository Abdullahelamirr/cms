﻿using System;
using System.Collections.Generic;

namespace Management.Models
{
    public partial class Agenda
    {
        public long AgendaId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? FromToDate { get; set; }
        public DateTime? DateTimeFrom { get; set; }
        public DateTime? DateTimeTo { get; set; }
        public long EventId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public short Status { get; set; }

        public Events Event { get; set; }
    }
}
